<div class="modal fade modal-window-bouquet" id="bouquetModal" tabindex="-1" role="dialog" aria-labelledby="bouquetModal" aria-hidden="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content row align-items-center justify-content-center">
            <div class="modal-body">

                <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                </button>
                <div class="container_for_text">
                    <div class="text">


                        <form action="{{route('products.addProductsToCart')}}" class="addProduct">
                            <h2 class="name name_modal -header-gradient title -standalone">Букет Айсберг</h2>

                            <input type="hidden" name="id_product" class="id_product" id="id_product">
                            <input type="hidden" name="size_product" id="size_product">
                            <input type="hidden" name="price_product" id="price_product">

                            <div class="choose-size-wrap">
                                <p class="choose-size-wrap-title">Выберите размер:</p>

                                <div class="choose-size-wrap-options cheks_wrap">



                                </div>
                            </div>
                            <p class="choose-size-wrap-title">Масса: <span class="mass_element">580</span> гр.</p>
                            <p class="description decription_modal">Размеры указаны стандартные, мы можем собрать букет исходя из ваших пожеланий</p>
                            <button class="btn btn-pink add_to_cart">Заказать за 1500 Р</button>
                        </form>
                    </div>
                </div>


                <div class="container_slider">
                    <div id="carouselImg" class="carousel slide carousel-bouquet" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner" role="listbox">
                            {{--<div class="carousel-item active">--}}
                                {{--<img class="d-block img-fluid" src="/assets/img/pics/img-7474.png" alt="First slide">--}}
                            {{--</div>--}}
                            {{--<div class="carousel-item">--}}
                                {{--<img class="d-block img-fluid" src="/assets/img/pics/img-7474.png" alt="Second slide">--}}
                            {{--</div>--}}
                            {{--<div class="carousel-item">--}}
                                {{--<img class="d-block img-fluid" src="/assets/img/img-7474.png" alt="Third slide">--}}
                            {{--</div>--}}
                        </div>
                        <a class="carousel-control-prev" href="#carouselImg" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselImg" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="orderModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content row align-items-center justify-content-center -order-form">

            <div class="modal-body js-formOut">
                <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="-header-gradient mb-1 title">Заказать звонок</h2>
                <p class="text mb-4">Менеджер свяжется с вами для уточнения деталей</p>
                <div class="modal-form mb-3">
                    <fieldset class="fieldset -phone">
                        <input type="text" class="input js-nameInputGoods" placeholder="Ваше имя">
                    </fieldset>
                    <fieldset class="fieldset -phone">
                        <input type="text" class="input js-phoneInputGoods" placeholder="+7 (___) ___-__-__">
                    </fieldset>
                    <button class="btn w-100 js-requestGoods">Заказать</button>
                </div>

                <p class="form-small">Ваши личные данные будут обработаны <br>на основе соглашения о конфиденциальности</p>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="responseModal" tabindex="-1" role="dialog" aria-labelledby="responseModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content row align-items-center justify-content-center -order-form">

            <div class="modal-body">
                <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="-header-gradient mb-1 title">Спасибо</h2>
                <p class="text mb-4">Менеджер свяжется с вами для уточнения деталей</p>
                <button class="btn" data-dismiss="modal" aria-label="Close">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addProductYes" tabindex="-1" role="dialog" aria-labelledby="addProductYes" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content row align-items-center justify-content-center -order-form">

            <div class="modal-body">
                <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="-header-gradient mb-1 title">Товар добавлен в корзину</h2>
                <a href="/cart" class="btn">Перейти в корзину</a>
                <button class="btn" data-dismiss="modal" aria-label="Close">Закрыть</button>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="addProductYes2" tabindex="-1" role="dialog" aria-labelledby="addProductYes2" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content row align-items-center justify-content-center -order-form">

            <div class="modal-body">
                <h2 class="-header-gradient mb-1 title">Товар добавлен в корзину</h2>
                <button class="btn reload_btn" data-dismiss="modal" aria-label="Close">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="photoModal" tabindex="-1" role="dialog" aria-labelledby="responseModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content row align-items-center justify-content-center -order-form">
            <div class="modal-body">
                <img src="" class="modal-image-item" alt="">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="thank" tabindex="-1" role="dialog" aria-labelledby="thank" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content row align-items-center justify-content-center -order-form">

            <div class="modal-body">
                <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="-header-gradient mb-1 title">Спасибо за заявку!</h2>
                <p class="text mb-4">Наш менеджер перезвонит Вам через 15 минут для уточнения всех деталей</p>
                <button class="btn" data-dismiss="modal" aria-label="Close">Закрыть</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="privacy" tabindex="-1" role="dialog" aria-labelledby="privacy" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content row align-items-center justify-content-center -order-form">

            <div class="modal-body">
                <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <p class="text mb-4">
                    @if (!empty($texts[0]->privacy))
                        {!!$texts[0]->privacy!!}
                    @endif
                </p>
                <button class="btn" data-dismiss="modal" aria-label="Close">Закрыть</button>
            </div>
        </div>
    </div>
</div>