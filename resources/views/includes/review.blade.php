@if(!empty(count($reviews)))
<section class="section -reviews-section">
    <div class="container">
        <h2 class="-header-gradient title -standalone text-center">Отзывы клиентов</h2>
    </div>
    <div class="reviews-carousel">
            @foreach ($reviews as $review)
                <?$product_id = App\Photo::find($review->image)?>
                    @if(!empty($product_id->file))
                        <div class="reviews-carousel-slide lazy-loaded" data-bg="{{$product_id->file}}">
                    @else
                        <div class="reviews-carousel-slide">
                    @endif
                            <div class="reviews-carousel-slide-action start_video_{{$review->id}}">
                                @if(!empty($review->video))
                                    <div style="webkit-border-radius: 20px;-moz-border-radius: 30px;border-radius: 30px;overflow:hidden;" id="stock-video_{{$review->id}}">
                                        <iframe width="1540" height="752" src="{{$review->video}}" id="video_{{$review->id}}"></iframe>
                                    </div>
                                    <a class="start-video stock-play-btn_{{$review->id}}">
                                        <p class="review-carousel-slide-action-title">Смотреть отзыв</p>
                                        <button class="btn __play-theme"></button>
                                    </a>
                                @endif
                            </div>
                        </div>
         @endforeach


    </div>
</section>
@endif


@if($reviews)
    {{--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>--}}
    {{--<script>--}}
        {{--@foreach ($reviews as $review)--}}
            {{--$('.start_video_{{$review->id}}').on('click', function() {--}}
                {{--$('.stock-play-btn_{{$review->id}}').fadeOut('normal');--}}
                {{--$("#video_{{$review->id}}")[0].src += "?autoplay=1";--}}
            {{--});--}}
        {{--@endforeach--}}
    {{--</script>--}}
@endif