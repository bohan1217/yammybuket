@if(!empty(count($products)))
<section class="section -catalog catalog" id="catalog">
    <div class="container container-big">
        <div class="row flex-wrap justify-content-between align-items-stretch vp-fade-catalog">
            <div class="col-md-12">
                <h2 class="-header-gradient title -standalone">Букеты</h2>
            </div>
            @if(!empty($products))
            @foreach ($products as $product)
            <?php
            $massa = "mass".$product->priority;
            $description = "description".$product->priority;
            $price_size = "price".$product->priority;

            $price = [$product->price1,$product->price2,$product->price3];
            foreach($price as $array_key=>$array_item)
            {
                if($price[$array_key] == NULL)
                {
                    unset($price[$array_key]);
                }
            }

            if(empty($price)) {
                $price[] = 0;
            }

            $min_price = min($price);
            $price_size = $product->$price_size;


            $price_1 = $product->price1;
            $price_2 = $product->price2;
            $price_3 = $product->price3;

            if(!empty($product->discount)) {
                $discount = $product->discount;
                $min_price = $min_price - ($min_price * ($discount/100));
                $price_size = $price_size - ($price_size * ($discount/100));


                $price_1 = $price_1 - ($price_1 * ($discount/100));
                $price_2 = $price_2 - ($price_2 * ($discount/100));
                $price_3 = $price_3 - ($price_3 * ($discount/100));
            }

            $price_size = round($price_size);

            $price_1 = round($price_1);
            $price_2 = round($price_2);
            $price_3 = round($price_3);

            $min_price = round($min_price);
            ?>
            <div class="col-md-6 item sale vp-fade" id="element_{{$product->id}}">
                <div class="container_for_item gtm-bouquet" data-custom-target="#bouquetModal" data-toggle="modal">
                    <div class="content light-green gtm-bouquet" @if(!empty($product->color)) style="background-color: {{$product->color}}" @endif>
                        <div class="img_container gtm-bouquet">
                            <img class="gtm-bouquet" src="{{$product->photo->file}}" alt="{{$product->name}}">
                        </div>
                        <div class="text gtm-bouquet">
                            <h2 class="name gtm-bouquet">{{$product->name}}</h2>
                            <p class="description __title-theme">Размер:
                                @if(($product->priority == 1))
                                S
                                @elseif(($product->priority == 2))
                                M
                                @elseif(($product->priority == 3))
                                L
                                @endif
                            </p>
                            @if(!empty($product->$massa))
                            <p class="description __title-theme">Масса: {{$product->$massa}} гр.</p>
                            @endif
                            @if(!empty($product->$description))
                            <p class="description gtm-bouquet">{!! $product->$description  !!}</p>
                            @endif
                            <div class="detail-wrap">
                                <p class="detail-wrap-title">от {{$min_price}} {{env("CURRENCY_PRICE", "₽")}}</p>
                                <button class="mb-2 btn">Другие размеры</button>
                            </div>
                        </div>
                        {{--additional-images--}}
                        <div class="additional-images">
                            @if(!empty($product->icon_id1))
                            @foreach (json_decode($product->icon_id1) as $photo)
                            <?$product_id = App\Photo::find($photo)?>
                            <img src="{{$product_id->file}}" data-value="1">
                            @endforeach
                            @endif

                            @if(!empty($product->icon_id2))
                            @foreach (json_decode($product->icon_id2) as $photo)
                            <?$product_id = App\Photo::find($photo)?>
                            <img src="{{$product_id->file}}" data-value="2">
                            @endforeach
                            @endif

                            @if(!empty($product->icon_id3))
                            @foreach (json_decode($product->icon_id3) as $photo)
                            <?$product_id = App\Photo::find($photo)?>
                            <img src="{{$product_id->file}}" data-value="3">
                            @endforeach
                            @endif
                        </div>

                        <div class="additional-check">
                            @if(!empty($product->icon_id1))
                            <label class="check-wrap">
                                <input type="radio" name="size" class="check-input-item ckech_1 check_input_item_{{$product->id}}"
                                       onclick="checkInput('{{$product->id}}',1, 'Заказать за {{$price_1}} Р', '{{$price_1}}')"
                                       @if(($product->priority == 1)) checked @endif value="1"
                                data-value="{!! $product->description1!!}"
                                data-size="{!! $product->mass1!!}"
                                >
                                <span class="check-input-title check_title_item_el_1 check_title_item_{{$product->id}}">S</span>
                            </label>
                            @endif
                            @if(!empty($product->icon_id2))
                            <label class="check-wrap">
                                <input type="radio" name="size" class="check-input-item ckech_2 check_input_item_{{$product->id}}"
                                       onclick="checkInput('{{$product->id}}',2, 'Заказать за {{$price_2}} Р', '{{$price_2}}')"
                                       @if(($product->priority == 2)) checked @endif value="2"
                                data-value="{!! $product->description2!!}"
                                data-size="{!! $product->mass2!!}"
                                >
                                <span class="check-input-title check_title_item_el_2 check_title_item_{{$product->id}}">M</span>
                            </label>
                            @endif
                            @if(!empty($product->icon_id3))
                            <label class="check-wrap">
                                <input type="radio" name="size" class="check-input-item ckech_3 check_input_item_{{$product->id}}"
                                       onclick='checkInput("{{$product->id}}",3,"Заказать за {{$price_3}} Р", "{{$price_3}}")'
                                       @if(($product->priority == 3)) checked @endif value="3"

                                data-value="{!! $product->description3!!}"
                                data-size="{!! $product->mass3!!}"
                                >
                                <span class="check-input-title check_title_item_el_3 check_title_item_{{$product->id}}">L</span>
                            </label>
                            @endif
                        </div>

                        <button class="mb-2 btn target-catalog-1 js-orderModalFromCatalog __mobile-detail button_data"
                                data-id="{{$product->id}}"
                                data-size="{{$product->$massa}}"
                                data-priority="{{$product->priority}}"
                                data-button_text="Заказать за {{$price_size}} Р"
                                data-price="{{$price_size}}"
                                data-description="{!! $product->$description  !!}"
                                data-custom-target="#bouquetModal"
                                data-toggle="modal"
                        >Другие размеры</button>

                        @if(!empty($product->discount))
                        <div class="sale-wrap">
                            <div class="sale-wrap-title">{{$product->discount}}%</div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>
@endif