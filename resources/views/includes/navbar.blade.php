<div class="navbar">
    <div class="navbar__left">
        <a href="/" class="navbar__logo">
            <img src="/assets/img/logo-white.png" alt="Yammy logo">
        </a>
        <a href="/cart" class="basket-mobile-link">
            <span class="nav-icon __basket"></span>
            <i class="basket-count my-total-qty" id="total_qty">@if($cart) {{$cart->totalQty}} @else 0 @endif</i>
        </a>
        <div class="navbar__dropdown js-navbarDropdown">
            <div class="navbar__dropdown-toggler js-navbarToggler">
                <i></i>
                <i></i>
                <i></i>
            </div>
            <ul class="navbar__dropdown-menu">
                <div class="navbar__dropdown-close js-navbarClose"></div>
                <li><a href="/catalog/sweet"><span>Наши букеты</span></a></li>
                <li><a href="/catalog/fruit"><span>Наши Корзины</span></a></li>
                <li>
                    <a href="#clean" class="__scroll-link"><span>Упаковка</span></a>
                </li>
                <li>
                    <a href="#delivery" class="__scroll-link"><span>Доставка</span></a>
                </li>
                <li>
                    <a href="#delivery" class="__scroll-link"><span>Оплата</span></a>
                </li>
                <li>
                    <a href="/cart">
                        <span class="nav-icon __basket"></span>
                        <span>Корзина</span>
                        <i class="basket-count my-total-qty">@if($cart) {{$cart->totalQty}} @else 0 @endif</i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="navbar__links">
        <a href="/catalog/sweet">Букеты</a>
        <a href="/catalog/fruit">Корзины</a>
        <a href="#clean" class="__scroll-link"><span>Упаковка</span></a>
        <a href="#delivery" class="__scroll-link"><span>Доставка</span></a>
        <a href="#delivery" class="__scroll-link"><span>Оплата</span></a>
        <a href="/cart">
            <span class="nav-icon __basket"></span>
            <span>Корзина</span>
            <i class="basket-count my-total-qty">@if($cart) {{$cart->totalQty}} @else 0 @endif</i>
        </a>
    </div>
    <div class="navbar__phone">
        <a class="phone" href="tel:+74951855756">8-495-185-57-56</a>
        <a class="phone" href="tel:+79262731568">8-926-273-15-68</a>
      <a href="delivery" class="__scroll-link"><span>zakazyammy@gmail.com</span></a>
    </div>
</div>