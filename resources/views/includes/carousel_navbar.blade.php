@if(!empty(count($products_cart)))
    <div class="openings-carousel-wrap">
        <h2 class="-header-gradient mb-3 title text-center">Не забудьте выбрать открытку и короб</h2>
        <div class="openings-carousel">
            @foreach ($products_cart as $product)
                <form action="{{route('products.addProductsToCartCarts')}}" class="addProductCarts">

                    <input type="hidden" name="type_product" class="type_product" value="2">
                    <input type="hidden" name="id_product" class="id_product" value="{{$product->id}}">
                    <input type="hidden" name="price_product" value="{{$product->price}}">

                    <div class="opening-box">
                        <div class="opening-box-preview -photo-modal" data-custom-target="#photoModal" data-toggle="modal" data-image-url="{{$product->photo->file}}">
                            <img src="{{$product->photo->file}}" alt="{{$product->name}}">
                        </div>
                        <div class="opening-box-content">
                            <p class="opening-box-content-title">{{$product->name}}</p>
                            <button class="opening-box-action-btn add_cart_carts">В корзину за {{$product->price}} {{env("CURRENCY_PRICE", "₽")}}</button>
                        </div>
                    </div>
                </form>
            @endforeach
        </div>
    </div>
@endif