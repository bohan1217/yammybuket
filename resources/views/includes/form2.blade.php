<section class="section -form __blue-theme">
    <div class="container">
        @if (!empty($texts[0]->title7))
            <h2 class="-header-gradient title -standalone">{!!$texts[0]->title7!!}</h2>
        @endif
        @if (!empty($texts[0]->text7))
            {!!$texts[0]->text7!!}
        @endif
        <form class="form form_feedback2" action="{{route('home.feedbackAdd')}}" name="form_feedback2">
            <fieldset class="fieldset -phone">
                <input type="text" name="phone" class="input -xl js-phoneInput" placeholder="+7 (___) ___-__-__" required>
            </fieldset>
            <div class="btn-wrap">
                <button class="btn -xl js-requestCall button_send_form_not_modal_window">Заказать звонок</button>
                <p class="privacy-detail">Нажимая на кнопку вы даете согласие на <a href="#privacy" data-toggle="modal"  data-custom-target="#privacy">обработку персональных данных</a></p>
            </div>
        </form>
    </div>
</section>