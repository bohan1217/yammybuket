@if(!empty(count($products)))
    <section class="section -catalog catalog" id="products">
        <div class="container container-big">
            <div class="row flex-wrap justify-content-between align-items-stretch vp-fade-catalog">
                <div class="col-md-12">
                    @if(!empty($category['name']))
                                <h2 class="-header-gradient title -standalone">{{$category['name']}}</h2>
                            @else
                                @if(Route::current()->getName() == 'mainpage')
                                    <a href="/catalog/sweet/"><h2 class="-header-gradient title -standalone">Сладкие букеты</h2></a>
                                @elseif(Route::current()->getName() == 'catalog')
                                    <h2 class="-header-gradient title -standalone">Букеты</h2>
                                @else
                                    <h2 class="-header-gradient title -standalone">Сладкие букеты</h2>
                                @endif
                    @endif
                </div>
                @if(!empty($products))
                    @foreach ($products as $product)
                        <div class="col-md-6 item sale vp-fade" id="element_{{$product->id}}">
                            <div class="container_for_item gtm-bouquet" data-custom-target="#bouquetModal" data-toggle="modal">
                                <div class="content light-green gtm-bouquet" @if(!empty($product->color)) style="background-color: {{$product->color}}" @endif>
                                    <div class="img_container gtm-bouquet">
                                        <img class="gtm-bouquet b-lazy" data-src="{{$product->photo->file}}" src="assets/img/loading.jpg" alt="{{$product->name}}">
                                    </div>
                                    <?
                                    $price = [];
                                    if(count($product->tproducts) != 0) {
                                        foreach ($product->tproducts as $titem) {
                                            if($titem->priority == 1) {
                                                $size = $titem->size;
                                                $price_size = $titem->price;
                                                $mass = $titem->mass;
                                                $desc = $titem->desc;
                                                $product_id_main = $titem->id;
                                            }
                                            $price[] = $titem->price;
                                        }
                                        $size = App\Size::find($size);
                                        if(!empty($size)) {
                                            $size = $size->name;
                                        }
                                        $minprice = min($price);
                                    } else {
                                        $price_size = null;
                                        $size = '';
                                        $mass = '';
                                        $desc = '';
                                        $minprice = null;
                                        $product_id_main = '';
                                    }

                                    if(!empty($product->discount)) {
                                        $discount = $product->discount;
                                        $minprice = $minprice - ($minprice * ($discount/100));
                                        $price_size = $price_size - ($price_size * ($discount/100));
                                    }

                                    $price_size = round($price_size);
                                    $minprice = round($minprice);
                                    ?>
                                    <div class="text gtm-bouquet">
                                        <h2 class="name gtm-bouquet">{{$product->name}}</h2>
                                        @if(!empty($size))
                                            <p class="description __title-theme">Размер: {{$size}}</p>
                                            <p class="description __title-theme">Масса: {{$mass}} гр.</p>
                                            <p class="description gtm-bouquet">{!! $desc !!}</p>
                                            <div class="detail-wrap">
                                                <p class="detail-wrap-title">от {{$minprice}} {{env("CURRENCY_PRICE", "₽")}} </p>
                                                <button class="mb-2 btn">Подробнее</button>
                                            </div>
                                        @endif
                                    </div>


                                    <div class="additional-check">
                                        @if(count($product->tproducts) != 0)
                                            @foreach ($product->tproducts as $key => $titem2)
                                                <?
                                                    if(!empty($titem2->size)) {
                                                        $sizeN = App\Size::find($titem2->size);
                                                        $sizeN = $sizeN->name;
                                                    } else {
                                                        $sizeN = '';
                                                    }


                                                $price_product = $titem2->price;
                                                if(!empty($product->discount)) {
                                                    $discount = $product->discount;
                                                    $price_product = $price_product - ($price_product * ($discount/100));
                                                }

                                                $price_product = round($price_product);
                                                if($titem2->priority == 1) {
                                                    $priority_main = $key+1;
                                                }
                                                ?>
                                                <label class="check-wrap">
                                                    <input type="radio" name="size" class="check-input-item ckech_{{$key+1}} check_input_item_{{$product->id}}"
                                                           onclick="checkInput('{{$product->id}}', '{{$titem2->id}}' ,'{{$key+1}}', 'Заказать за {{$price_product}} {{env("CURRENCY_PRICE", "₽")}}', '{{$price_product}}')"
                                                           @if(($titem2->priority == 1)) checked @endif
                                                           value="1"
                                                           data-value="{!! $titem2->desc!!}"
                                                           data-size="{!! $titem2->mass!!}"
                                                    >
                                                    <span class="check-input-title check_title_item_el_{{$key+1}} check_title_item_{{$product->id}}">{{$sizeN}}</span>
                                                </label>
                                            @endforeach
                                        @endif
                                    </div>

                                    <div class="additional-images">
                                        @if(count($product->tproducts) != 0)
                                            @foreach ($product->tproducts as $keyY => $titem3)
                                                @foreach (json_decode($titem3->icon) as $photo)
                                                    <?$product_id = App\Photo::find($photo);?>
                                                    <img src="{{$product_id->file}}" data-value="{{$keyY+1}}">
                                                @endforeach
                                            @endforeach
                                        @endif
                                    </div>

                                    @if(!empty($size))
                                        <button class="mb-2 btn target-catalog-1 js-orderModalFromCatalog __mobile-detail button_data"
                                                data-id="{{$product_id_main}}"
                                                data-size="{{$mass}}"
                                                data-priority="{{$priority_main ? $priority_main : 1}}"
                                                data-button_text="Заказать за {{$price_size}} {{env("CURRENCY_PRICE", "₽")}}"
                                                data-price="{{$price_size}}"
                                                data-description="{!! $desc  !!}"
                                                data-custom-target="#bouquetModal"
                                                data-toggle="modal"
                                        >Другие размеры</button>
                                    @endif

                                    @if(!empty($product->discount))
                                        <div class="sale-wrap">
                                            <div class="sale-wrap-title">{{$product->discount}}%</div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            @if(Route::current()->getName() == 'mainpage')
                <div class="btn-wrap">
                    <a class="btn -xl gotopage mainall" data-url="/catalog/sweet/#products">Посмотреть все</a>
                </div>
            @endif

        </div>
    </section>
@endif