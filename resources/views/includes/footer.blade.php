<footer class="footer">
    <div class="container">
        <div class="footer__left">
            <a href="/">
                <img class="footer__logo" src="/assets/img/logo-color.png" alt="Yammy logo">
            </a>
        </div>
        <div class="footer__center">
            <div class="footer__links">
                <a href="/catalog/sweet">Букеты</a>
                <a href="/catalog/fruit">Корзины</a>
                <a href="#clean" class="__scroll-link"><span>Упаковка</span></a>
                <a href="#delivery" class="__scroll-link"><span>Доставка</span></a>
                <a href="#delivery" class="__scroll-link"><span>Оплата</span></a>
            </div>
            <div class="footer__socials">
                <a href="https://www.facebook.com/yammybuket" target="_blank">
                    <img src="assets/img/fb.png" alt="">
                </a>
                <a href="https://vk.com/yammybuket" target="_blank">
                    <img src="assets/img/vk.png" alt="">
                </a>
                <a href="https://www.instagram.com/yammy_buket/" target="_blank">
                    <img src="assets/img/inst.png" alt="">
                </a>
            </div>
        </div>
        <div class="footer__right">
            <a class="phone -black" href="tel:+74951855756">8-495-185-57-56</a>
            <a class="phone -black" href="tel:+79262731568"></a>
        </div>
    </div>
</footer>

