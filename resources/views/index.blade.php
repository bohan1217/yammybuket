@extends('layouts.app')

@section('content')
    <main id="wrapper">
        <section class="section -main">
            <div class="container">
                @include('includes.navbar')
                <div class="content">
                    <div class="row">
                        <div class="col-md-6 main__text">
                            @if (!empty($texts[0]->slider))
                                {!!$texts[0]->slider!!}
                            @endif
                            <a class="btn -x1" href="#products" class="__scroll-link" onclick="dataLayer.push({'event': 'buy-click'});">Купить букет</a>
                        </div>
                        <div class="col-md-6 main__flowers">
                            <img class="main-pic" src="/assets/img/pic-main.png" alt="Большой букет маршмеллоу">
                        </div>
                    </div>
                </div>
            </div>
            <div class="decor -main"></div>
        </section>

        <section class="section -features">
            <div class="container -smaller">
                <div class="row features">
                    <div class="col-md-3 col-sm-6 col-6 features__item mb-1">
                        <div class="features__pic">
                            <img data-src="/assets/img/feature-time.png" alt="">
                        </div>
                        <div class="features__title">Срок хранения до 2 недель</div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-6 features__item mb-1">
                        <a href="#company" class="__scroll-link">
                            <div class="features__pic">
                                <img data-src="/assets/img/feature-components.png" alt="">
                            </div>
                            <div class="features__title">Натуральные компоненты</div>
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-6 features__item mb-1">
                        <a href="#delivery" class="__scroll-link">
                            <div class="features__pic">
                                <img data-src="/assets/img/feature-delivery.png" alt="">
                            </div>
                            <div class="features__title">Доставка в день заказа</div>
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-6 features__item mb-1">
                        <div class="features__pic">
                            <img data-src="/assets/img/feature-shape.png" alt="">
                        </div>
                        <div class="features__title">Низкая калорийность</div>
                    </div>
                </div>
            </div>
        </section>


        @include('includes.cartProduct')
        @include('includes.cartProduct2')


        @if ((!empty($texts[0]->title2)) && (!empty($texts[0]->text2)))
        <section class="section -seo-section" id="seo-section">
            <div class="container" id="company">
                @if (!empty($texts[0]->title2))
                    <h1 class="-header-gradient title -standalone">{!!$texts[0]->title2!!}</h1>
                @endif
                @if (!empty($texts[0]->text2))
                    {!!$texts[0]->text2!!}
                @endif
                <div class="partners-list">
                    <a href="#" target="_blank">
                        <img data-src="assets/img/partner_1.png" alt="">
                    </a>
                    <a href="#" target="_blank">
                        <img data-src="assets/img/partner_2.png" alt="">
                    </a>
                    <a href="#" target="_blank">
                        <img data-src="assets/img/partner_3.png" alt="">
                    </a>
                    <a href="#" target="_blank">
                        <img data-src="assets/img/partner_4.png" alt="">
                    </a>
                </div>
            </div>
        </section>
        @endif


        @if ((!empty($texts[0]->title3)) && (!empty($texts[0]->text3)))
            <section class="section -clean" id="clean">
                <div class="container">
                    @if (!empty($texts[0]->title3))
                        <h2 class="title">{!!$texts[0]->title3!!}</h2>
                    @endif

                    @if (!empty($texts[0]->text3))
                        {!!$texts[0]->text3!!}
                    @endif
                </div>
                <div class="decor -clean"></div>
            </section>
        @endif


        <section class="section -points">
            <div class="container -smaller">
                <div class="row">
                    <div class="col-md-3 point col-sm-6">
                        <span class="point__num">1</span>
                        <div class="point__text">Если ваша девушка любит сладкое больше чем вас - мы не поможем, но вы сделаете ей
                            приятно=)
                        </div>
                    </div>
                    <div class="col-md-3 point col-sm-6">
                        <span class="point__num">2</span>
                        <div class="point__text">Наш зефир настолько мягкий, что его можно использовать как подушку!</div>
                    </div>
                    <div class="col-md-3 point col-sm-6">
                        <span class="point__num">3</span>
                        <div class="point__text">Благодаря нам вы повысите зефирин в организме во время зимы!</div>
                    </div>
                    <div class="col-md-3 point col-sm-6">
                        <span class="point__num">4</span>
                        <div class="point__text">Ты не знаешь, что подарить своей фитоняшке? Подари ей низкоколорийный букет от Yammy!
                        </div>
                    </div>
                </div>
            </div>
        </section>



        @if ((!empty($texts[0]->title4)) && (!empty($texts[0]->text4)))
        <section class="section -delivery" id="delivery">
            <div class="container -smaller">
                <div class="content -no-padding">
                    <div class="row -flex-center">
                        <div class="col-md-6">
                            @if (!empty($texts[0]->title4))
                                <h2 class="-header-gradient mb-3 title">{!!$texts[0]->title4!!}</h2>
                            @endif
                             @if (!empty($texts[0]->text4))
                                {!!$texts[0]->text4!!}
                             @endif
                        </div>
                        <div class="col-md-6">
                            <img data-src="assets/img/delivery-car.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endif


        @include('includes.form1')


        @include('includes.review')






        @if ((!empty($texts[0]->title6)) && (!empty($texts[0]->text6)))
            <section class="section -text-section">
                <div class="container -smaller">
                    <div class="row">
                        <div class="col-md-6">
                            @if (!empty($texts[0]->title6))
                                <h2 class="-header-gradient title -standalone">{!!$texts[0]->title6!!}</h2>
                            @endif
                            @if (!empty($texts[0]->text6))
                                {!!$texts[0]->text6!!}
                            @endif
                        </div>
                    </div>
                </div>
                <img data-src="assets/img/text-preview.png" class="text-preview" alt="">
            </section>
        @endif

        @include('includes.form2')


        @if(!empty($shops))
        <section class="section -contacts">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="contacts-list-wrap">
                            <div class="contacts-list">
                                @foreach($shops as $shop)
                                    <div class="contacts-list-item">
                                        <div class="contact-title">{{$shop->name}}</div>
                                        <div class="contact-address">{{$shop->address}}</div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div id="map" class="map"></div>
                    </div>
                </div>
            </div>
        </section>
        @endif


        {{--footer--}}
        @include('includes.footer')
        {{--footer--}}

        <div class="basket-panel">
            <a href="tel:8-495-185-57-56" class="panel-link __phone-link">8-495-185-57-56</a>
            <a href="/cart" class="panel-link __basket-link basket_link @if($cart && $cart->totalPrice == 0) display_none @endif"><span class="my-total-price">@if($cart) {{$cart->totalPrice}} @else 0 @endif</span> {{env("CURRENCY_PRICE", "₽")}}</a>
        </div>


    </main>
@endsection
