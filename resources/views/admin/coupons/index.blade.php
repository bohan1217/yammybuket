@extends('layouts.admin')
@section('title') Products @endsection
@section('content')
    <h1>Купоны</h1>


    @include('includes.success')

    <div class="row justify-content-center">
        <div class="col-md-3">
            <h1 class="text-center">Создать купон</h1>
            {!! Form::open(['method'=>'POST', 'action'=>'Admin\AdminCouponsController@store']) !!}

            <div class="form-group">
                {!! Form::label('code', 'Код') !!}
                {!! Form::text('code', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('discount', 'Скидка %') !!}
                {!! Form::text('discount', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Создать купон', ['class'=>'btn btn-block btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>


    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Купоны</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Код</th>
                        <th>Скидка</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Код</th>
                        <th>Скидка</th>
                        <th>Действия</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @if($coupons)
                        @foreach($coupons as $coupon)
                            <tr>
                                <td>{{$coupon -> id}}</td>
                                <td>{{ $coupon->code }}</td>
                                <td>{{ $coupon->discount }} %</td>
                                <td>
                                    {!! Form::open(['method'=>'DELETE', 'action'=>['Admin\AdminCouponsController@destroy', $coupon->id], 'class'=>'delete-form d-inline']) !!}
                                    <button type="submit" title="Delete question" class="ml-1 my-delete-button btn btn-small btn-danger d-inline-block"><i class="fa fa-lg fa-trash-o"></i></button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <!-- Delete User Modal-->
    <!-- Delete User Modal-->
    <a class="nav-link" data-toggle="modal" data-target="#deleteModal" style="display: none" id="my-modal-button">
        <i class="fa fa-fw fa-power-off"></i>Logout
    </a>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Ready to do this?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "OK" below if you are ready to do chosen action.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button id="my-modal-button-delete" class="btn btn-danger">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Delete User Modal-->





    <!-- END Delete User Confirmation Modal-->







    <script>



        document.addEventListener('DOMContentLoaded', function(){



            const buttons = document.querySelectorAll('.my-delete-button');

            const modal = document.querySelector('#deleteModal')



            const modalButton = document.querySelector('#my-modal-button');

            const modalButtonDelete = document.querySelector('#my-modal-button-delete');



            const count = buttons.length;



            let form;



            for(let i=0; i<count; i++) {





                buttons[i].addEventListener('click', function(e) {

                    e.preventDefault();

                    form = buttons[i].parentElement;

                    console.log(form);

                    modalButton.click();

                });



            }





            modalButtonDelete.addEventListener('click', function(){

                form.submit();

            })









        });



    </script>





@endsection

