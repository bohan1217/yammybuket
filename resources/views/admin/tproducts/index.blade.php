@extends('layouts.admin')
@section('title') Товарные предложения @endsection
{{--@section('breadcrumbs', Breadcrumbs::render('admin.dashboard.products'))--}}
@section('content')
    <h1>Товарные предложения</h1>

    @include('includes.success')

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Список товаров


            <a class="ml-1 " href="{{ route('admin.tproducts.create') }}" title="Edit shop"><button class="text-dark btn btn-warning d-inline-block">Создать товар <i class="fa fa-plus fa-lg"></i></button></a>

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Картинка</th>
                        <th>Размер</th>
                        <th>Цена</th>
                        <th>Обновлено в</th>
                        <th>Создано в</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Название</th>
                        <th>Картинка</th>
                        <th>Размер</th>
                        <th>Цена</th>
                        <th>Обновлено в</th>
                        <th>Создано в</th>
                        <th>Действия</th>
                    </tr>

                    </tfoot>
                    <tbody>
                    @if($products)
                        @foreach($products as $product)
                            <tr>
                                <td>{{$product -> name}}</td>
                                <?

                                if(!empty($product->size)) {
                                    $sizeN = App\Size::find($product->size);
                                    $sizeN = $sizeN->name;
                                } else {
                                    $sizeN = '';
                                }

                                $product_id = App\Photo::find(json_decode($product->icon)[0]);
                                if(!empty($product_id->file)) {
                                    $img_src = url('/').$product_id->file;
                                } else {
                                    $img_src = 'https://pngimage.net/wp-content/uploads/2018/05/default-png-6.png';
                                }
                                ?>
                                <td class="tdImage"><img src="{{ $img_src }}" alt="" class="imgProductsList"></td>
                                <td>{{ $sizeN }}</td>
                                <td>{{ $product->price }} {{env("CURRENCY_PRICE", "₽")}}</td>
                                <td>{{ $product->updated_at }}</td>
                                <td>{{ $product->created_at }}</td>

                                <td>
                                    <a class="ml-1 " href="{{ route('admin.tproducts.edit', $product->id) }}" title="Edit product"><button class="text-dark btn btn-warning d-inline-block"><i class="fa fa-edit fa-lg"></i></button></a>
                                    {!! Form::open(['method'=>'DELETE', 'action'=>['Admin\AdminTProductsController@destroy', $product->id], 'class'=>'delete-form d-inline']) !!}
                                    <button type="submit" title="Delete category" class="ml-1 my-delete-button btn btn-small btn-danger d-inline-block"><i class="fa fa-lg fa-trash-o"></i></button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>

@endsection





@section('modal')



    <!-- Delete User Modal-->

    <!-- Delete User Modal-->

    <a class="nav-link" data-toggle="modal" data-target="#deleteModal" style="display: none" id="my-modal-button">

        <i class="fa fa-fw fa-power-off"></i>Logout

    </a>



    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">

        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="deleteModalLabel">Ready to do this?</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">×</span>

                    </button>

                </div>

                <div class="modal-body">Select "OK" below if you are ready to do chosen action.</div>

                <div class="modal-footer">

                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

                    <button id="my-modal-button-delete" class="btn btn-danger">OK</button>

                </div>

            </div>

        </div>

    </div>

    <!-- END Delete User Modal-->





    <!-- END Delete User Confirmation Modal-->







    <script>



        document.addEventListener('DOMContentLoaded', function(){



            const buttons = document.querySelectorAll('.my-delete-button');

            const modal = document.querySelector('#deleteModal')



            const modalButton = document.querySelector('#my-modal-button');

            const modalButtonDelete = document.querySelector('#my-modal-button-delete');



            const count = buttons.length;



            let form;



            for(let i=0; i<count; i++) {





                buttons[i].addEventListener('click', function(e) {

                    e.preventDefault();

                    form = buttons[i].parentElement;

                    console.log(form);

                    modalButton.click();

                });



            }





            modalButtonDelete.addEventListener('click', function(){

                form.submit();

            })









        });



    </script>





@endsection

