@extends('layouts.admin')
@section('title') Edit Product @endsection
{{--@section('breadcrumbs', Breadcrumbs::render('admin.dashboard.products.edit', $product))--}}
@section('content')
    <h1>Редактировать продукт {{$product->name}}</h1>
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="row">
                <div class="col-lg-4">
                    @if(!empty($product->icon))
                        <h4>Фото</h4>
                        <div class="row">
                                @foreach (json_decode($product->icon) as $photo)
                                    <?$product_id = App\Photo::find($photo);
                                        if(!empty($product_id->file)) {
                                            $img_src = url('/').$product_id->file;
                                        } else {
                                            $img_src = 'https://pngimage.net/wp-content/uploads/2018/05/default-png-6.png';
                                        }
                                        ?>
                                    <img src="{{$img_src}}" alt="" class="border rounded" style="max-width: 200px; max-height: 200px;">
                                @endforeach
                        </div>
                    @endif
                </div>


                <div class="col-lg-8">
                    {!! Form::model($product, ['method'=>'PATCH','action' => ['Admin\AdminTProductsController@update', $product->id], 'files' => true,'enctype'=>'multipart/form-data']) !!}

                    <div class= "form-group" >
                        {!! Form::label('name', 'Имя *') !!}
                        {!! Form::text('name', null, ['class' => $errors->has('name') ? 'form-control is-invalid' : 'form-control']) !!}
                        @if ($errors->has('name'))
                            {{--{{ dd($errors->all()) }}--}}
                            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                        @endif
                    </div>


                    <div class="form-group">
                        {!! Form::label('product_id', 'Товар *') !!}
                        {!! Form::select('product_id', array(''=>'Choose Option') + $products , null,  ['id'=>'my-category','class' => $errors->has('product_id') ? 'form-control is-invalid' : 'form-control']) !!}
                        @if ($errors->has('product_id'))
                            <div class="invalid-feedback">{{ $errors->first('product_id') }}</div>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::label('size', 'Размер *') !!}
                        {!! Form::select('size', array(''=>'Choose Option') + $size , null,  ['id'=>'my-category','class' => $errors->has('size') ? 'form-control is-invalid' : 'form-control']) !!}
                        @if ($errors->has('size'))
                            <div class="invalid-feedback">{{ $errors->first('size') }}</div>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::label('icon', 'Фотография ') !!}
                        {!! Form::file('icon[]', ['multiple'=>true, 'class' => $errors->has('icon') ? 'form-control is-invalid' : 'form-control']) !!}
                        @if ($errors->has('icon'))
                            <div class="invalid-feedback">{{ $errors->first('icon') }}</div>
                        @endif
                    </div>

                    <div class= "form-group" >
                        {!! Form::label('price', 'Цена') !!}
                        {!! Form::number('price', old('price'), ['class' => $errors->has('price') ? 'form-control is-invalid' : 'form-control']) !!}
                        @if ($errors->has('price'))
                            <div class="invalid-feedback">{{ $errors->first('price2') }}</div>
                        @endif
                    </div>



                    <div class= "form-group" >
                        {!! Form::label('mass', 'Масса') !!}
                        {!! Form::number('mass', null, ['class' => $errors->has('mass') ? 'form-control is-invalid' : 'form-control']) !!}
                        @if ($errors->has('mass'))
                            <div class="invalid-feedback">{{ $errors->first('mass') }}</div>
                        @endif
                    </div>

                    <!-- radio -->
                    <div class="form-group">
                        {!! Form::label('priority', 'Приоритет') !!}
                        {!! Form::checkbox('priority', 1 , true , ['class' => $errors->has('priority') ? ' is-invalid' : '']) !!}
                        @if ($errors->has('priority'))
                            <div class="invalid-feedback">{{ $errors->first('priority') }}</div>
                        @endif
                    </div>


                    <div class= "form-group" >
                        {!! Form::label('desc', 'Описание') !!}
                        {!! Form::textarea('desc', null, ['rows'=>3, 'class' => $errors->has('desc') ? 'form-control is-invalid' : 'form-control']) !!}
                        @if ($errors->has('desc'))
                            <div class="invalid-feedback">{{ $errors->first('desc') }}</div>
                        @endif
                    </div>




                    <div class="form-group">
                        {!! Form::submit('Обновить продукт', ['class'=>'btn btn-primary btn-block']) !!}
                    </div>
                    {!! Form::close() !!}


                    {!! Form::open(['method'=>'DELETE', 'action'=>['Admin\AdminTProductsController@destroy', $product->id]]) !!}
                    <div class="form-group">
                        {!! Form::submit('Удалить продукт', ['class'=>'btn btn-danger btn-block']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>





            </div>

        </div>

    </div>



@endsection





@section('modal')





















    <script>

        /**

         * Created by idgu on 10.12.2017.

         */



        function getSubcategories(categoryValue) {

            const subcategoriesSelect = document.querySelector('#my-subcategory');

            var xhr = new XMLHttpRequest();



            if (categoryValue !== '') {



                xhr.open('GET',  '{{ route('getSubcategories', null) }}' +'/' + categoryValue, true);



                xhr.onload = function() {

                    if (this.status == 200) {

                        subcategories = JSON.parse(this.responseText);

                        display = '<option value="" selected="selected">Choose Option</option>';

                        subcategories.forEach(function(subcategory) {

                            display += '<option value="'+ subcategory.id +'">' + subcategory.name +'</option>';

                        });



                        subcategoriesSelect.innerHTML = display;



                        if(window.first) {

                            let subcategoriesSelect = document.querySelector('#my-subcategory');

                            let options  = subcategoriesSelect.options;



                            for(var i=0, len = options.length; i < len; i++) {

                                if(options[i].value == {{ $product->subcategory_id }}) {

                                    options[i].selected = 'selected';

                                }

                            }



                            window.first = false;

                        }

                    }





                }

                xhr.send();

            } else {

                subcategoriesSelect.innerHTML = '<option value="" selected="selected">Choose Option</option>';

            }

        }







        document.addEventListener('DOMContentLoaded', function () {

            const category = document.querySelector('#my-category');

            window.first = true;

            getSubcategories(category.value);







            category.addEventListener('change', function () {

                getSubcategories(category.value);

            })

        });

    </script>







@endsection