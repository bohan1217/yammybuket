@extends('layouts.admin')
@section('title') Create Product @endsection
{{--@section('breadcrumbs', Breadcrumbs::render('admin.dashboard.products.create'))--}}
@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-6 mt-4">
            <h1>Создать продукт</h1>
            {!! Form::open(['method'=>'POST', 'action' => 'Admin\AdminTProductsController@store','files'=>true,'enctype'=>'multipart/form-data']) !!}


            <div class= "form-group" >
                {!! Form::label('name', 'Имя *') !!}
                {!! Form::text('name', null, ['class' => $errors->has('name') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('name'))
                    {{--{{ dd($errors->all()) }}--}}
                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                @endif
            </div>

            <div class="form-group">
                {!! Form::label('product_id', 'Товар *') !!}
                {!! Form::select('product_id', array(''=>'Choose Option') + $products , null,  ['id'=>'my-category','class' => $errors->has('product_id') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('product_id'))
                    <div class="invalid-feedback">{{ $errors->first('product_id') }}</div>
                @endif
            </div>

            <div class="form-group">
                {!! Form::label('size', 'Размер *') !!}
                {!! Form::select('size', array(''=>'Choose Option') + $size , null,  ['id'=>'my-category','class' => $errors->has('size') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('size'))
                    <div class="invalid-feedback">{{ $errors->first('size') }}</div>
                @endif
            </div>

            <hr>
            <div class="form-group">
                {!! Form::label('icon', 'Фотография ') !!}
                {!! Form::file('icon[]', ['multiple'=>true, 'class' => $errors->has('icon') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('icon'))
                    <div class="invalid-feedback">{{ $errors->first('icon') }}</div>
                @endif
            </div>

            <div class= "form-group" >
                {!! Form::label('price', 'Цена') !!}
                {!! Form::number('price', null, ['class' => $errors->has('price') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('price'))
                    <div class="invalid-feedback">{{ $errors->first('price') }}</div>
                @endif
            </div>

            <div class= "form-group" >
                {!! Form::label('mass', 'Масса') !!}
                {!! Form::number('mass', null, ['class' => $errors->has('mass') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('mass'))
                    <div class="invalid-feedback">{{ $errors->first('mass') }}</div>
                @endif
            </div>

            <!-- radio -->
            <div class="form-group">
                {!! Form::label('priority', 'Приоритет') !!}
                {!! Form::checkbox('priority', 1, true , ['class' => $errors->has('priority') ? ' is-invalid' : '']) !!}
            </div>

            <div class= "form-group" >
                {!! Form::label('desc', 'Описание') !!}
                {!! Form::textarea('desc', null, ['rows'=>3, 'class' => $errors->has('desc') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('desc'))
                    <div class="invalid-feedback">{{ $errors->first('desc') }}</div>
                @endif
            </div>


            <div class="form-group">
                {!! Form::token() !!}
                {!! Form::submit('Создать', ['class'=>'btn btn-primary btn-block']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection





@section('modal')





    <script>

        /**

         * Created by idgu on 10.12.2017.

         */



        function getSubcategories(categoryValue) {

            const subcategoriesSelect = document.querySelector('#my-subcategory');

            var xhr = new XMLHttpRequest();



            if (categoryValue !== '') {



                xhr.open('GET',  '{{ route('getSubcategories', null) }}' +'/' + categoryValue, true);



                xhr.onload = function() {

                    if (this.status == 200) {

                        subcategories = JSON.parse(this.responseText);

                        display = '<option value="" selected="selected">Choose Option</option>';

                        subcategories.forEach(function(subcategory) {

                            display += '<option value="'+ subcategory.id +'">' + subcategory.name +'</option>';

                        });



                        subcategoriesSelect.innerHTML = display;

                    }





                }

                xhr.send();

            } else {

                subcategoriesSelect.innerHTML = '<option value="" selected="selected">Choose Option</option>';

            }

        }







        document.addEventListener('DOMContentLoaded', function () {

            const category = document.querySelector('#my-category');



            getSubcategories(category.value);

            category.addEventListener('change', function () {

                getSubcategories(category.value);

            })

        });

    </script>







@endsection

