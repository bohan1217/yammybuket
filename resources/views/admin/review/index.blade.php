@extends('layouts.admin')
@section('title') отзывы @endsection
@section('content')
    <h1>Отзывы</h1>


    @include('includes.success')

    <div class="row justify-content-center">
        <div class="col-md-3">
            <h1 class="text-center">Создать отзыв</h1>
            {!! Form::open(['method'=>'POST', 'action'=>'Admin\AdminReviewsController@store','files'=>true,'enctype'=>'multipart/form-data']) !!}

            <div class="form-group">
                {!! Form::label('video', 'Youtube') !!}
                {!! Form::text('video', null, ['class' => 'form-control']) !!}
            </div>


            <div class="form-group">
                {!! Form::label('image', 'Фото') !!}
                {!! Form::file('image', ['class' => $errors->has('image') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('image'))
                    <div class="invalid-feedback">{{ $errors->first('image') }}</div>
                @endif
            </div>


            <div class="form-group">
                {!! Form::submit('Создать отзыв', ['class'=>'btn btn-block btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>


    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Отзывы</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Youtube</th>
                        <th>Фото</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Youtube</th>
                        <th>Фото</th>
                        <th>Действия</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @if($reviews)
                        @foreach($reviews as $review)
                            <tr>
                                <td>{{$review -> id}}</td>
                                <td>{{$review->video }}</td>
                                <?$product_id = App\Photo::find($review->image)?>
                                <td>
                                    @if(!empty($product_id->file))
                                        <img src="{{url('/')}}{{$product_id->file}}" alt="" class="imgProductsList">
                                    @endif
                                </td>
                                <td>
                                    {!! Form::open(['method'=>'DELETE', 'action'=>['Admin\AdminReviewsController@destroy', $review->id], 'class'=>'delete-form d-inline']) !!}
                                    <button type="submit" title="Delete question" class="ml-1 my-delete-button btn btn-small btn-danger d-inline-block"><i class="fa fa-lg fa-trash-o"></i></button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <!-- Delete User Modal-->
    <!-- Delete User Modal-->
    <a class="nav-link" data-toggle="modal" data-target="#deleteModal" style="display: none" id="my-modal-button">
        <i class="fa fa-fw fa-power-off"></i>Logout
    </a>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Ready to do this?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "OK" below if you are ready to do chosen action.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button id="my-modal-button-delete" class="btn btn-danger">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Delete User Modal-->





    <!-- END Delete User Confirmation Modal-->







    <script>



        document.addEventListener('DOMContentLoaded', function(){



            const buttons = document.querySelectorAll('.my-delete-button');

            const modal = document.querySelector('#deleteModal')



            const modalButton = document.querySelector('#my-modal-button');

            const modalButtonDelete = document.querySelector('#my-modal-button-delete');



            const count = buttons.length;



            let form;



            for(let i=0; i<count; i++) {





                buttons[i].addEventListener('click', function(e) {

                    e.preventDefault();

                    form = buttons[i].parentElement;

                    console.log(form);

                    modalButton.click();

                });



            }





            modalButtonDelete.addEventListener('click', function(){

                form.submit();

            })









        });



    </script>





@endsection

