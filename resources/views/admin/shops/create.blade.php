@extends('layouts.admin')
@section('title') Создать магазин @endsection
{{--@section('breadcrumbs', Breadcrumbs::render('admin.dashboard.products.create'))--}}
@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-6 mt-4">
            <h1>Создать магазин</h1>
            {!! Form::open(['method'=>'POST', 'action' => 'Admin\AdminShopsController@store','files'=>true]) !!}


            <div class= "form-group" >
                {!! Form::label('name', 'Имя *') !!}
                {!! Form::text('name', null, ['class' => $errors->has('name') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('name'))
                    {{--{{ dd($errors->all()) }}--}}
                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                @endif
            </div>


            <div class= "form-group" >
                {!! Form::label('address', 'Улица') !!}
                {!! Form::text('address', null, ['class' => $errors->has('address') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('address'))
                    <div class="invalid-feedback">{{ $errors->first('address') }}</div>
                @endif
            </div>

            <div class= "form-group" >
                {!! Form::label('lat', 'LAT') !!}
                {!! Form::text('lat', null, ['class' => $errors->has('lat') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('lat'))
                    <div class="invalid-feedback">{{ $errors->first('lat') }}</div>
                @endif
            </div>

            <div class= "form-group" >
                {!! Form::label('lng', 'LNG') !!}
                {!! Form::text('lng', null, ['class' => $errors->has('lng') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('lng'))
                    <div class="invalid-feedback">{{ $errors->first('lng') }}</div>
                @endif
            </div>


            <div class="form-group">
                {!! Form::token() !!}
                {!! Form::submit('Создать', ['class'=>'btn btn-primary btn-block']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection





@section('modal')





    <script>

        /**

         * Created by idgu on 10.12.2017.

         */



        function getSubcategories(categoryValue) {

            const subcategoriesSelect = document.querySelector('#my-subcategory');

            var xhr = new XMLHttpRequest();



            if (categoryValue !== '') {



                xhr.open('GET',  '{{ route('getSubcategories', null) }}' +'/' + categoryValue, true);



                xhr.onload = function() {

                    if (this.status == 200) {

                        subcategories = JSON.parse(this.responseText);

                        display = '<option value="" selected="selected">Choose Option</option>';

                        subcategories.forEach(function(subcategory) {

                            display += '<option value="'+ subcategory.id +'">' + subcategory.name +'</option>';

                        });



                        subcategoriesSelect.innerHTML = display;

                    }





                }

                xhr.send();

            } else {

                subcategoriesSelect.innerHTML = '<option value="" selected="selected">Choose Option</option>';

            }

        }







        document.addEventListener('DOMContentLoaded', function () {

            const category = document.querySelector('#my-category');



            getSubcategories(category.value);

            category.addEventListener('change', function () {

                getSubcategories(category.value);

            })

        });

    </script>







@endsection

