@extends('layouts.admin')
@section('title') Настройки сайта @endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Настройки сайта</h1>
            {!! Form::model($text, ['method'=>'PATCH', 'action'=>['Admin\AdminSettingsController@update', $text->id]]) !!}
            <div class="form-group">
                {!! Form::label('slider', 'Слайдер текст') !!}
                {!! Form::textarea('slider', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>

            <h4>Блок 1</h4>
            <hr>
            <div class="form-group">
                {!! Form::label('title2', 'Заголовок') !!}
                {!! Form::text('title2', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('text2', 'Описание:') !!}
                {!! Form::textarea('text2', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>


            <h4>Блок 2</h4>
            <hr>
            <div class="form-group">
                {!! Form::label('title3', 'Заголовок') !!}
                {!! Form::text('title3', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('text3', 'Описание: ') !!}
                {!! Form::textarea('text3', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>


            <h4>Блок 3</h4>
            <hr>
            <div class="form-group">
                {!! Form::label('title4', 'Заголовок') !!}
                {!! Form::text('title4', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('text4', 'Описание: ') !!}
                {!! Form::textarea('text4', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>

            <h4>Блок 4</h4>
            <hr>
            <div class="form-group">
                {!! Form::label('title5', 'Заголовок') !!}
                {!! Form::text('title5', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('text5', 'Описание: ') !!}
                {!! Form::textarea('text5', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>


            <h4>Блок 5</h4>
            <hr>
            <div class="form-group">
                {!! Form::label('title6', 'Заголовок') !!}
                {!! Form::text('title6', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('text6', 'Описание: ') !!}
                {!! Form::textarea('text6', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>


            <h4>Блок 6</h4>
            <hr>
            <div class="form-group">
                {!! Form::label('title7', 'Заголовок') !!}
                {!! Form::text('title7', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('text7', 'Описание: ') !!}
                {!! Form::textarea('text7', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>


            <h4>Обработка персональных данных</h4>
            <hr>
            <div class="form-group">
                {!! Form::label('privacy', 'Обработка персональных данных: ') !!}
                {!! Form::textarea('privacy', null, ['class' => 'form-control form-no-ckeditor']) !!}
            </div>


            <div class="form-group">
                {!! Form::submit('Обновить', ['class'=>'btn btn-primary btn-block']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection