@extends('layouts.admin')
@section('title') Products @endsection
@section('content')
    <h1>Настройки сайта</h1>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Настройки сайта</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Слайдер</th>
                        <th>Блок1 заголовок</th>
                        <th>Блок1 описание</th>
                        <th>Блок2 заголовок</th>
                        <th>Блок2 описание</th>
                        <th>Блок3 заголовок</th>
                        <th>Блок3 описание</th>
                        <th>Блок4 заголовок</th>
                        <th>Блок4 описание</th>


                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Слайдер</th>
                        <th>Блок1 заголовок</th>
                        <th>Блок1 описание</th>
                        <th>Блок2 заголовок</th>
                        <th>Блок2 описание</th>
                        <th>Блок3 заголовок</th>
                        <th>Блок3 описание</th>
                        <th>Блок4 заголовок</th>
                        <th>Блок4 описание</th>



                        <th>Действие</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @if($text)
                        @foreach($text as $item)
                            <tr>
                                <td>{!! $item->slider !!}</td>

                                <td>{{ $item->title2 }}</td>
                                <td>{!! $item->text2 !!}</td>

                                <td>{{ $item->title3 }}</td>
                                <td>{!! $item->text3 !!}</td>

                                <td>{{ $item->title4 }}</td>
                                <td>{!! $item->text4 !!}</td>

                                <td>{{ $item->title5 }}</td>
                                <td>{!! $item->text5 !!}</td>
                                <td>
                                    <a class="ml-1 " href="{{ route('admin.settings.edit', $item->id) }}" title="Edit category"><button class="btn btn-warning right d-inline-block"><i class="fa fa-edit fa-lg"></i></button></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <!-- Delete User Modal-->
    <!-- Delete User Modal-->
    <a class="nav-link" data-toggle="modal" data-target="#deleteModal" style="display: none" id="my-modal-button">
        <i class="fa fa-fw fa-power-off"></i>Logout
    </a>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Ready to do this?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "OK" below if you are ready to do chosen action.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button id="my-modal-button-delete" class="btn btn-danger">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Delete User Modal-->





    <!-- END Delete User Confirmation Modal-->







    <script>



        document.addEventListener('DOMContentLoaded', function(){



            const buttons = document.querySelectorAll('.my-delete-button');

            const modal = document.querySelector('#deleteModal')



            const modalButton = document.querySelector('#my-modal-button');

            const modalButtonDelete = document.querySelector('#my-modal-button-delete');



            const count = buttons.length;



            let form;



            for(let i=0; i<count; i++) {





                buttons[i].addEventListener('click', function(e) {

                    e.preventDefault();

                    form = buttons[i].parentElement;

                    console.log(form);

                    modalButton.click();

                });



            }





            modalButtonDelete.addEventListener('click', function(){

                form.submit();

            })









        });



    </script>





@endsection

