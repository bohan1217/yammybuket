@extends('layouts.admin')
@section('title') Users @endsection
{{--@section('breadcrumbs', Breadcrumbs::render('admin.dashboard.orders'))--}}
@section('content')
    <h1>Orders</h1>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Orders list</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Имя и Телефон заказчика</th>
                        <th>Имя и Телефон получателя</th>
                        <th>Цена</th>
                        <th>Товары</th>
                        <th>Доставка</th>
                        <th>Сообщение</th>
                        <th>Создано в</th>
                        <th>Удалить</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Имя и Телефон заказчика</th>
                        <th>Имя и Телефон получателя</th>
                        <th>Цена</th>
                        <th>Товары</th>
                        <th>Доставка</th>
                        <th>Сообщение</th>
                        <th>Создано в</th>
                        <th>Удалить</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @if($orders)
                        @foreach($orders as $order)
                            <tr>
                                <td>{{ $order->id }}</td>
                                <td>
                                    {{ $order->name }} <br>
                                    {{ $order->phone }}
                                </td>
                                <td>
                                    {{ $order->name2 }} <br>
                                    {{ $order->phone2 }}
                                </td>
                                <td>{{ $order->totalAmount/100 }} {{env("CURRENCY_PRICE", "₽")}}</td>
                                <td>
                                   @if(!empty($order->products))
                                        @foreach($order->products as $product)
                                            @if(!empty($product['product']))
                                                <?
                                                $product_id = App\Photo::find(json_decode($product['product']->icon)[0]);
                                                if(!empty($product_id->file)) {
                                                    $img = $product_id->file;
                                                }
                                                if(empty($product_id->file)) {
                                                    $product_id = App\Photo::find($product['product']->photo);
                                                    $img = $product_id[0]->file;
                                                }
                                                ?>
                                                {{$product['qty']}} x {{$product['product']->name}}
                                                 @if(!empty($product['product']->size))
                                                     <?
                                                        $sizeN = App\Size::find($product['product']->size);
                                                        $sizeN = $sizeN->name;
                                                    ?>
                                                    Размер: {{$sizeN}}
                                                 @endif
                                                <img src="{{url('/')}}{{$img}}" alt="{{$product['product']->name . ' ' .$product['product']->name}}" title="{{$product['product']->name . ' ' .$product['product']->name}}" style="height: 40px; width: 40px;">
                                                <br>
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    <h6 style="color: red"> @if($order->delivery == 0) Доставка @else Самовывоз @endif</h6><br>
                                    @if($order->delivery == 0)
                                        @if(!empty($order->street)) Улица:   {{ $order->street }} <br> @endif
                                        @if(!empty($order->house)) Дом: {{ $order->house }} <br> @endif
                                        @if(!empty($order->flat)) Квартира:   {{ $order->flat }} <br> @endif
                                        @if(!empty($order->ascension)) Подъезд: {{ $order->ascension }} <br> @endif
                                    @endif
                                </td>

                                <td>{{ $order->message }}</td>
                                <td>{{ $order->created_at }}</td>

                                <td>
                                    {!! Form::open(['method'=>'DELETE', 'action'=>['Admin\AdminOrdersController@destroy', $order->id], 'class'=>'delete-form d-inline']) !!}
                                    <button type="submit" title="Delete question" class="ml-1 my-delete-button btn btn-small btn-danger d-inline-block"><i class="fa fa-lg fa-trash-o"></i></button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

