@extends('layouts.app')

@section('content')
    <script>
         window.location += "#products";
    </script>
    <main id="wrapper">
        <div class="basket-wrap">
            <section class="section -main">
                <div class="container">
                    @include('includes.navbar')
                    <div class="content">
                        <div class="row">
                            <div class="col-md-6 main__text">
                                        @if (!empty($texts[0]->slider))
                                            {!!$texts[0]->slider!!}
                                        @endif
                                <a href="#products" class="btn __scroll-link">Купить букет</a>
                            </div>
                            <div class="col-md-6 main__flowers">
                                <img class="main-pic" src="/assets/img/pic-main.png" alt="Большой букет маршмеллоу">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="decor -main"></div>
            </section>


            <br>
            @include('includes.cartProduct')
        </div>


        {{--footer--}}
        @include('includes.footer')
        {{--footer--}}

        <div class="basket-panel">
            <a href="tel:8-495-185-57-56" class="panel-link __phone-link">8-495-185-57-56</a>
            <a href="/cart" class="panel-link __basket-link basket_link @if($cart && $cart->totalPrice == 0) display_none @endif"><span class="my-total-price">@if($cart) {{$cart->totalPrice}} @else 0 @endif</span> {{env("CURRENCY_PRICE", "₽")}}</a>
        </div>

    </main>
@endsection