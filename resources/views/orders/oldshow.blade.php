@extends('layouts.app')

@section('content')
    <main id="wrapper" class="static-size">

        <div class="basket-wrap">

            <section class="section __header-theme">
                <div class="container">
                    @include('includes.navbar')
                </div>
            </section>

            @if(!empty($cart->items))
                <section class="section basket-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="basket-nav">
                                    <ul class="basket-nav-list">
                                        <li class="basket-nav-list-item active">
                                            <button class="tab-link basket-switch-tab active" data-id="0" data-enable-move="true">
                                                <span class="tab-link-icon">1</span>
                                                <span class="tab-link-title">Корзина</span>
                                            </button>
                                        </li>
                                        <li class="basket-nav-list-item">
                                            <button class="tab-link basket-switch-tab" data-id="1" data-enable-move="false">
                                                <span class="tab-link-icon">2</span>
                                                <span class="tab-link-title">Оформление заказа</span>
                                            </button>
                                        </li>
                                        <li class="basket-nav-list-item">
                                            <button class="tab-link basket-switch-tab" data-id="2" data-enable-move="false">
                                                <span class="tab-link-icon">3</span>
                                                <span class="tab-link-title">Заказ в пути</span>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            @endif



            <section class="section basket-carousel">
                <div class="basket-carousel-wrap" id="checkout">
                    @if(!empty($cart->items))
                        <div class="basket-carousel-slide">
                            <div class="container coupon">
                                <span class="error_result"></span>
                                @if(!empty($coupon->discount))
                                    <h3 class="coupon_h3">Ваш купон на -{{$coupon->discount}}%</h3>
                                    <br>
                                @endif
                            </div>
                            <div class="container">
                                <div class="basket-list">
                                    @foreach($cart->items as $item)
                                        <div class="basket-list-item my-tr" id="item_{{ $item['item']->id }}">
                                            <?
                                            $icon_id_el = 'icon_id'.$item['value'];
                                            $product_id = App\Photo::find(json_decode($item['item']->$icon_id_el)[0])
                                            ?>
                                            @if(!empty($item['item']->$icon_id_el))
                                                <div class="basket-list-item-photo">
                                                    <img src="{{$product_id->file}}" alt="">
                                                </div>
                                            @else
                                                <div class="basket-list-item-photo">
                                                    <img src="{{$item['item']->photo->file}}" alt="">
                                                </div>
                                            @endif
                                            <div class="basket-list-item-title">
                                                <p>{{$item['item']->name}}</p>

                                                @if($item['item']->category_id == 1)
                                                    <p class="cartSize">
                                                        Размер:
                                                        @if(($item['value'] == 1))
                                                            S
                                                        @elseif(($item['value'] == 2))
                                                            M
                                                        @elseif(($item['value'] == 3))
                                                            L
                                                        @endif
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="basket-list-item-count">
                                                <div class="count-wrap">
                                                    <button class="count-wrap-btn __minus my-take-button"></button>
                                                    <input data-id="{{ $item['item']->id }}" type="text" class="count-wrap-input my-qty-input" value="{{ $item['qty'] }}" readonly="readonly">
                                                    <button class="count-wrap-btn __plus my-add-button"></button>
                                                </div>
                                            </div>
                                            <div class="basket-list-item-price">
                                                <p><span class="my-total-items-price" data-id="{{ $item['item']->id }}">{{$item['price']}}</span> {{env("CURRENCY_PRICE", "₽")}}</p>
                                            </div>
                                            <div class="basket-list-item-action">
                                                <button class="remove-product my-del-button"></button>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <p class="text __delivery-info"><strong>Бесплатная доставка по Москве и в пределах МКАД</strong> осуществляется на следующий день после заказа. Возможна доставка день в день по согласованию. <strong>Стоимость доставки за пределы МКАД</strong> менеджер предварительно согласует с вами после получения заявки.
                                            <br>
                                            Оплата производится курьеру наличными или переводом на карту. </p>
                                    </div>
                                </div>

                                <div class="basket-total-wrap">
                                    <div class="basket-total-wrap-item">
                                        <a href="/" class="btn-link">Вернуться к выбору</a>
                                    </div>
                                    <form action="{{route('coupon.buy')}}" class="basket-total-wrap-item order_promocode">
                                        <div class="form-group">
                                            <label class="input-container">
                                                <input type="text" name="code" class="input-item" required>
                                                <span class="label-item"></span>
                                            </label>
                                            <button class="btn" type="submit">Промокод</button>
                                        </div>
                                    </form>
                                    <div class="basket-total-wrap-item">
                                        <p class="basket-total-wrap-title">Сумма заказа: <span class="my-total-price">{{$cart->totalPrice}}</span> {{env("CURRENCY_PRICE", "₽")}}</p>
                                        <button class="btn basket-switch-tab" data-id="1" data-enable-move="true"><strong>ОФОРМИТЬ ЗАКАЗ</strong></button>
                                    </div>
                                </div>
                                {{--openings-carousel-wrap--}}
                                @include('includes.carousel_navbar')
                            </div>
                        </div>


                        <div class="basket-carousel-slide">
                            <div class="container">
                                <div class="form-wrap basket-form-wrap">
                                    <form action="{{route('orders.buy')}}">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="input-container __not-required">
                                                    <input type="text" class="input-item" name="name">
                                                    <span class="label-item">Имя заказчика</span>
                                                    <span class="input-message">(Необязательно)</span>
                                                </label>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="input-container __not-required">
                                                    <input type="text" name="email" class="input-item">
                                                    <span class="label-item">E-mail</span>
                                                    <span class="input-message">(Необязательно)</span>
                                                </label>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="input-container">
                                                    <input type="text" class="input-item js-phoneInput" name="phone" data-required>
                                                    <span class="label-item">Телефон заказчика</span>
                                                    <span class="input-message">Поле обязятельное для заполения</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <p class="text __delivery-info">Вы можете указать только номер телефона и наш менеджер с вами свяжется для уточнения информации</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="input-group mt-4">
                                                    <label class="input-container __checkbox-theme">
                                                        <input type="radio" name="delivery" value="0" class="input-item __checlbox-item __delivery-switch" checked="checked">
                                                        <span class="label-item __checkbox-theme">Доставка</span>
                                                    </label>
                                                    <label class="input-container __checkbox-theme">
                                                        <input type="radio" name="delivery" value="1" class="input-item __checlbox-item __delivery-switch">
                                                        <span class="label-item __checkbox-theme">Самовывоз</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="for-delivery">

                                            <div class="row">
                                                <div class="col-12">
                                                    <label class="input-container">
                                                        <input type="text" class="input-item" name="street">
                                                        <span class="label-item">Улица</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="input-container">
                                                        <input type="text" class="input-item" name="house">
                                                        <span class="label-item">Дом</span>
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="input-container">
                                                        <input type="text" class="input-item" name="flat">
                                                        <span class="label-item">Квартира</span>
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="input-container">
                                                        <input type="text" class="input-item" name="ascension">
                                                        <span class="label-item">Подъезд</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <label class="input-container">
                                                        <input type="text" class="input-item" name="message">
                                                        <span class="label-item">Дополнительная информация</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>


                                        @if(!empty($shops))
                                            <div class="for-self-checkout">
                                                <div class="row">
                                                    <div class="col-12">
                                                        @foreach($shops as $shop)
                                                            <div class="contacts-list-item __info-theme">
                                                                <div class="contact-title">{{$shop->name}}</div>
                                                                <div class="contact-address">{{$shop->address}}</div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-action text-center">
                                                    <button class="btn">Заказать</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                        <div class="basket-carousel-slide">
                            <div class="container">
                                <div class="basket-thank-wrap">
                                    <h2 class="-header-gradient mb-3 title text-center">Спасибо!</h2>
                                    <p class="basket-thank-wrap-description">Ваш заказ принят в обработку. В ближайшее время с вами свяжется менеджер для подтверждения заказа!</p>
                                    <a href="/" class="btn">На главную</a>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="basket-carousel-slide">
                            <div class="container">
                                <div class="basket-thank-wrap">
                                    <p class="basket-thank-wrap-description">Ваша корзина пуста</p>
                                    <a href="/" class="btn">На главную</a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </section>


            {{--footer--}}
            @include('includes.footer')
            {{--footer--}}

            <div class="basket-panel">
                <a href="tel:8-495-185-57-56" class="panel-link __phone-link">8-495-185-57-56</a>
            </div>

        </div>
    </main>
@endsection



@section('scripts')
    <script>
        Element.prototype.remove = function() {
            this.parentElement.removeChild(this);
        }

        NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
            for(var i = this.length - 1; i >= 0; i--) {
                if(this[i] && this[i].parentElement) {
                    this[i].parentElement.removeChild(this[i]);
                }
            }
        }
        document.addEventListener('DOMContentLoaded', function () {
            function Cart() {
                this.totalQty = @if($cart) {{$cart->totalQty }} @endif ;
                this.totalPrice = @if(!empty($cart->totalPrice)) {{$cart->totalPrice }} @else 0 @endif ;
                this.totalQtyInputs = document.querySelectorAll('.my-total-qty');
                this.totalPriceInputs = document.querySelectorAll('.my-total-price');
                this.addButtons = document.querySelectorAll('.my-add-button');
                this.qtyInputs = document.querySelectorAll('.my-qty-input');
                this.takeButtons = document.querySelectorAll('.my-take-button');
                this.delButtons = document.querySelectorAll('.my-del-button');

                this.updateFields = function(items) {

                    this.totalItemsPriceInputs = document.querySelectorAll('.my-total-items-price');
                    for(let i=0, len = this.totalQtyInputs.length; i<len; i++) {
                        this.totalQtyInputs[i].innerHTML = this.totalQty;
                    }

                    for(let i=0, len = this.totalPriceInputs.length; i<len; i++) {
                        this.totalPriceInputs[i].innerHTML = this.totalPrice;
                    }

                    //console.log(this.totalItemsPriceInputs);

                    for(let i=0; i<this.totalItemsPriceInputs.length; i++) {
                        if(this.totalItemsPriceInputs[i].dataset.id in items ) {
                            this.totalItemsPriceInputs[i].innerHTML = items[this.totalItemsPriceInputs[i].dataset.id].price;
                        }

                        if (this.totalItemsPriceInputs[i]){
                            if (!items[this.totalItemsPriceInputs[i].dataset.id]) {
                                console.log('dzialam' + 'item_' + this.totalItemsPriceInputs[i].dataset.id);
                                document.getElementById('item_' + this.totalItemsPriceInputs[i].dataset.id).remove();
                                this.totalItemsPriceInputs[i] = 'jasiu';
                            }
                        }
                    }
                }

                this.updateCart = function(cart, qtyInput, qtyInputValue, items) {
                    this.totalQty = cart.totalQty;
                    this.totalPrice = cart.totalPrice;
                    qtyInput.value = (qtyInputValue > 0 )? qtyInputValue : 0;
                    this.updateFields(items);
                }

                this.addProductsToCart = function (id, qty, qtyInput) {
                    $('#loading').fadeIn('fast');
                    var xhr = new XMLHttpRequest();
                    xhr.open('GET', `https://yammybuket.ru/xhrAddProductsToCart/${id}/${qty}/`, true);
                    const self = this;
                    const qtyInputValue =  Number(qtyInput.value) + qty;
                    xhr.onload = function () {
                        if (this.status == 200) {
                            let cart = JSON.parse(this.responseText);
                            self.updateCart(cart, qtyInput, qtyInputValue, cart.items);
                            $('#loading').fadeOut('fast');
                        }
                    }
                    xhr.send();
                }

                this.deleteProductsFromCart = function (id, qty, qtyInput) {
                    $('#loading').fadeIn('fast');
                    var xhr = new XMLHttpRequest();
                    xhr.open('GET', `{{ route('products.xhrDeleteProductsFromCart',[null,null]) }}/${id}/${qty}`, true);
                    const self = this;
                    const qtyInputValue =  Number(qtyInput.value) - qty;
                    xhr.onload = function () {
                        if (this.status == 200) {
                            let cart = JSON.parse(this.responseText);
                            self.updateCart(cart, qtyInput, qtyInputValue,  cart.items);
                            $('#loading').fadeOut('fast');
                        }
                    }
                    xhr.send();
                }

                this.setButtons = function () {
                    for (let i = 0, len = this.addButtons.length; i < len; i++) {
                        this.addButtons[i].addEventListener('click', function () {
                            let id = this.qtyInputs[i].dataset.id;
                            this.addProductsToCart(id, 1, this.qtyInputs[i]);
                            this.addButtons[i].disabled = true;
                            setTimeout(function() {
                                this.addButtons[i].disabled = false;
                            }.bind(this), 1500);
                        }.bind(this));
                    }
                    for (let i = 0, len = this.takeButtons.length; i < len; i++) {
                        this.takeButtons[i].addEventListener('click', function () {
                            let id = this.qtyInputs[i].dataset.id;
                            //let id = 1;
                            this.deleteProductsFromCart(id, 1, this.qtyInputs[i]);
                            this.takeButtons[i].disabled = true;
                            setTimeout(function() {
                                this.takeButtons[i].disabled = false;
                            }.bind(this), 1500);
                        }.bind(this));
                    }

                    for (let i = 0, len = this.delButtons.length; i < len; i++) {
                        this.delButtons[i].addEventListener('click', function () {
                            let id = this.qtyInputs[i].dataset.id;
                            this.deleteProductsFromCart(id, this.qtyInputs[i].value, this.qtyInputs[i]);
                            this.delButtons[i].disabled = true;
                            $('#loading').fadeIn('fast');

                            setTimeout(function() {
                                this.delButtons[i].disabled = false;
                            }.bind(this), 1500);


                            let basket_counter = $('#total_qty');
                            $basket_counter = parseInt(basket_counter.text()-1);
                            if($basket_counter == 0){
                                $("#checkout").html('<div class="basket-carousel-slide"><div class="container"> <div class="basket-thank-wrap"><p class="basket-thank-wrap-description">Ваша корзина пуста</p> <a href="/" class="btn">На главную</a></div></div></div>');
                            }

                            $('#loading').fadeOut('fast');
                        }.bind(this));
                    }
                }
            }



            const cart = new Cart();
            cart.setButtons();
        });
    </script>
@endsection