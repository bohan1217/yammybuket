<!DOCTYPE html>
<html>
<head>
    <title>Yammy необычные сладкие букеты</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width" content="initial-scale=1">

    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="assets/css/hbmdev.css">
    <link rel="stylesheet" type="text/css" href="assets/css/preloader.css">

    <meta property="og:title" content="Yammy | Сладкие воздушные букеты из маршмэллоу и мармеладок">
    <meta property="og:description" content="Yammy | Вкусные необычные букеты из маршмеллоу и мармеладок">
    <meta property="og:image" content="/assets/img/pic-main.png" alt="Большой букет маршмеллоу">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://yammybuket.ru/">

    <link rel="shortcut icon" href="./assets/img/favicon.ico" type="image/x-icon">


    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5P7GQKZ');</script>
    <!-- End Google Tag Manager -->


</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5P7GQKZ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


@include('includes.preloader')

@yield('content')

@include('includes.modals')




<script type="text/javascript" src="assets/js/main.js"></script>

@yield('scripts')

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.js"></script>

<script type="text/javascript" src="assets/js/jquery.validate.js"></script>
<script type="text/javascript" src="assets/js/hbmdev.js"></script>



@if(Route::current()->getName() == 'mainpage')
    @if(!empty($shops))
    <script async src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script async type="text/javascript">
        ymaps.ready(init);
        var myMap;
        //отключаем зум колёсиком мышки
        function init() {
            var i;
            var place;
            var pointer = [
                @foreach($shops as $shop)
                    [{{$shop->lat}},{{$shop->lng}}],
                @endforeach
            ];

            var myMap = new ymaps.Map("map", {
                center: [55.888760,37.433526],
                zoom: 12
            });

            for(i = 0; i < pointer.length; ++i) {
                place = new ymaps.Placemark(pointer[i]);
                myMap.geoObjects.add(place);
            }
        };
    </script>
    @endif
@endif



</body>
</html>