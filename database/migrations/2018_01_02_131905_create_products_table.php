<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name'); // name

            $table->smallInteger('photo_id')->unsigned()->nullable(); //main photo
            $table->smallInteger('category_id')->unsigned()->nullable();

            $table->smallInteger('icon_id1')->unsigned()->nullable();
            $table->smallInteger('icon_id2')->unsigned()->nullable();
            $table->smallInteger('icon_id3')->unsigned()->nullable();


            $table->string('priority1')->nullable(); //priority
            $table->string('priority2')->nullable(); //priority
            $table->string('priority3')->nullable(); //priority

            $table->string('mass1')->nullable(); //mass маса
            $table->string('mass2')->nullable(); //mass маса
            $table->string('mass3')->nullable(); //mass маса

            $table->string('discount')->nullable(); //discount размер скидки

            $table->integer('price1')->nullable();
            $table->integer('price2')->nullable();
            $table->integer('price3')->nullable();


            $table->text('description1')->nullable();
            $table->text('description2')->nullable();
            $table->text('description3')->nullable();

            $table->date('released_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
