<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function($table) {
            $table->dropColumn('user_id');

            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->integer('delivery')->nullable();

            $table->string('name2')->nullable();
            $table->string('phone2')->nullable();
            $table->string('street')->nullable();
            $table->string('house')->nullable();
            $table->string('flat')->nullable();
            $table->string('ascension')->nullable();
            $table->text('message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function($table) {
            $table->dropColumn('user_id');

            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->integer('delivery')->nullable();

            $table->string('name2')->nullable();
            $table->string('phone2')->nullable();
            $table->string('street')->nullable();
            $table->string('house')->nullable();
            $table->string('flat')->nullable();
            $table->string('ascension')->nullable();
            $table->text('message')->nullable();
        });
    }
}
