<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTproductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tproducts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name'); // name
            $table->text('icon'); //main photo
            $table->smallInteger('product_id')->unsigned()->nullable();
            $table->integer('price')->nullable();
            $table->string('mass')->nullable(); //mass маса
            $table->string('priority')->nullable(); //priority
            $table->text('desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tproducts');
    }
}
