<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteFromCatalogAndAdd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function($table) {
            $table->dropColumn('icon_id1');
            $table->dropColumn('icon_id2');
            $table->dropColumn('icon_id3');

            $table->dropColumn('mass1');
            $table->dropColumn('mass2');
            $table->dropColumn('mass3');

            $table->dropColumn('price1');
            $table->dropColumn('price2');
            $table->dropColumn('price3');

            $table->dropColumn('description1');
            $table->dropColumn('description2');
            $table->dropColumn('description3');

            $table->dropColumn('priority');


            $table->integer('sort')->default(500);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function($table) {
            $table->dropColumn('icon_id1');
            $table->dropColumn('icon_id2');
            $table->dropColumn('icon_id3');

            $table->dropColumn('mass1');
            $table->dropColumn('mass2');
            $table->dropColumn('mass3');

            $table->dropColumn('price1');
            $table->dropColumn('price2');
            $table->dropColumn('price3');

            $table->dropColumn('description1');
            $table->dropColumn('description2');
            $table->dropColumn('description3');

            $table->dropColumn('priority');


            $table->integer('sort')->default(500);
        });
    }
}
