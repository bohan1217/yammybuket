<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('texts', function (Blueprint $table) {
            $table->increments('id');

            $table->text('slider')->nullable();

            $table->text('title2')->nullable();
            $table->text('text2')->nullable();

            $table->text('title3')->nullable();
            $table->text('text3')->nullable();

            $table->text('title4')->nullable();
            $table->text('text4')->nullable();

            $table->text('title5')->nullable();
            $table->text('text5')->nullable();

            $table->text('title6')->nullable();
            $table->text('text6')->nullable();

            $table->text('title7')->nullable();
            $table->text('text7')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('texts');
    }
}
