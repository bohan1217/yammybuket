# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.23)
# Database: yammybuket
# Generation Time: 2019-09-05 5:03:35 PM +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'Букеты','2019-06-10 17:39:26','2019-06-10 17:39:26'),
	(2,'Открытки','2019-06-17 17:53:19','2019-06-17 17:53:19');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table coupons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coupons`;

CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;

INSERT INTO `coupons` (`id`, `code`, `discount`, `end_date`, `created_at`, `updated_at`)
VALUES
	(3,'001-508566-758',10,NULL,'2019-06-14 13:04:37','2019-06-14 13:04:37');

/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table faqs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faqs`;

CREATE TABLE `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table feedback
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;

INSERT INTO `feedback` (`id`, `phone`, `created_at`, `updated_at`)
VALUES
	(1,'+7(372) 423-42-34','2019-06-12 22:53:03','2019-06-12 22:53:03'),
	(2,'+7(342) 432-34-32','2019-06-14 12:08:13','2019-06-14 12:08:13'),
	(3,'+7(656) 546-54-56','2019-06-17 17:56:18','2019-06-17 17:56:18'),
	(4,'+7(564) 565-46-46','2019-06-18 17:23:12','2019-06-18 17:23:12'),
	(5,'+7(574) 645-64-56','2019-06-18 17:23:30','2019-06-18 17:23:30'),
	(6,'+7(345) 435-43-53','2019-06-18 19:15:58','2019-06-18 19:15:58'),
	(7,'+7(345) 435-45-33','2019-06-18 19:18:08','2019-06-18 19:18:08');

/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` int(10) unsigned NOT NULL,
  `to` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2018_01_02_131905_create_products_table',1),
	(4,'2018_01_02_132136_create_categories_table',1),
	(5,'2018_01_02_132533_create_subcategories_table',1),
	(6,'2018_01_02_213025_create_roles_table',1),
	(7,'2018_01_06_000752_create_photos_table',1),
	(8,'2018_01_09_092104_create_faqs_table',1),
	(9,'2018_01_12_163545_create_orders_table',1),
	(10,'2019_06_01_101336_change_discoutn_type_in_table_product',1),
	(11,'2019_06_01_134942_delete_mig_priority',1),
	(12,'2019_06_08_151732_create_text_table',1),
	(13,'2019_06_10_092018_create_feedback_table',1),
	(14,'2019_06_10_133104_change_order_table',1),
	(15,'2019_06_10_175352_create_shops_table',2),
	(16,'2019_06_12_085801_create_texts_table',3),
	(17,'2019_06_13_163650_create_coupons_table',4),
	(18,'2019_06_14_122203_delete_from_users',5),
	(19,'2019_06_14_130527_add_email_orders',6),
	(20,'2019_06_14_131630_create_messages_table',7),
	(21,'2019_06_17_180351_update_product',8),
	(22,'2019_06_18_141645_create_reviews_table',9),
	(23,'2019_06_22_085215_update_table_text',10),
	(24,'2019_09_04_132037_delete_from_catalog_and_add',11),
	(25,'2019_09_04_140418_create_size_table',12),
	(26,'2019_09_04_143616_create_tproducts_table',13),
	(27,'2019_09_04_145414_add_t_size',14);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` int(11) DEFAULT NULL,
  `products` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery` int(11) DEFAULT NULL,
  `name2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ascension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;

INSERT INTO `orders` (`id`, `orderId`, `status`, `totalAmount`, `products`, `adress`, `created_at`, `updated_at`, `name`, `phone`, `delivery`, `name2`, `phone2`, `street`, `house`, `flat`, `ascension`, `message`, `email`)
VALUES
	(30,'5d713c3bafd1d','PENDING',370000,'12-2-1|13-1-2|11-1-1','___','2019-09-05 16:47:55','2019-09-05 16:47:55','Богдан','+7(506) 612-00-4',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'testswipeks@yandex.ru');

/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table photos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `photos`;

CREATE TABLE `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;

INSERT INTO `photos` (`id`, `file`, `created_at`, `updated_at`)
VALUES
	(104,'icon_1560793165card_bouquet2.png','2019-06-17 17:39:25','2019-06-17 17:39:25'),
	(105,'icon_1560793165card_bouquet3.png','2019-06-17 17:39:25','2019-06-17 17:39:25'),
	(106,'icon_1560793165card_bouquet4.png','2019-06-17 17:39:25','2019-06-17 17:39:25'),
	(107,'icon_1560793165card_bouquet5.png','2019-06-17 17:39:25','2019-06-17 17:39:25'),
	(109,'icon_1560793561card_bouquet3.png','2019-06-17 17:46:01','2019-06-17 17:46:01'),
	(110,'icon_1560793561card_bouquet4.png','2019-06-17 17:46:01','2019-06-17 17:46:01'),
	(111,'icon_1560793561card_bouquet5.png','2019-06-17 17:46:01','2019-06-17 17:46:01'),
	(112,'icon_1560793561hands4.png','2019-06-17 17:46:01','2019-06-17 17:46:01'),
	(114,'15608705281.png','2019-06-18 15:08:48','2019-06-18 15:08:48'),
	(115,'15608709544.png','2019-06-18 15:15:54','2019-06-18 15:15:54'),
	(116,'15608709654.png','2019-06-18 15:16:05','2019-06-18 15:16:05'),
	(117,'15608710493.png','2019-06-18 15:17:29','2019-06-18 15:17:29'),
	(118,'15608711103.png','2019-06-18 15:18:30','2019-06-18 15:18:30'),
	(119,'1560871133card_bouquet4.png','2019-06-18 15:18:53','2019-06-18 15:18:53'),
	(120,'1560872074Радуга.png','2019-06-18 15:34:34','2019-06-18 15:34:34'),
	(121,'1560872415Фламинго.png','2019-06-18 15:40:15','2019-06-18 15:40:15'),
	(122,'1560883007photo_2019-06-18_20-58-19.jpg','2019-06-18 18:36:48','2019-06-18 18:36:48'),
	(123,'1560883017photo_2019-06-18_20-58-20.jpg','2019-06-18 18:36:57','2019-06-18 18:36:57'),
	(124,'1560883021photo_2019-06-18_20-58-21.jpg','2019-06-18 18:37:01','2019-06-18 18:37:01'),
	(125,'1560883025photo_2019-06-18_20-58-22 (2).jpg','2019-06-18 18:37:05','2019-06-18 18:37:05'),
	(126,'1560883029photo_2019-06-18_20-58-22.jpg','2019-06-18 18:37:09','2019-06-18 18:37:09'),
	(127,'1560883085IMG_2595.MOV','2019-06-18 18:38:05','2019-06-18 18:38:05'),
	(133,'icon_1560887672Бэлла f2a0b4.png','2019-06-18 19:54:32','2019-06-18 19:54:32'),
	(134,'icon_1560887867Бэлла f2a0b4.png','2019-06-18 19:57:47','2019-06-18 19:57:47'),
	(135,'icon_1560888063Бэлла f2a0b4.png','2019-06-18 20:01:03','2019-06-18 20:01:03'),
	(137,'icon_1560889042f2a0b4 бэлла.jpg','2019-06-18 20:17:23','2019-06-18 20:17:23'),
	(138,'1560890703a8d1f1 Азуро — копия.jpg','2019-06-18 20:33:43','2019-06-18 20:45:03'),
	(139,'icon_1560890076a8d1f1 Азуро — копия.jpg','2019-06-18 20:34:36','2019-06-18 20:34:36'),
	(140,'icon_1560890703a8d1f1 Азуро — копия.jpg','2019-06-18 20:45:03','2019-06-18 20:45:03'),
	(141,'1560892022card_bouquet4.png','2019-06-18 21:07:03','2019-06-18 21:07:03'),
	(142,'1560939690f2a0b4 бэлла.jpg','2019-06-19 10:21:30','2019-06-19 10:21:30'),
	(143,'icon_1560939690f2a0b4 бэлла.jpg','2019-06-19 10:21:30','2019-06-19 10:21:30'),
	(144,'icon_1560939906IMG_1420.HEIC','2019-06-19 10:25:06','2019-06-19 10:25:06'),
	(145,'icon_1560940752Белла М.HEIC','2019-06-19 10:39:13','2019-06-19 10:39:13'),
	(146,'icon_1560940752Белла L.HEIC','2019-06-19 10:39:13','2019-06-19 10:39:13'),
	(147,'1560945818f2f3b3 Подсолнухи.png','2019-06-19 12:03:38','2019-06-19 12:03:38'),
	(148,'1560946051d8e6d4 верано.jpg','2019-06-19 12:05:28','2019-06-19 12:07:31'),
	(149,'1560946332амур f8daf8.jpg','2019-06-19 12:09:20','2019-06-19 12:12:12'),
	(150,'1560946410eeef8f лето.png','2019-06-19 12:13:30','2019-06-19 12:13:30'),
	(151,'1561059566фламинго f899bd.png','2019-06-19 12:15:15','2019-06-20 19:39:26'),
	(152,'1561034820d4a5f2 индиг33333о.jpg','2019-06-19 12:16:28','2019-06-20 12:47:00'),
	(153,'1561056749e9c76b.jpg','2019-06-19 12:19:22','2019-06-20 18:52:29'),
	(154,'1560946821c3d5f6 гармония.jpg','2019-06-19 12:20:21','2019-06-19 12:20:21'),
	(155,'1560946927лаванда e5d2f2.jpg','2019-06-19 12:22:07','2019-06-19 12:22:07'),
	(156,'1561045799b3f4ac.png','2019-06-19 12:23:38','2019-06-20 15:49:59'),
	(158,'1560947273e09eb4 сердце.jpg','2019-06-19 12:26:40','2019-06-19 12:27:53'),
	(159,'1561058405Фрэнки 30dbf4.png','2019-06-19 12:29:19','2019-06-20 19:20:05'),
	(160,'1560947426d7a167 бананза.png','2019-06-19 12:30:26','2019-06-19 12:30:26'),
	(161,'1561055245photo_2019-06-18_20-58-19.jpg','2019-06-20 18:27:25','2019-06-20 18:27:25'),
	(162,'1561055860photo_2019-06-18_20-58-19.jpg','2019-06-20 18:37:40','2019-06-20 18:37:40'),
	(163,'1561056184photo_2019-06-18_20-58-22.jpg','2019-06-20 18:43:04','2019-06-20 18:43:04'),
	(164,'icon_1561056948IMG_1331.jpeg','2019-06-20 18:55:48','2019-06-20 18:55:48'),
	(165,'icon_1561057033DH9B9121.jpg','2019-06-20 18:57:13','2019-06-20 18:57:13'),
	(166,'icon_1561057140DH22229B9121.jpg','2019-06-20 18:59:00','2019-06-20 18:59:00'),
	(167,'icon_1561057185DH22229B9121.jpg','2019-06-20 18:59:45','2019-06-20 18:59:45'),
	(168,'icon_1561057406IMG_1331.jpeg','2019-06-20 19:03:26','2019-06-20 19:03:26'),
	(169,'icon_1561057473DH9B9056.jpg','2019-06-20 19:04:33','2019-06-20 19:04:33'),
	(170,'icon_1561057543DH9B22229056.jpg','2019-06-20 19:05:43','2019-06-20 19:05:43'),
	(171,'icon_1561057643IMG_0959.jpeg','2019-06-20 19:07:23','2019-06-20 19:07:23'),
	(172,'icon_1561057643IMG_0973.jpeg','2019-06-20 19:07:23','2019-06-20 19:07:23'),
	(173,'icon_1561057643IMG_0973.jpeg','2019-06-20 19:07:23','2019-06-20 19:07:23'),
	(174,'icon_1561057670IMG_1181.jpeg','2019-06-20 19:07:50','2019-06-20 19:07:50'),
	(175,'icon_1561058144DH9B9195 — копия.jpg','2019-06-20 19:15:44','2019-06-20 19:15:44'),
	(176,'icon_1561058253IMG_1465.jpeg','2019-06-20 19:17:33','2019-06-20 19:17:33'),
	(177,'icon_1561058253IMG_1526.jpeg','2019-06-20 19:17:33','2019-06-20 19:17:33'),
	(178,'icon_1561058253IMG_1535.jpeg','2019-06-20 19:17:33','2019-06-20 19:17:33'),
	(179,'icon_1561058435IMG_1465.jpeg','2019-06-20 19:20:35','2019-06-20 19:20:35'),
	(180,'icon_1561058463IMG_1526.jpeg','2019-06-20 19:21:03','2019-06-20 19:21:03'),
	(181,'icon_1561058524IMG_1526.jpeg','2019-06-20 19:22:04','2019-06-20 19:22:04'),
	(182,'icon_1561058546IMG_1526 — копия.jpeg','2019-06-20 19:22:26','2019-06-20 19:22:26'),
	(183,'icon_1561058568IMG_1465.jpeg','2019-06-20 19:22:48','2019-06-20 19:22:48'),
	(184,'icon_1561058568IMG_1535.jpeg','2019-06-20 19:22:48','2019-06-20 19:22:48'),
	(185,'icon_1561058642IMG_1276.jpeg','2019-06-20 19:24:02','2019-06-20 19:24:02'),
	(186,'icon_1561058716DH9B9064 — копия.jpg','2019-06-20 19:25:16','2019-06-20 19:25:16'),
	(187,'icon_1561058778IMG_9859.JPG','2019-06-20 19:26:18','2019-06-20 19:26:18'),
	(188,'icon_1561058778IMG_9879.JPG','2019-06-20 19:26:18','2019-06-20 19:26:18'),
	(189,'icon_1561058965DH9B9130 — копия.jpg','2019-06-20 19:29:25','2019-06-20 19:29:25'),
	(190,'icon_1561059012IMG_1219.jpeg','2019-06-20 19:30:12','2019-06-20 19:30:12'),
	(191,'icon_1561059026IMG_1238.jpeg','2019-06-20 19:30:26','2019-06-20 19:30:26'),
	(192,'icon_1561059049IMG_1235.jpeg','2019-06-20 19:30:49','2019-06-20 19:30:49'),
	(193,'icon_1561059152IMG_1547.jpeg','2019-06-20 19:32:32','2019-06-20 19:32:32'),
	(194,'icon_1561059152IMG_1568.jpeg','2019-06-20 19:32:32','2019-06-20 19:32:32'),
	(195,'icon_1561059152IMG_1587.jpeg','2019-06-20 19:32:32','2019-06-20 19:32:32'),
	(196,'icon_1561059221IMG_1415.jpeg','2019-06-20 19:33:41','2019-06-20 19:33:41'),
	(197,'icon_1561059221IMG_1459.jpeg','2019-06-20 19:33:41','2019-06-20 19:33:41'),
	(198,'icon_1561059320IMG_0956 (1).jpeg','2019-06-20 19:35:20','2019-06-20 19:35:20'),
	(199,'icon_1561059320IMG_1006.jpeg','2019-06-20 19:35:20','2019-06-20 19:35:20'),
	(200,'icon_1561059320IMG_1159.jpeg','2019-06-20 19:35:20','2019-06-20 19:35:20'),
	(201,'icon_1561059377IMG_0956 (1).jpeg','2019-06-20 19:36:17','2019-06-20 19:36:17'),
	(202,'icon_1561059434IMG_1290.jpeg','2019-06-20 19:37:14','2019-06-20 19:37:14'),
	(203,'icon_1561059457IMG_1300.jpeg','2019-06-20 19:37:37','2019-06-20 19:37:37'),
	(204,'icon_1561059614DH9B9155.jpg','2019-06-20 19:40:14','2019-06-20 19:40:14'),
	(205,'icon_1561059614IMG_9859.JPG','2019-06-20 19:40:14','2019-06-20 19:40:14'),
	(206,'icon_1561059614IMG_9879.JPG','2019-06-20 19:40:14','2019-06-20 19:40:14'),
	(207,'icon_1561059662DH9B9155 — копия.jpg','2019-06-20 19:41:02','2019-06-20 19:41:02'),
	(210,'15671649433JfSzHAJSWqoAsR.jpg','2019-08-30 11:35:43','2019-08-30 11:35:43'),
	(212,'1567609375Z6Xnjjt8Gs7Jq5b.jpg','2019-09-04 15:02:55','2019-09-04 15:02:55'),
	(213,'1567609375G9O1UO229HUZlps.jpg','2019-09-04 15:02:55','2019-09-04 15:02:55'),
	(214,'1567609375sDTMuJxmvpGDK1f.jpg','2019-09-04 15:02:55','2019-09-04 15:02:55'),
	(215,'1567609463WBrcj2XIGpmIGyY.jpg','2019-09-04 15:04:23','2019-09-04 15:04:23'),
	(216,'1567609463fCRof4XEwDTBLVp.jpg','2019-09-04 15:04:23','2019-09-04 15:04:23'),
	(217,'156760946332JZhqWZ8ryuA8b.jpg','2019-09-04 15:04:23','2019-09-04 15:04:23'),
	(218,'1567609515X0Iyr0Kfr9qlwq8.jpg','2019-09-04 15:05:15','2019-09-04 15:05:15'),
	(219,'15676095153T2N7Hvr8b0UyU8.jpg','2019-09-04 15:05:15','2019-09-04 15:05:15'),
	(220,'1567609515IDAvEk9ZGjQ0P5P.jpg','2019-09-04 15:05:15','2019-09-04 15:05:15'),
	(221,'1567609563NWQoOhd6nbJmnLV.jpg','2019-09-04 15:06:03','2019-09-04 15:06:03'),
	(222,'1567609563Il3F6LswZ8qBlLf.jpg','2019-09-04 15:06:03','2019-09-04 15:06:03'),
	(223,'1567609563FQvMtr5mcjf3LDd.jpg','2019-09-04 15:06:03','2019-09-04 15:06:03'),
	(224,'156760956579WJhYG8GY6dcF5.jpg','2019-09-04 15:06:05','2019-09-04 15:06:05'),
	(225,'1567609565TCGoJx5NON5sR1h.jpg','2019-09-04 15:06:05','2019-09-04 15:06:05'),
	(226,'1567609565s5ODJYmODPHDLtX.jpg','2019-09-04 15:06:05','2019-09-04 15:06:05'),
	(227,'156760963038XUzF076wIjYD4.jpg','2019-09-04 15:07:10','2019-09-04 15:07:10'),
	(228,'1567609630LL3n3W5QCeO5612.jpg','2019-09-04 15:07:10','2019-09-04 15:07:10'),
	(229,'1567609630NKKtHkGWyE81iKq.jpg','2019-09-04 15:07:10','2019-09-04 15:07:10'),
	(230,'1567609652yrilW9Gbn6gbwFP.jpg','2019-09-04 15:07:32','2019-09-04 15:07:32'),
	(231,'1567609652aaRcgVLKLZMNziV.jpg','2019-09-04 15:07:32','2019-09-04 15:07:32'),
	(232,'1567609652ZVtcZz8oN9JDSMX.jpg','2019-09-04 15:07:32','2019-09-04 15:07:32'),
	(233,'1567609794UZClc9ozeSOUG3m.jpg','2019-09-04 15:09:54','2019-09-04 15:09:54'),
	(234,'1567609794nbEvuN1jWf8gBja.jpg','2019-09-04 15:09:54','2019-09-04 15:09:54'),
	(235,'15676097946gkAnTGbZrlO0ut.jpg','2019-09-04 15:09:54','2019-09-04 15:09:54'),
	(236,'1567609804QeB5wW741kZGCIh.jpg','2019-09-04 15:10:04','2019-09-04 15:10:04'),
	(237,'1567609804mZEInHvolZeaLfw.jpg','2019-09-04 15:10:04','2019-09-04 15:10:04'),
	(238,'1567609804tih2PYP4RL77kDk.jpg','2019-09-04 15:10:04','2019-09-04 15:10:04'),
	(239,'1567609806XLhXuvIQEjlyjv8.jpg','2019-09-04 15:10:06','2019-09-04 15:10:06'),
	(240,'1567609806skMunOu9Gv3FOrt.jpg','2019-09-04 15:10:06','2019-09-04 15:10:06'),
	(241,'1567609806DCv1oGomGP0aCwx.jpg','2019-09-04 15:10:06','2019-09-04 15:10:06'),
	(242,'1567609841nwY8s7qDB6wLPjd.jpg','2019-09-04 15:10:41','2019-09-04 15:10:41'),
	(243,'1567609841MdLl5geytunNwei.jpg','2019-09-04 15:10:41','2019-09-04 15:10:41'),
	(244,'15676098412MH4jHn4zDNBLRO.jpg','2019-09-04 15:10:41','2019-09-04 15:10:41'),
	(245,'15676098979p77oPgixCbARER.jpg','2019-09-04 15:11:37','2019-09-04 15:11:37'),
	(246,'15676098973Me35QXp4msTCLy.jpg','2019-09-04 15:11:37','2019-09-04 15:11:37'),
	(247,'1567609967cUjEPIzjzyy4oJU.jpg','2019-09-04 15:12:47','2019-09-04 15:12:47'),
	(248,'1567609967UyJwSq6rMahJVU8.jpg','2019-09-04 15:12:47','2019-09-04 15:12:47'),
	(249,'1567614346T0cFpDx9lugej5L.jpg','2019-09-04 16:25:46','2019-09-04 16:25:46'),
	(250,'1567614346cz51l6wF9T4dNQ6.jpg','2019-09-04 16:25:46','2019-09-04 16:25:46'),
	(251,'1567614346iAS1QHO0wvbN2oF.jpg','2019-09-04 16:25:46','2019-09-04 16:25:46'),
	(252,'1567629676SjLbBxTd4Q9k0Xu.jpg','2019-09-04 20:41:16','2019-09-04 20:41:16'),
	(253,'1567629676JLtY5xJGG0n7vaW.jpg','2019-09-04 20:41:16','2019-09-04 20:41:16'),
	(255,'1567696157NTL485uaOKD4Qsq.png','2019-09-05 15:09:17','2019-09-05 15:09:17'),
	(256,'1567696157Mlgex6b9zcQ2YCE.png','2019-09-05 15:09:17','2019-09-05 15:09:17'),
	(257,'1567699414TARrNQ6lRrbQ3YH.jpg','2019-09-05 16:03:34','2019-09-05 16:03:34'),
	(258,'1567699414M064ozdcdBNMiz1.jpg','2019-09-05 16:03:34','2019-09-05 16:03:34'),
	(259,'1567699481bgbJzUGyfQjjSef.jpg','2019-09-05 16:04:41','2019-09-05 16:04:41'),
	(260,'1567699653g4fzzpy3V8U9M6f.jpg','2019-09-05 16:07:33','2019-09-05 16:07:33'),
	(261,'1567699653LrQeL8s2XZu67Bt.jpg','2019-09-05 16:07:33','2019-09-05 16:07:33'),
	(262,'1567699653NwBRz6Gg0MSnb7X.jpg','2019-09-05 16:07:33','2019-09-05 16:07:33'),
	(263,'15676996530GsTayLsd8LyyCN.jpg','2019-09-05 16:07:33','2019-09-05 16:07:33'),
	(264,'15676996535hEYuN9MCgl4oFO.jpg','2019-09-05 16:07:33','2019-09-05 16:07:33'),
	(265,'15676996818V5mpAodaVmhNTX.jpg','2019-09-05 16:08:01','2019-09-05 16:08:01'),
	(266,'1567699681MwEonmb0F86h6uN.jpg','2019-09-05 16:08:01','2019-09-05 16:08:01'),
	(267,'15677019628f61gOGQwHZwjJ4.jpg','2019-09-05 16:46:02','2019-09-05 16:46:02'),
	(268,'1567701962T2zDM0tghrUIPDW.jpg','2019-09-05 16:46:02','2019-09-05 16:46:02'),
	(269,'1567702028W9dpd1xEbqBk8r1.jpg','2019-09-05 16:47:08','2019-09-05 16:47:08'),
	(270,'1567702028LThsvax9bydd8E6.jpg','2019-09-05 16:47:08','2019-09-05 16:47:08');

/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_id` smallint(5) unsigned DEFAULT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `released_on` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '500',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `name`, `photo_id`, `category_id`, `discount`, `released_on`, `created_at`, `updated_at`, `price`, `color`, `sort`)
VALUES
	(26,'Букет Азурро',138,1,NULL,NULL,'2019-06-18 20:33:43','2019-06-20 19:03:26',NULL,'#a8d1f1',500),
	(27,'Букет Белла',142,1,NULL,NULL,'2019-06-19 10:21:30','2019-06-20 19:41:45',NULL,'#f2a0b4',500),
	(28,'Букет  Подсолнухи',147,1,NULL,NULL,'2019-06-19 12:03:38','2019-06-20 19:42:09','2000','#f2f3b3',500),
	(29,'Букет Верано',148,1,NULL,NULL,'2019-06-19 12:05:28','2019-06-20 19:15:44',NULL,'#d8e6d4',500),
	(30,'Букет Амур',149,1,NULL,NULL,'2019-06-19 12:09:20','2019-06-20 18:59:45',NULL,'#f8daf8',500),
	(31,'Букет Лето',150,1,NULL,NULL,'2019-06-19 12:13:30','2019-06-20 19:31:16',NULL,'#eeef8f',500),
	(32,'Букет Фламинго',151,1,NULL,NULL,'2019-06-19 12:15:15','2019-06-20 19:50:47',NULL,'#f899bd',500),
	(33,'Букет Индиго',152,1,NULL,NULL,'2019-06-19 12:16:28','2019-06-20 19:41:02',NULL,'#e9a8f9',500),
	(34,'Букет Африка',153,1,NULL,NULL,'2019-06-19 12:19:22','2019-06-20 19:05:43',NULL,'#f4c963',500),
	(35,'Букет Гармония',154,1,NULL,NULL,'2019-06-19 12:20:21','2019-06-20 19:25:16',NULL,'#c3d5f6',500),
	(36,'Букет Лаванда',155,1,NULL,NULL,'2019-06-19 12:22:08','2019-06-20 19:29:25',NULL,'#ecd3f4',500),
	(37,'Букет Радуга',156,1,NULL,NULL,'2019-06-19 12:23:38','2019-06-20 19:32:32',NULL,'#b3f4ac',500),
	(39,'Букет Вэлинтайн',158,1,NULL,NULL,'2019-06-19 12:26:41','2019-06-20 19:24:02',NULL,'#e09eb4',500),
	(40,'Букет Виолетти',159,1,NULL,NULL,'2019-06-19 12:29:19','2019-06-20 19:22:48',NULL,'#30dbf4',500),
	(41,'Букет Бананза',160,1,NULL,NULL,'2019-06-19 12:30:26','2019-06-20 19:07:50',NULL,'#d7a167',500);

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table reviews
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reviews`;

CREATE TABLE `reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image` smallint(5) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;

INSERT INTO `reviews` (`id`, `video`, `image`, `created_at`, `updated_at`)
VALUES
	(33,'https://youtube.com/embed/hnxCtRbss9g',NULL,'2019-06-19 10:14:00','2019-06-19 10:14:00'),
	(35,NULL,162,'2019-06-20 18:37:40','2019-06-20 18:37:40'),
	(36,NULL,163,'2019-06-20 18:43:04','2019-06-20 18:43:04');

/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shops
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shops`;

CREATE TABLE `shops` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `shops` WRITE;
/*!40000 ALTER TABLE `shops` DISABLE KEYS */;

INSERT INTO `shops` (`id`, `name`, `address`, `lat`, `lng`, `created_at`, `updated_at`)
VALUES
	(1,'Самовывоз','Строгинский бульвар 4','55.804616','37.396617',NULL,'2019-06-18 17:50:07'),
	(2,'Самовывоз','улица Коккинаки д 4','55.804304','37.546138','2019-06-18 17:51:52','2019-06-18 17:51:52'),
	(3,'Самовывоз','ТЦ Лига, Ленинградское шоссе вл 5','55.888997','37.433046','2019-06-18 17:53:04','2019-06-18 17:53:27'),
	(4,'Самовывоз','Лихачёвский просп. д 64','55.931235','37.494153','2019-06-18 17:54:21','2019-06-18 17:54:21');

/*!40000 ALTER TABLE `shops` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table size
# ------------------------------------------------------------

DROP TABLE IF EXISTS `size`;

CREATE TABLE `size` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `size` WRITE;
/*!40000 ALTER TABLE `size` DISABLE KEYS */;

INSERT INTO `size` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(3,'S','2019-09-04 14:31:35','2019-09-04 14:31:35'),
	(4,'M','2019-09-04 14:31:39','2019-09-04 14:31:39'),
	(5,'L','2019-09-04 14:31:42','2019-09-04 14:31:42');

/*!40000 ALTER TABLE `size` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subcategories`;

CREATE TABLE `subcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subcategories_category_id_index` (`category_id`),
  CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table texts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `texts`;

CREATE TABLE `texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slider` text COLLATE utf8mb4_unicode_ci,
  `title2` text COLLATE utf8mb4_unicode_ci,
  `text2` text COLLATE utf8mb4_unicode_ci,
  `title3` text COLLATE utf8mb4_unicode_ci,
  `text3` text COLLATE utf8mb4_unicode_ci,
  `title4` text COLLATE utf8mb4_unicode_ci,
  `text4` text COLLATE utf8mb4_unicode_ci,
  `title5` text COLLATE utf8mb4_unicode_ci,
  `text5` text COLLATE utf8mb4_unicode_ci,
  `title6` text COLLATE utf8mb4_unicode_ci,
  `text6` text COLLATE utf8mb4_unicode_ci,
  `title7` text COLLATE utf8mb4_unicode_ci,
  `text7` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `privacy` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `texts` WRITE;
/*!40000 ALTER TABLE `texts` DISABLE KEYS */;

INSERT INTO `texts` (`id`, `slider`, `title2`, `text2`, `title3`, `text3`, `title4`, `text4`, `title5`, `text5`, `title6`, `text6`, `title7`, `text7`, `created_at`, `updated_at`, `privacy`)
VALUES
	(1,'<h1>Сладкие воздушные<br />\r\nбукеты из зефира<br />\r\nи мармеладок</h1>','Используем самые качественные ингридиенты','<p class=\"subtitle\"><span style=\"font-size:16px;\"><span style=\"line-height:115%\"><span new=\"\" roman=\"\" times=\"\">Наши букеты из маршмеллоу состоят из высококачественной продукции крупнейших европейских производителей, таких как Fini Sweets (Италия), Bulgari (Испания), Damel (Испания),&nbsp; Golmasa (Испания). Ведущие компании имеют огромный опыт и большую историю и продолжают традиции в кондитерском деле, имеют самые высокие международные оценки качества и безопасности пищевых продуктов, сертификаты IFS, BRC и ISO 9001: 2008. </span></span></span></p>\r\n\r\n<p class=\"subtitle\"><span style=\"font-size:16px;\"><span style=\"line-height:115%\"><span new=\"\" roman=\"\" times=\"\">Зефирные букеты всегда самого высшего качества и свежести, соблюдаются все условия хранения . Наше суфле остаётся мягким и свежим на протяжении долгого времени после изготовления букета. Мы собираем сладкие букеты согласно фитосанитарным нормам в перчатках и упаковываем в пищевую слюду за 2 часа до доставки, что позволяет гарантировать качество нашей продукции.</span></span></span></p>','Соблюдаем все санитарные нормы','<h4 class=\"subtitle\">Букеты собираются в перчатках и упаковываются в плёнку</h4>','Доставка и оплата  <br> по Москве и области','<p class=\"text\">Оплата курьеру <strong>наличными</strong> или <strong>переводом</strong> на карту. Доставка осуществляется на следующий день после заказа. Возможна доставка день в день по согласованию. Бесплатная доставка по Москве в пределах МКАД.</p>','Выбери свой букет','<p>Наши букеты настолько вкусно пахнут, что перед доставкой букета мы кормим курьера зефиром, т.к. есть шанс, что он его съест</p>','Полезные сладости – это не миф','<p><span style=\"font-size:14px;\"><span style=\"background:white\"><span style=\"line-height:115%\"><span new=\"\" roman=\"\" times=\"\"><span style=\"color:black\">Основа зефира &ndash; фруктово-ягодное пюре. Его готовят из яблок, груш, клюквы, черной смородины и других плодов. Все они богаты витаминами и минералами, органическими кислотами и пектином.&nbsp;</span></span></span></span><span style=\"line-height:150%\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"background:white\"><span style=\"line-height:150%\"><span new=\"\" roman=\"\" times=\"\"><span style=\"color:black\">Воздушный зефир не содержит ни грамма жира, он легко переваривается, поэтому такое лакомство &laquo;не нагружает&raquo; наш желудок, как пирожные и торты. А за счет содержания пектина или другого желирующего агента зефир даже улучшает пищеварение.</span></span></span></span></span></span></span></p>','Вы можете приобрести букет в одном из магазинов наших партнёров','<p>Для того чтобы стать партнёром оставьте свой номер, мы перезвоним и расскажем про условия</p>',NULL,'2019-06-19 14:18:26',NULL);

/*!40000 ALTER TABLE `texts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tproducts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tproducts`;

CREATE TABLE `tproducts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` text COLLATE utf8mb4_unicode_ci,
  `product_id` smallint(5) unsigned DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `mass` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `size` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `tproducts` WRITE;
/*!40000 ALTER TABLE `tproducts` DISABLE KEYS */;

INSERT INTO `tproducts` (`id`, `name`, `icon`, `product_id`, `price`, `mass`, `priority`, `desc`, `created_at`, `updated_at`, `size`)
VALUES
	(10,'Букет Азурро','[260,261,262,263,264]',26,500,'30','1','<p>Описание</p>','2019-09-05 16:07:33','2019-09-05 16:07:33',3),
	(11,'Букет Белла','[265,266]',27,1200,'690','1','<p>Описание</p>','2019-09-05 16:08:01','2019-09-05 16:08:01',5),
	(12,'Букет Подсолнухи','[267,268]',28,950,'30','1',NULL,'2019-09-05 16:46:02','2019-09-05 16:46:02',5),
	(13,'Букет Подсолнухи','[269,270]',28,600,'20',NULL,NULL,'2019-09-05 16:47:08','2019-09-05 16:47:08',4);

/*!40000 ALTER TABLE `tproducts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `name`, `surname`, `adress`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'admin@admin.com','Admin',NULL,NULL,NULL,'$2y$10$6C7ejF8FPb2OMR2buBQKbOmk1dL9l3b.pE8N9VALuXoZpjpqnArDe','YIEIsSyUbe3fvS0QafF72EhXziqfHoEgaO5WWvIBe0EnhhOEWYLRgM6ArKoU','2019-06-10 17:37:44','2019-06-10 17:37:44');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
