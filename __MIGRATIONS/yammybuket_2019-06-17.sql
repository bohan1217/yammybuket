# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.23)
# Database: yammybuket
# Generation Time: 2019-06-17 8:30:31 PM +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'Букеты','2019-06-10 17:39:26','2019-06-10 17:39:26'),
	(2,'Открытки','2019-06-17 17:53:19','2019-06-17 17:53:19');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table coupons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coupons`;

CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;

INSERT INTO `coupons` (`id`, `code`, `discount`, `end_date`, `created_at`, `updated_at`)
VALUES
	(3,'001-508566-758',10,NULL,'2019-06-14 13:04:37','2019-06-14 13:04:37');

/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table faqs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faqs`;

CREATE TABLE `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table feedback
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;

INSERT INTO `feedback` (`id`, `phone`, `created_at`, `updated_at`)
VALUES
	(1,'+7(372) 423-42-34','2019-06-12 22:53:03','2019-06-12 22:53:03'),
	(2,'+7(342) 432-34-32','2019-06-14 12:08:13','2019-06-14 12:08:13'),
	(3,'+7(656) 546-54-56','2019-06-17 17:56:18','2019-06-17 17:56:18');

/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` int(10) unsigned NOT NULL,
  `to` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2018_01_02_131905_create_products_table',1),
	(4,'2018_01_02_132136_create_categories_table',1),
	(5,'2018_01_02_132533_create_subcategories_table',1),
	(6,'2018_01_02_213025_create_roles_table',1),
	(7,'2018_01_06_000752_create_photos_table',1),
	(8,'2018_01_09_092104_create_faqs_table',1),
	(9,'2018_01_12_163545_create_orders_table',1),
	(10,'2019_06_01_101336_change_discoutn_type_in_table_product',1),
	(11,'2019_06_01_134942_delete_mig_priority',1),
	(12,'2019_06_08_151732_create_text_table',1),
	(13,'2019_06_10_092018_create_feedback_table',1),
	(14,'2019_06_10_133104_change_order_table',1),
	(15,'2019_06_10_175352_create_shops_table',2),
	(16,'2019_06_12_085801_create_texts_table',3),
	(17,'2019_06_13_163650_create_coupons_table',4),
	(18,'2019_06_14_122203_delete_from_users',5),
	(19,'2019_06_14_130527_add_email_orders',6),
	(20,'2019_06_14_131630_create_messages_table',7),
	(21,'2019_06_17_180351_update_product',8);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` int(11) DEFAULT NULL,
  `products` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery` int(11) DEFAULT NULL,
  `name2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ascension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;

INSERT INTO `orders` (`id`, `orderId`, `status`, `totalAmount`, `products`, `adress`, `created_at`, `updated_at`, `name`, `phone`, `delivery`, `name2`, `phone2`, `street`, `house`, `flat`, `ascension`, `message`, `email`)
VALUES
	(21,'5d07d24bca961','PENDING',192000,'18-4-2','___','2019-06-17 17:47:55','2019-06-17 17:47:55','test','+7(506) 612-00-4',1,NULL,NULL,'Яворницького 3а','46',NULL,NULL,NULL,'testswipeks@yandex.ru');

/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table photos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `photos`;

CREATE TABLE `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;

INSERT INTO `photos` (`id`, `file`, `created_at`, `updated_at`)
VALUES
	(103,'1560793521card_bouquet5.png','2019-06-17 17:39:25','2019-06-17 17:45:21'),
	(104,'icon_1560793165card_bouquet2.png','2019-06-17 17:39:25','2019-06-17 17:39:25'),
	(105,'icon_1560793165card_bouquet3.png','2019-06-17 17:39:25','2019-06-17 17:39:25'),
	(106,'icon_1560793165card_bouquet4.png','2019-06-17 17:39:25','2019-06-17 17:39:25'),
	(107,'icon_1560793165card_bouquet5.png','2019-06-17 17:39:25','2019-06-17 17:39:25'),
	(108,'1560793561clean-bg.png','2019-06-17 17:46:01','2019-06-17 17:46:01'),
	(109,'icon_1560793561card_bouquet3.png','2019-06-17 17:46:01','2019-06-17 17:46:01'),
	(110,'icon_1560793561card_bouquet4.png','2019-06-17 17:46:01','2019-06-17 17:46:01'),
	(111,'icon_1560793561card_bouquet5.png','2019-06-17 17:46:01','2019-06-17 17:46:01'),
	(112,'icon_1560793561hands4.png','2019-06-17 17:46:01','2019-06-17 17:46:01'),
	(113,'1560801659opening-1.png','2019-06-17 20:00:59','2019-06-17 20:00:59');

/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_id` smallint(5) unsigned DEFAULT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `icon_id1` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_id2` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_id3` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mass1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mass2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mass3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `price1` int(11) DEFAULT NULL,
  `price2` int(11) DEFAULT NULL,
  `price3` int(11) DEFAULT NULL,
  `description1` text COLLATE utf8mb4_unicode_ci,
  `description2` text COLLATE utf8mb4_unicode_ci,
  `description3` text COLLATE utf8mb4_unicode_ci,
  `released_on` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `name`, `photo_id`, `category_id`, `icon_id1`, `icon_id2`, `icon_id3`, `mass1`, `mass2`, `mass3`, `discount`, `price1`, `price2`, `price3`, `description1`, `description2`, `description3`, `released_on`, `created_at`, `updated_at`, `priority`, `price`, `color`)
VALUES
	(17,'Test',103,1,'[104,105,106,107]',NULL,NULL,'6000',NULL,NULL,10,200,NULL,NULL,'<p>Описание размера S</p>',NULL,NULL,NULL,'2019-06-17 17:39:25','2019-06-17 19:48:51',1,NULL,'#1745e6'),
	(18,'Test123',108,1,'[109,110,111]','[112]',NULL,'20002','200',NULL,20,600,1596,NULL,NULL,'<p>Описание размера M</p>',NULL,NULL,'2019-06-17 17:46:01','2019-06-17 17:55:46',2,NULL,NULL),
	(19,'test',113,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-06-17 20:00:59','2019-06-17 20:00:59',1,'500','#3b0f0f');

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shops
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shops`;

CREATE TABLE `shops` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `shops` WRITE;
/*!40000 ALTER TABLE `shops` DISABLE KEYS */;

INSERT INTO `shops` (`id`, `name`, `address`, `lat`, `lng`, `created_at`, `updated_at`)
VALUES
	(1,'Магазин «Один»','Чапаевский переулок 14','55.797159','37.517590',NULL,'2019-06-11 15:09:59');

/*!40000 ALTER TABLE `shops` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subcategories`;

CREATE TABLE `subcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subcategories_category_id_index` (`category_id`),
  CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table texts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `texts`;

CREATE TABLE `texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slider` text COLLATE utf8mb4_unicode_ci,
  `title2` text COLLATE utf8mb4_unicode_ci,
  `text2` text COLLATE utf8mb4_unicode_ci,
  `title3` text COLLATE utf8mb4_unicode_ci,
  `text3` text COLLATE utf8mb4_unicode_ci,
  `title4` text COLLATE utf8mb4_unicode_ci,
  `text4` text COLLATE utf8mb4_unicode_ci,
  `title5` text COLLATE utf8mb4_unicode_ci,
  `text5` text COLLATE utf8mb4_unicode_ci,
  `title6` text COLLATE utf8mb4_unicode_ci,
  `text6` text COLLATE utf8mb4_unicode_ci,
  `title7` text COLLATE utf8mb4_unicode_ci,
  `text7` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `texts` WRITE;
/*!40000 ALTER TABLE `texts` DISABLE KEYS */;

INSERT INTO `texts` (`id`, `slider`, `title2`, `text2`, `title3`, `text3`, `title4`, `text4`, `title5`, `text5`, `title6`, `text6`, `title7`, `text7`, `created_at`, `updated_at`)
VALUES
	(1,'<h1>Сладкие воздушные<br />\r\nбукеты из зефира<br />\r\nи мармеладок</h1>','Используем самые качественные ингридиенты','<h4 class=\"subtitle\">Ингридиенты поставляются к нам с заводов лучших производителей: Fini Sweets (Италия) и Bulgari (Испания)</h4>\r\n\r\n<p class=\"text\">Итальяснкая компания основанная ещё в 1880 году семьей Bulgari, члены которой по-прежнему продолжают традиции в кондитерском секторе. Благодрая тщательному и постоянному вниманию к качеству своей продукции, на сегодняшний день кампания имеет сертифкаты IFS, BRC и ISO 9001: 2008.</p>\r\n\r\n<p class=\"text\">Fini Sweets с более чем 40-летним опытом является крупнейшим испанским производителем и дистрибьютором сахаристых кондитерских изделий. Это ведущий бренд в Испании. Компания имеет самые высокие международные оценки качества и безопасности пищевых продуктов, сертификаты IFS (International Food Standard) и BRC (British Retail Consortium)</p>','Соблюдаем все санитарные нормы','<h4 class=\"subtitle\">Букеты собираются в перчатках и упаковываются в плёнку</h4>','Доставим вкусняшки <br> по Москве и области','<p class=\"text\">Оплата курьеру наличными или картой. Доставка осуществляется на следующий день после заказа. Возможна доставка день в день по согласованию. Бесплатная доставка по Москве в пределах МКАД.</p>','Выбери свой букет','<p>Наши букеты настолько вкусно пахнут, что перед доставкой букета мы кормим курьера зефиром, т.к. есть шанс, что он его съест</p>','Текстовый заголовок','<p class=\"text\">Оплата курьеру или на сайте, по вашему желанию. Доставка осуществляется на следующий день после заказа. Возможна доставка день в день по согласованию.</p>\r\n\r\n<p class=\"text\">Оплата курьеру или на сайте, по вашему желанию. Доставка осуществляется на следующий день после заказа. Возможна доставка день в день по согласованию.</p>','Вы можете приобрести букет в одном из магазинов наших партнёров','<p>Для того чтобы стать партнёром оставьте свой номер, мы перезвоним и расскажем про условия</p>',NULL,'2019-06-13 15:17:50');

/*!40000 ALTER TABLE `texts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `name`, `surname`, `adress`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'admin@admin.com','Admin',NULL,NULL,NULL,'$2y$10$6C7ejF8FPb2OMR2buBQKbOmk1dL9l3b.pE8N9VALuXoZpjpqnArDe','YIEIsSyUbe3fvS0QafF72EhXziqfHoEgaO5WWvIBe0EnhhOEWYLRgM6ArKoU','2019-06-10 17:37:44','2019-06-10 17:37:44');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
