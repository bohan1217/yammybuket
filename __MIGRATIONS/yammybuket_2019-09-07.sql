# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.23)
# Database: yammybuket
# Generation Time: 2019-09-07 7:53:56 PM +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'Букеты','2019-06-10 17:39:26','2019-06-10 17:39:26'),
	(2,'Открытки','2019-06-17 17:53:19','2019-06-17 17:53:19');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table coupons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coupons`;

CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;

INSERT INTO `coupons` (`id`, `code`, `discount`, `end_date`, `created_at`, `updated_at`)
VALUES
	(3,'001-508566-758',10,NULL,'2019-06-14 13:04:37','2019-06-14 13:04:37');

/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table faqs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faqs`;

CREATE TABLE `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table feedback
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;

INSERT INTO `feedback` (`id`, `phone`, `created_at`, `updated_at`)
VALUES
	(1,'+7(372) 423-42-34','2019-06-12 22:53:03','2019-06-12 22:53:03'),
	(2,'+7(342) 432-34-32','2019-06-14 12:08:13','2019-06-14 12:08:13'),
	(3,'+7(656) 546-54-56','2019-06-17 17:56:18','2019-06-17 17:56:18'),
	(4,'+7(564) 565-46-46','2019-06-18 17:23:12','2019-06-18 17:23:12'),
	(5,'+7(574) 645-64-56','2019-06-18 17:23:30','2019-06-18 17:23:30'),
	(6,'+7(345) 435-43-53','2019-06-18 19:15:58','2019-06-18 19:15:58');

/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` int(10) unsigned NOT NULL,
  `to` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2018_01_02_131905_create_products_table',1),
	(4,'2018_01_02_132136_create_categories_table',1),
	(5,'2018_01_02_132533_create_subcategories_table',1),
	(6,'2018_01_02_213025_create_roles_table',1),
	(7,'2018_01_06_000752_create_photos_table',1),
	(8,'2018_01_09_092104_create_faqs_table',1),
	(9,'2018_01_12_163545_create_orders_table',1),
	(10,'2019_06_01_101336_change_discoutn_type_in_table_product',1),
	(11,'2019_06_01_134942_delete_mig_priority',1),
	(12,'2019_06_08_151732_create_text_table',1),
	(13,'2019_06_10_092018_create_feedback_table',1),
	(14,'2019_06_10_133104_change_order_table',1),
	(15,'2019_06_10_175352_create_shops_table',2),
	(16,'2019_06_12_085801_create_texts_table',3),
	(17,'2019_06_13_163650_create_coupons_table',4),
	(18,'2019_06_14_122203_delete_from_users',5),
	(19,'2019_06_14_130527_add_email_orders',6),
	(20,'2019_06_14_131630_create_messages_table',7),
	(21,'2019_06_17_180351_update_product',8),
	(22,'2019_06_18_141645_create_reviews_table',9),
	(23,'2019_06_22_085215_update_table_text',10),
	(24,'2019_09_04_132037_delete_from_catalog_and_add',11),
	(25,'2019_09_04_140418_create_size_table',12),
	(26,'2019_09_04_143616_create_tproducts_table',13),
	(27,'2019_09_04_145414_add_t_size',14);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` int(11) DEFAULT NULL,
  `products` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery` int(11) DEFAULT NULL,
  `name2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ascension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;

INSERT INTO `orders` (`id`, `orderId`, `status`, `totalAmount`, `products`, `adress`, `created_at`, `updated_at`, `name`, `phone`, `delivery`, `name2`, `phone2`, `street`, `house`, `flat`, `ascension`, `message`, `email`)
VALUES
	(30,'5d713c3bafd1d','PENDING',370000,'12-2-1|13-1-2|11-1-1','___','2019-09-05 16:47:55','2019-09-05 16:47:55','Богдан','+7(506) 612-00-4',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'testswipeks@yandex.ru');

/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table photos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `photos`;

CREATE TABLE `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;

INSERT INTO `photos` (`id`, `file`, `created_at`, `updated_at`)
VALUES
	(10,'15678797641561055860photo_2019-06-18_20-58-19.jpg','2019-09-07 18:09:25','2019-09-07 18:09:25'),
	(11,'15678799071561056184photo_2019-06-18_20-58-22.jpg','2019-09-07 18:11:47','2019-09-07 18:11:47'),
	(13,'1567880867Z2oGCXvOaCP5VWo.png','2019-09-07 18:27:48','2019-09-07 18:27:48'),
	(14,'1567880942FR5Sfj4whNOFa9B.jpg','2019-09-07 18:29:03','2019-09-07 18:29:03'),
	(15,'1567880957fv9ZvyCkCXowFjB.png','2019-09-07 18:29:18','2019-09-07 18:29:18'),
	(17,'1567881255WV33x4hs2FwYIPY.png','2019-09-07 18:34:16','2019-09-07 18:34:16'),
	(18,'1567881278QOlkWl5YtHF0crQ.png','2019-09-07 18:34:39','2019-09-07 18:34:39'),
	(19,'1567881311LI4xwJdS2NgqBgk.png','2019-09-07 18:35:12','2019-09-07 18:35:12'),
	(20,'1567881367XD8hQ2DuYqw64Ut.png','2019-09-07 18:36:08','2019-09-07 18:36:08'),
	(55,'1567885381nvU9s34eHumMIn7.jpg','2019-09-07 19:43:03','2019-09-07 19:43:03'),
	(56,'15678853837ZbAWhbrg4FiyWb.jpg','2019-09-07 19:43:04','2019-09-07 19:43:04'),
	(57,'1567885384akurbZwpRrOBz7e.jpg','2019-09-07 19:43:05','2019-09-07 19:43:05'),
	(58,'1567885473UGySktRa4bw0c5L.jpg','2019-09-07 19:44:34','2019-09-07 19:44:34'),
	(59,'1567885474eU1or1RLvuOTPd9.jpg','2019-09-07 19:44:36','2019-09-07 19:44:36'),
	(60,'1567885476uYEjH27PZa1Vrwp.jpg','2019-09-07 19:44:36','2019-09-07 19:44:36');

/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_id` smallint(5) unsigned DEFAULT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `released_on` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '500',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `name`, `photo_id`, `category_id`, `discount`, `released_on`, `created_at`, `updated_at`, `price`, `color`, `sort`)
VALUES
	(43,'Букет Фламинго',20,1,NULL,NULL,'2019-09-07 18:34:16','2019-09-07 19:46:45',NULL,'#f899bd',1);

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table reviews
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reviews`;

CREATE TABLE `reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image` smallint(5) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;

INSERT INTO `reviews` (`id`, `video`, `image`, `created_at`, `updated_at`)
VALUES
	(47,NULL,10,'2019-09-07 18:09:25','2019-09-07 18:09:25'),
	(48,NULL,11,'2019-09-07 18:11:47','2019-09-07 18:11:47');

/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shops
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shops`;

CREATE TABLE `shops` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `shops` WRITE;
/*!40000 ALTER TABLE `shops` DISABLE KEYS */;

INSERT INTO `shops` (`id`, `name`, `address`, `lat`, `lng`, `created_at`, `updated_at`)
VALUES
	(1,'Самовывоз','Строгинский бульвар 4','55.804616','37.396617',NULL,'2019-06-18 17:50:07'),
	(2,'Самовывоз','улица Коккинаки д 4','55.804304','37.546138','2019-06-18 17:51:52','2019-06-18 17:51:52'),
	(3,'Самовывоз','ТЦ Лига, Ленинградское шоссе вл 5','55.888997','37.433046','2019-06-18 17:53:04','2019-06-18 17:53:27'),
	(4,'Самовывоз','Лихачёвский просп. д 64','55.931235','37.494153','2019-06-18 17:54:21','2019-06-18 17:54:21');

/*!40000 ALTER TABLE `shops` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table size
# ------------------------------------------------------------

DROP TABLE IF EXISTS `size`;

CREATE TABLE `size` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `size` WRITE;
/*!40000 ALTER TABLE `size` DISABLE KEYS */;

INSERT INTO `size` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(3,'S','2019-09-04 14:31:35','2019-09-04 14:31:35'),
	(4,'M','2019-09-04 14:31:39','2019-09-04 14:31:39'),
	(5,'L','2019-09-04 14:31:42','2019-09-04 14:31:42');

/*!40000 ALTER TABLE `size` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subcategories`;

CREATE TABLE `subcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subcategories_category_id_index` (`category_id`),
  CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table texts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `texts`;

CREATE TABLE `texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slider` text COLLATE utf8mb4_unicode_ci,
  `title2` text COLLATE utf8mb4_unicode_ci,
  `text2` text COLLATE utf8mb4_unicode_ci,
  `title3` text COLLATE utf8mb4_unicode_ci,
  `text3` text COLLATE utf8mb4_unicode_ci,
  `title4` text COLLATE utf8mb4_unicode_ci,
  `text4` text COLLATE utf8mb4_unicode_ci,
  `title5` text COLLATE utf8mb4_unicode_ci,
  `text5` text COLLATE utf8mb4_unicode_ci,
  `title6` text COLLATE utf8mb4_unicode_ci,
  `text6` text COLLATE utf8mb4_unicode_ci,
  `title7` text COLLATE utf8mb4_unicode_ci,
  `text7` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `privacy` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `texts` WRITE;
/*!40000 ALTER TABLE `texts` DISABLE KEYS */;

INSERT INTO `texts` (`id`, `slider`, `title2`, `text2`, `title3`, `text3`, `title4`, `text4`, `title5`, `text5`, `title6`, `text6`, `title7`, `text7`, `created_at`, `updated_at`, `privacy`)
VALUES
	(1,'<h1>Сладкие воздушные<br />\r\nбукеты из зефира<br />\r\nи мармеладок</h1>','Используем самые качественные ингридиенты','<p class=\"subtitle\"><span style=\"font-size:16px;\"><span style=\"line-height:115%\"><span new=\"\" roman=\"\" times=\"\">Наши букеты из маршмеллоу состоят из высококачественной продукции крупнейших европейских производителей, таких как Fini Sweets (Италия), Bulgari (Испания), Damel (Испания),&nbsp; Golmasa (Испания). Ведущие компании имеют огромный опыт и большую историю и продолжают традиции в кондитерском деле, имеют самые высокие международные оценки качества и безопасности пищевых продуктов, сертификаты IFS, BRC и ISO 9001: 2008. </span></span></span></p>\r\n\r\n<p class=\"subtitle\"><span style=\"font-size:16px;\"><span style=\"line-height:115%\"><span new=\"\" roman=\"\" times=\"\">Зефирные букеты всегда самого высшего качества и свежести, соблюдаются все условия хранения . Наше суфле остаётся мягким и свежим на протяжении долгого времени после изготовления букета. Мы собираем сладкие букеты согласно фитосанитарным нормам в перчатках и упаковываем в пищевую слюду за 2 часа до доставки, что позволяет гарантировать качество нашей продукции.</span></span></span></p>','Соблюдаем все санитарные нормы','<h4 class=\"subtitle\">Букеты собираются в перчатках и упаковываются в плёнку</h4>','Доставка и оплата  <br> по Москве и области','<p class=\"text\">Оплата курьеру <strong>наличными</strong> или <strong>переводом</strong> на карту. Доставка осуществляется на следующий день после заказа. Возможна доставка день в день по согласованию. Бесплатная доставка по Москве в пределах МКАД.</p>','Выбери свой букет','<p>Наши букеты настолько вкусно пахнут, что перед доставкой букета мы кормим курьера зефиром, т.к. есть шанс, что он его съест</p>','Полезные сладости – это не миф','<p><span style=\"font-size:14px;\"><span style=\"background:white\"><span style=\"line-height:115%\"><span new=\"\" roman=\"\" times=\"\"><span style=\"color:black\">Основа зефира &ndash; фруктово-ягодное пюре. Его готовят из яблок, груш, клюквы, черной смородины и других плодов. Все они богаты витаминами и минералами, органическими кислотами и пектином.&nbsp;</span></span></span></span><span style=\"line-height:150%\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"background:white\"><span style=\"line-height:150%\"><span new=\"\" roman=\"\" times=\"\"><span style=\"color:black\">Воздушный зефир не содержит ни грамма жира, он легко переваривается, поэтому такое лакомство &laquo;не нагружает&raquo; наш желудок, как пирожные и торты. А за счет содержания пектина или другого желирующего агента зефир даже улучшает пищеварение.</span></span></span></span></span></span></span></p>','Вы можете приобрести букет в одном из магазинов наших партнёров','<p>Для того чтобы стать партнёром оставьте свой номер, мы перезвоним и расскажем про условия</p>',NULL,'2019-06-19 14:18:26',NULL);

/*!40000 ALTER TABLE `texts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tproducts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tproducts`;

CREATE TABLE `tproducts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` text COLLATE utf8mb4_unicode_ci,
  `product_id` smallint(5) unsigned DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `mass` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `size` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `tproducts` WRITE;
/*!40000 ALTER TABLE `tproducts` DISABLE KEYS */;

INSERT INTO `tproducts` (`id`, `name`, `icon`, `product_id`, `price`, `mass`, `priority`, `desc`, `created_at`, `updated_at`, `size`)
VALUES
	(25,'Букет Фламинго','[55,56,57]',43,2190,'580','1','<p>Мы предложили вам суфле со вкусом мороженного в виде рожка. Так же малиновые ягоды и сливочное маршмеллоу.</p>','2019-09-07 19:36:33','2019-09-07 19:47:18',4),
	(27,'Букет Фламинго','[58,59,60]',43,1690,'360',NULL,'<p>Мы предложили вам суфле со вкусом мороженного в виде рожка. Так же малиновые ягоды и сливочное маршмеллоу.</p>','2019-09-07 19:44:36','2019-09-07 19:47:18',3);

/*!40000 ALTER TABLE `tproducts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `name`, `surname`, `adress`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'admin@admin.com','Admin',NULL,NULL,NULL,'$2y$10$6C7ejF8FPb2OMR2buBQKbOmk1dL9l3b.pE8N9VALuXoZpjpqnArDe','YIEIsSyUbe3fvS0QafF72EhXziqfHoEgaO5WWvIBe0EnhhOEWYLRgM6ArKoU','2019-06-10 17:37:44','2019-06-10 17:37:44');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
