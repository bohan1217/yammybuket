-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 26, 2019 at 08:16 PM
-- Server version: 5.6.34
-- PHP Version: 5.3.10-1ubuntu3.26

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yammybuket`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Букеты', '2019-06-10 14:39:26', '2019-06-10 14:39:26'),
(2, 'Открытки', '2019-06-17 14:53:19', '2019-06-17 14:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `discount`, `end_date`, `created_at`, `updated_at`) VALUES
(3, '001-508566-758', 10, NULL, '2019-06-14 10:04:37', '2019-06-14 10:04:37'),
(4, '222', 10, NULL, '2019-09-09 05:19:40', '2019-09-09 05:19:40');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `phone`, `created_at`, `updated_at`) VALUES
(2, '+7(342) 432-34-32', '2019-06-14 09:08:13', '2019-06-14 09:08:13');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` int(10) unsigned NOT NULL,
  `to` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=28 ;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_01_02_131905_create_products_table', 1),
(4, '2018_01_02_132136_create_categories_table', 1),
(5, '2018_01_02_132533_create_subcategories_table', 1),
(6, '2018_01_02_213025_create_roles_table', 1),
(7, '2018_01_06_000752_create_photos_table', 1),
(8, '2018_01_09_092104_create_faqs_table', 1),
(9, '2018_01_12_163545_create_orders_table', 1),
(10, '2019_06_01_101336_change_discoutn_type_in_table_product', 1),
(11, '2019_06_01_134942_delete_mig_priority', 1),
(12, '2019_06_08_151732_create_text_table', 1),
(13, '2019_06_10_092018_create_feedback_table', 1),
(14, '2019_06_10_133104_change_order_table', 1),
(15, '2019_06_10_175352_create_shops_table', 2),
(16, '2019_06_12_085801_create_texts_table', 3),
(17, '2019_06_13_163650_create_coupons_table', 4),
(18, '2019_06_14_122203_delete_from_users', 5),
(19, '2019_06_14_130527_add_email_orders', 6),
(20, '2019_06_14_131630_create_messages_table', 7),
(21, '2019_06_17_180351_update_product', 8),
(22, '2019_06_18_141645_create_reviews_table', 9),
(23, '2019_06_22_085215_update_table_text', 10),
(24, '2019_09_04_132037_delete_from_catalog_and_add', 11),
(25, '2019_09_04_140418_create_size_table', 12),
(26, '2019_09_04_143616_create_tproducts_table', 13),
(27, '2019_09_04_145414_add_t_size', 14);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` int(11) DEFAULT NULL,
  `products` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery` int(11) DEFAULT NULL,
  `name2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ascension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `orderId`, `status`, `totalAmount`, `products`, `adress`, `created_at`, `updated_at`, `name`, `phone`, `delivery`, `name2`, `phone2`, `street`, `house`, `flat`, `ascension`, `message`, `email`) VALUES
(30, '5d713c3bafd1d', 'PENDING', 370000, '12-2-1|13-1-2|11-1-1', '___', '2019-09-05 13:47:55', '2019-09-05 13:47:55', 'Богдан', '+7(506) 612-00-4', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testswipeks@yandex.ru'),
(31, '5d7412b788c97', 'PENDING', 338000, '27-2-2', '___', '2019-09-07 17:27:35', '2019-09-07 17:27:35', 'Богдан', '+7(506) 612-00-4', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testswipeks@yandex.ru');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=227 ;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `file`, `created_at`, `updated_at`) VALUES
(13, '1567880867Z2oGCXvOaCP5VWo.png', '2019-09-07 15:27:48', '2019-09-07 15:27:48'),
(14, '1567880942FR5Sfj4whNOFa9B.jpg', '2019-09-07 15:29:03', '2019-09-07 15:29:03'),
(15, '1567880957fv9ZvyCkCXowFjB.png', '2019-09-07 15:29:18', '2019-09-07 15:29:18'),
(17, '1567881255WV33x4hs2FwYIPY.png', '2019-09-07 15:34:16', '2019-09-07 15:34:16'),
(18, '1567881278QOlkWl5YtHF0crQ.png', '2019-09-07 15:34:39', '2019-09-07 15:34:39'),
(19, '1567881311LI4xwJdS2NgqBgk.png', '2019-09-07 15:35:12', '2019-09-07 15:35:12'),
(20, '1567881367XD8hQ2DuYqw64Ut.png', '2019-09-07 15:36:08', '2019-09-07 15:36:08'),
(68, '1568624383c927fGwDpKkg57M.jpg', '2019-09-16 05:59:44', '2019-09-16 05:59:44'),
(77, '15686531582oZacdlLtncvoyh.jpg', '2019-09-16 13:59:19', '2019-09-16 13:59:19'),
(86, '15686566366fsAnX28CWRh3Cm.jpg', '2019-09-16 14:57:16', '2019-09-16 14:57:16'),
(91, '1568656971inDNfCOM2j5RTcU.jpg', '2019-09-16 15:02:51', '2019-09-16 15:02:51'),
(92, '1568656971XzpuAxh0brudVI3.jpg', '2019-09-16 15:02:52', '2019-09-16 15:02:52'),
(93, '1568656972olxsIMGXqVggDAf.jpg', '2019-09-16 15:02:52', '2019-09-16 15:02:52'),
(94, '1568656972ycvShkM8DDkc6XB.jpg', '2019-09-16 15:02:52', '2019-09-16 15:02:52'),
(95, '1568657057iO1Y6eiBlDvAmLX.jpg', '2019-09-16 15:04:17', '2019-09-16 15:04:17'),
(96, '1568657102y92skwNw37Jj91J.jpg', '2019-09-16 15:05:02', '2019-09-16 15:05:02'),
(97, '1568657102yGEikwctiDSv5bl.jpg', '2019-09-16 15:05:02', '2019-09-16 15:05:02'),
(102, '1568657184q8bG9V2hhRCHmlX.jpg', '2019-09-16 15:06:24', '2019-09-16 15:06:24'),
(103, '1568657184Jrb8wadI4swHMt2.jpg', '2019-09-16 15:06:24', '2019-09-16 15:06:24'),
(104, '15686572341AXVlOby9gFOHJ6.jpg', '2019-09-16 15:07:14', '2019-09-16 15:07:14'),
(105, '1568657234QoLlEmfWhoMZAMZ.jpg', '2019-09-16 15:07:14', '2019-09-16 15:07:14'),
(106, '1568657234m8Oo2eEmzZtWZd3.jpg', '2019-09-16 15:07:14', '2019-09-16 15:07:14'),
(107, '1568657320jgtHxyha1pcr6yg.jpg', '2019-09-16 15:08:40', '2019-09-16 15:08:40'),
(108, '1568657368D6FL2LlySdNlpli.jpg', '2019-09-16 15:09:28', '2019-09-16 15:09:28'),
(109, '1568657368HdjJqRe2gWQR8fA.jpg', '2019-09-16 15:09:29', '2019-09-16 15:09:29'),
(110, '1568657428BTU1BvBufRMkfWC.jpg', '2019-09-16 15:10:28', '2019-09-16 15:10:28'),
(111, '15686574287JsUe5om1LnhucK.jpg', '2019-09-16 15:10:28', '2019-09-16 15:10:28'),
(112, '1568657455hf88OX3NjEsGRBk.jpg', '2019-09-16 15:10:55', '2019-09-16 15:10:55'),
(113, '1568657455YDOVDj7oLdyTFCE.jpg', '2019-09-16 15:10:55', '2019-09-16 15:10:55'),
(114, '1568657455YseeXNUOggY3Jf7.jpg', '2019-09-16 15:10:55', '2019-09-16 15:10:55'),
(115, '1568657455TQcSn6Nd0RqU3hB.jpg', '2019-09-16 15:10:56', '2019-09-16 15:10:56'),
(116, '1568657514SAfRseb8DhgfhZ1.jpg', '2019-09-16 15:11:54', '2019-09-16 15:11:54'),
(117, '15686575515pPo933atXXDAad.jpg', '2019-09-16 15:12:31', '2019-09-16 15:12:31'),
(118, '1568657551KiQ96xt4NzDPJan.jpg', '2019-09-16 15:12:31', '2019-09-16 15:12:31'),
(119, '1568657597pVtbase48ZuOz28.jpg', '2019-09-16 15:13:17', '2019-09-16 15:13:17'),
(120, '1568657597JOgq9G6oxqF1Dco.jpg', '2019-09-16 15:13:17', '2019-09-16 15:13:17'),
(121, '156865762241ZX8uckPb8qsnd.jpg', '2019-09-16 15:13:42', '2019-09-16 15:13:42'),
(122, '1568657622tqxpTmFdbNqd9Z7.jpg', '2019-09-16 15:13:42', '2019-09-16 15:13:42'),
(123, '1568657622ryYhZdRUl8XyyJO.jpg', '2019-09-16 15:13:42', '2019-09-16 15:13:42'),
(124, '1568657622ikOMgYc9gUFfWhv.jpg', '2019-09-16 15:13:42', '2019-09-16 15:13:42'),
(125, '1568657622zBuZXgpxsisSxWG.jpg', '2019-09-16 15:13:43', '2019-09-16 15:13:43'),
(126, '1568657702ozR1M94kU5aVmI2.jpg', '2019-09-16 15:15:02', '2019-09-16 15:15:02'),
(127, '1568657748ektLB8SeyEp6Ki9.jpg', '2019-09-16 15:15:48', '2019-09-16 15:15:48'),
(128, '1568657748RfHPojwAsyDf2HY.jpg', '2019-09-16 15:15:48', '2019-09-16 15:15:48'),
(129, '1568657748GZyv5XaVmK41Rd9.jpg', '2019-09-16 15:15:49', '2019-09-16 15:15:49'),
(130, '1568657749jKBXwOWhcSiidVa.jpg', '2019-09-16 15:15:49', '2019-09-16 15:15:49'),
(131, '1568657803drclGbgV5bN2pOF.jpg', '2019-09-16 15:16:43', '2019-09-16 15:16:43'),
(132, '1568657836M4kZbEyDeYhCqo8.jpg', '2019-09-16 15:17:16', '2019-09-16 15:17:16'),
(133, '1568657836Zp9hWPDjplVwpG5.jpg', '2019-09-16 15:17:16', '2019-09-16 15:17:16'),
(134, '1568657867VPUL51nFjR0hIZ5.jpg', '2019-09-16 15:17:47', '2019-09-16 15:17:47'),
(135, '1568657867dVeo9YcuozgAR8k.jpg', '2019-09-16 15:17:47', '2019-09-16 15:17:47'),
(136, '1568657867xGW9htLGuQjCKyB.jpg', '2019-09-16 15:17:48', '2019-09-16 15:17:48'),
(137, '1568657890x1vYQkBEvYoZdBj.jpg', '2019-09-16 15:18:10', '2019-09-16 15:18:10'),
(138, '1568657891dLiUxJzum6gUrto.jpg', '2019-09-16 15:18:11', '2019-09-16 15:18:11'),
(139, '1568657985a8N7J265p0kM3Hv.jpg', '2019-09-16 15:19:45', '2019-09-16 15:19:45'),
(140, '1568658052mJx5jUd30Ne5XJI.jpg', '2019-09-16 15:20:52', '2019-09-16 15:20:52'),
(141, '1568658052ibfvx6rzNde29rr.jpg', '2019-09-16 15:20:52', '2019-09-16 15:20:52'),
(142, '1568658052pqXqohldKKvQVrv.jpg', '2019-09-16 15:20:52', '2019-09-16 15:20:52'),
(143, '1568658110kyLknWgUdKydZyl.jpg', '2019-09-16 15:21:51', '2019-09-16 15:21:51'),
(144, '1568658150o247Mn1jdCGldaK.jpg', '2019-09-16 15:22:31', '2019-09-16 15:22:31'),
(145, '1568658151T5fmQhov3i78DAj.jpg', '2019-09-16 15:22:31', '2019-09-16 15:22:31'),
(146, '1568658151kT6ALtE2hNSQsmx.jpg', '2019-09-16 15:22:31', '2019-09-16 15:22:31'),
(147, '1568658205R7ACZrZa0l4YpZj.jpg', '2019-09-16 15:23:25', '2019-09-16 15:23:25'),
(152, '1568658255HquTKnTyqDlZ06Q.jpg', '2019-09-16 15:24:16', '2019-09-16 15:24:16'),
(153, '1568658853xI7iDEsJKi8fJgN.jpg', '2019-09-16 15:34:14', '2019-09-16 15:34:14'),
(154, '1568658901WU49nUJfQSu0H5H.jpg', '2019-09-16 15:35:02', '2019-09-16 15:35:02'),
(155, '1568658902czU3CpksvJVuv3b.jpg', '2019-09-16 15:35:02', '2019-09-16 15:35:02'),
(156, '1568658940msgzotBljUFLhmI.jpg', '2019-09-16 15:35:41', '2019-09-16 15:35:41'),
(157, '1568658967fMHHByWxcmDah49.jpg', '2019-09-16 15:36:08', '2019-09-16 15:36:08'),
(158, '1568659083ejVC0DQj1vHr5Du.jpg', '2019-09-16 15:38:03', '2019-09-16 15:38:03'),
(159, '1568659116gwKaoasScdl3iou.jpg', '2019-09-16 15:38:36', '2019-09-16 15:38:36'),
(160, '15686591163h9IWJcqsw6V85o.jpg', '2019-09-16 15:38:36', '2019-09-16 15:38:36'),
(161, '1568659176RxW4Umg7WJbU5Jb.jpg', '2019-09-16 15:39:36', '2019-09-16 15:39:36'),
(162, '1568659176mHnEtG0vtXvh4Rs.jpg', '2019-09-16 15:39:36', '2019-09-16 15:39:36'),
(163, '1568659176i2CUySJDpNLkM9L.jpg', '2019-09-16 15:39:37', '2019-09-16 15:39:37'),
(164, '1568659177yjlog9ZDck5E4ar.jpg', '2019-09-16 15:39:37', '2019-09-16 15:39:37'),
(165, '1568659202hBCrM1jDoGzk4Cv.jpg', '2019-09-16 15:40:03', '2019-09-16 15:40:03'),
(166, '1568659203Cb48gvE820ZDuYV.jpg', '2019-09-16 15:40:03', '2019-09-16 15:40:03'),
(167, '1568659203Zsep6uDhoZqPp5S.jpg', '2019-09-16 15:40:03', '2019-09-16 15:40:03'),
(168, '15686593011dBDbONSTzZTpxq.jpg', '2019-09-16 15:41:41', '2019-09-16 15:41:41'),
(169, '1568659332O8l46kGYZYfmRCn.jpg', '2019-09-16 15:42:13', '2019-09-16 15:42:13'),
(170, '1568659333gtIbDP45YUOs83m.jpg', '2019-09-16 15:42:13', '2019-09-16 15:42:13'),
(171, '1568659363sPZOrdUyXNBovsf.jpg', '2019-09-16 15:42:43', '2019-09-16 15:42:43'),
(172, '15686593634GNxNmNpSfFhfvG.jpg', '2019-09-16 15:42:43', '2019-09-16 15:42:43'),
(173, '1568659363xJ8MjOvwrcz5Lhz.jpg', '2019-09-16 15:42:43', '2019-09-16 15:42:43'),
(174, '1568659430IJsCuRD6xSRBGXf.jpg', '2019-09-16 15:43:50', '2019-09-16 15:43:50'),
(179, '1568659651o9zYr6xB8w3lFVH.jpg', '2019-09-16 15:47:31', '2019-09-16 15:47:31'),
(187, '1568659863MNefUmixszlZKSx.jpg', '2019-09-16 15:51:03', '2019-09-16 15:51:03'),
(198, '1568660108SGQE2KCPPiTSqgl.jpg', '2019-09-16 15:55:08', '2019-09-16 15:55:08'),
(199, '1568660108ALVYSs4pMsudZtF.jpg', '2019-09-16 15:55:09', '2019-09-16 15:55:09'),
(200, '1568660129G8tUBO3HBMYSQbF.jpg', '2019-09-16 15:55:30', '2019-09-16 15:55:30'),
(201, '1568660130ImsM3Ws6XmjeVkS.jpeg', '2019-09-16 15:55:30', '2019-09-16 15:55:30'),
(202, '1568660130ilhsBEE6jTavRRA.jpg', '2019-09-16 15:55:30', '2019-09-16 15:55:30'),
(203, '1568660404h0zf1MXT84paLMM.jpg', '2019-09-16 16:00:04', '2019-09-16 16:00:04'),
(204, '1568660437hca2Y5tCWcge6UH.jpg', '2019-09-16 16:00:37', '2019-09-16 16:00:37'),
(205, '1568660438PhOMf4sCBkp4X3s.jpg', '2019-09-16 16:00:38', '2019-09-16 16:00:38'),
(206, '1568660490grUhQ0jGJsSkr03.jpg', '2019-09-16 16:01:30', '2019-09-16 16:01:30'),
(207, '1568660517hX96xeGByd7T9pv.jpg', '2019-09-16 16:01:58', '2019-09-16 16:01:58'),
(208, '1568660518oDUlMBOhnX5LveE.jpg', '2019-09-16 16:01:58', '2019-09-16 16:01:58'),
(209, '1568660518EuHNYHjy1pJ4bqp.jpg', '2019-09-16 16:01:58', '2019-09-16 16:01:58'),
(210, '1568661002w5DqNUBy8gSS7mi.jpg', '2019-09-16 16:10:02', '2019-09-16 16:10:02'),
(211, '1568661019TnkuTqzDFj4z4zZ.jpg', '2019-09-16 16:10:19', '2019-09-16 16:10:19'),
(212, '1568661138msIeP7yrtkZvoWl.jpg', '2019-09-16 16:12:18', '2019-09-16 16:12:18'),
(213, '1568661149LNuezJh46QaxkhI.jpg', '2019-09-16 16:12:29', '2019-09-16 16:12:29'),
(214, '1568661162sEEVYik63nU6gqJ.jpg', '2019-09-16 16:12:42', '2019-09-16 16:12:42'),
(215, '1568661176X1kA6bLcak7cKR8.jpg', '2019-09-16 16:12:56', '2019-09-16 16:12:56'),
(216, '1568661187xb6pMfGQrc9Onzp.jpg', '2019-09-16 16:13:07', '2019-09-16 16:13:07'),
(217, '1568661203Av0e4miH3rQzAzl.jpg', '2019-09-16 16:13:23', '2019-09-16 16:13:23'),
(218, '1568897067zb6GdJqjeSZbVu3.jpg', '2019-09-19 09:44:28', '2019-09-19 09:44:28'),
(219, '1568897090H4ocJb5cyQsmJC2.jpg', '2019-09-19 09:44:51', '2019-09-19 09:44:51'),
(220, '1568897153Th6ueh6duLYjkD2.jpg', '2019-09-19 09:45:54', '2019-09-19 09:45:54'),
(221, '15688971886reArUai9H0Ebb9.jpg', '2019-09-19 09:46:28', '2019-09-19 09:46:28'),
(224, '1568897387N4NnNpIsuhFUCtp.jpg', '2019-09-19 09:49:47', '2019-09-19 09:49:47'),
(225, '15693373151561055860photo_2019-06-18_20-58-19.jpg', '2019-09-24 12:01:55', '2019-09-24 12:01:55'),
(226, '15693373211561056184photo_2019-06-18_20-58-22.jpg', '2019-09-24 12:02:02', '2019-09-24 12:02:02');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_id` smallint(5) unsigned DEFAULT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `released_on` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '500',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=75 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `photo_id`, `category_id`, `discount`, `released_on`, `created_at`, `updated_at`, `price`, `color`, `sort`) VALUES
(49, 'Амур', 86, 1, 10, NULL, '2019-09-16 14:57:17', '2019-09-25 06:24:26', NULL, '#ffddf9', 1),
(50, 'Виолетти', 95, 1, NULL, NULL, '2019-09-16 15:04:19', '2019-09-16 15:04:19', NULL, '#31dbf4', 2),
(51, 'Бананза', 107, 1, NULL, NULL, '2019-09-16 15:08:40', '2019-09-16 15:11:15', NULL, '#e9b77a', 3),
(52, 'Флоренция', 116, 1, 10, NULL, '2019-09-16 15:11:54', '2019-09-19 09:56:27', NULL, '#f9f7be', 4),
(53, 'Вэлэнтайн', 126, 1, NULL, NULL, '2019-09-16 15:15:02', '2019-09-16 15:15:02', NULL, '#df9eb4', 5),
(54, 'Радуга', 131, 1, NULL, NULL, '2019-09-16 15:16:43', '2019-09-16 15:19:55', NULL, '#b2f4ac', 6),
(55, 'Лаванда', 139, 1, NULL, NULL, '2019-09-16 15:19:45', '2019-09-16 15:19:45', NULL, '#f8d4f4', 7),
(56, 'Африка', 143, 1, NULL, NULL, '2019-09-16 15:21:51', '2019-09-16 15:21:51', NULL, '#f9c868', 8),
(57, 'Гармония', 221, 1, NULL, NULL, '2019-09-16 15:23:25', '2019-09-19 09:46:28', NULL, '#c0d5fd', 9),
(58, 'Индиго', 153, 1, NULL, NULL, '2019-09-16 15:34:14', '2019-09-16 15:34:14', NULL, '#e8abf8', 10),
(59, 'Лето', 158, 1, NULL, NULL, '2019-09-16 15:38:03', '2019-09-16 15:58:51', NULL, '#eef08f', 11),
(60, 'Фламинго', 168, 1, NULL, NULL, '2019-09-16 15:41:41', '2019-09-16 15:41:41', NULL, '#f999be', 11),
(61, 'Бэлла', 224, 1, NULL, NULL, '2019-09-16 15:43:50', '2019-09-19 09:49:47', NULL, '#f3a2b5', 13),
(62, 'Подсолнухи', 187, 1, NULL, NULL, '2019-09-16 15:51:03', '2019-09-16 15:58:56', NULL, '#f3f3b3', 12),
(63, 'Верано', 203, 1, NULL, NULL, '2019-09-16 16:00:04', '2019-09-16 16:00:04', NULL, '#d3e7d5', 14),
(64, 'Марсель', 206, 1, NULL, NULL, '2019-09-16 16:01:30', '2019-09-16 16:01:30', NULL, '#f58c6c', 14),
(65, 'Открытка', 210, 2, NULL, NULL, '2019-09-16 16:10:02', '2019-09-16 16:10:02', '190', NULL, 500),
(66, 'Открытка', 211, 2, NULL, NULL, '2019-09-16 16:10:19', '2019-09-16 16:10:19', '190', NULL, 500),
(67, 'Открытка', 212, 2, NULL, NULL, '2019-09-16 16:12:18', '2019-09-16 16:12:18', '190', NULL, 500),
(68, 'Открытка', 213, 2, NULL, NULL, '2019-09-16 16:12:29', '2019-09-16 16:12:29', '190', NULL, 500),
(69, 'Открытка', 214, 2, NULL, NULL, '2019-09-16 16:12:42', '2019-09-16 16:12:42', '190', NULL, 500),
(70, 'Открытка', 215, 2, NULL, NULL, '2019-09-16 16:12:56', '2019-09-16 16:12:56', '190', NULL, 500),
(71, 'Открытка', 216, 2, NULL, NULL, '2019-09-16 16:13:07', '2019-09-16 16:16:15', '190', NULL, 500),
(72, 'Открытка', 217, 2, NULL, NULL, '2019-09-16 16:13:23', '2019-09-16 16:13:23', '190', NULL, 500);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image` smallint(5) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=51 ;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `video`, `image`, `created_at`, `updated_at`) VALUES
(49, NULL, 225, '2019-09-24 12:01:55', '2019-09-24 12:01:55'),
(50, NULL, 226, '2019-09-24 12:02:02', '2019-09-24 12:02:02');

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `name`, `address`, `lat`, `lng`, `created_at`, `updated_at`) VALUES
(1, 'Самовывоз', 'Строгинский бульвар 4', '55.804616', '37.396617', NULL, '2019-06-18 14:50:07'),
(2, 'Самовывоз', 'улица Коккинаки д 4', '55.804304', '37.546138', '2019-06-18 14:51:52', '2019-06-18 14:51:52'),
(3, 'Самовывоз', 'ТЦ Лига, Ленинградское шоссе вл 5', '55.888997', '37.433046', '2019-06-18 14:53:04', '2019-06-18 14:53:27');

-- --------------------------------------------------------

--
-- Table structure for table `size`
--

CREATE TABLE IF NOT EXISTS `size` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `size`
--

INSERT INTO `size` (`id`, `name`, `created_at`, `updated_at`) VALUES
(3, 'S', '2019-09-04 11:31:35', '2019-09-04 11:31:35'),
(4, 'M', '2019-09-04 11:31:39', '2019-09-04 11:31:39'),
(5, 'L', '2019-09-04 11:31:42', '2019-09-04 11:31:42');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subcategories_category_id_index` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `texts`
--

CREATE TABLE IF NOT EXISTS `texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slider` text COLLATE utf8mb4_unicode_ci,
  `title2` text COLLATE utf8mb4_unicode_ci,
  `text2` text COLLATE utf8mb4_unicode_ci,
  `title3` text COLLATE utf8mb4_unicode_ci,
  `text3` text COLLATE utf8mb4_unicode_ci,
  `title4` text COLLATE utf8mb4_unicode_ci,
  `text4` text COLLATE utf8mb4_unicode_ci,
  `title5` text COLLATE utf8mb4_unicode_ci,
  `text5` text COLLATE utf8mb4_unicode_ci,
  `title6` text COLLATE utf8mb4_unicode_ci,
  `text6` text COLLATE utf8mb4_unicode_ci,
  `title7` text COLLATE utf8mb4_unicode_ci,
  `text7` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `privacy` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `texts`
--

INSERT INTO `texts` (`id`, `slider`, `title2`, `text2`, `title3`, `text3`, `title4`, `text4`, `title5`, `text5`, `title6`, `text6`, `title7`, `text7`, `created_at`, `updated_at`, `privacy`) VALUES
(1, '<h1>Сладкие воздушные<br />\r\nбукеты из зефира<br />\r\nи мармеладок</h1>', 'Используем самые качественные ингридиенты', '<p>Наши букеты из маршмеллоу и мармелада&nbsp;состоят из высококачественной продукции крупнейших европейских производителей, таких как Fini Sweets (Италия), Bulgari (Испания), Damel (Испания),&nbsp; Golmasa (Испания). Ведущие компании имеют огромный опыт и большую историю и продолжают традиции в кондитерском деле, имеют самые высокие международные оценки качества и безопасности пищевых продуктов, сертификаты IFS, BRC и ISO 9001: 2008.</p>\r\n\r\n<p>Зефирные букеты всегда самого высшего качества и свежести, соблюдаются все условия хранения . Наше суфле остаётся мягким и свежим на протяжении долгого времени после изготовления букета. Мы собираем сладкие букеты согласно фитосанитарным нормам в перчатках и упаковываем в пищевую слюду за 2 часа до доставки, что позволяет гарантировать качество нашей продукции.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Букеты из маршмеллоу - всегда отличный подарок на любой праздник, юбилей, день рождение, свадьбу, 1 сентября учителю, 8 марта.</p>', 'Соблюдаем все санитарные нормы', '<h4 class="subtitle">Букеты собираются в перчатках и упаковываются в плёнку</h4>', 'Доставка и оплата  <br> по Москве и области', '<p><strong>Бесплатная доставка</strong>&nbsp;по Москве и в пределах МКАД осуществляется на следующий день после заказа.&nbsp;Возможна доставка день в день по согласованию.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Оплата курьеру&nbsp;<strong>наличными</strong>&nbsp;или банковским&nbsp;<strong>переводом</strong>&nbsp;на карту.&nbsp;</p>', 'Выбери свой букет', '<p>Наши букеты настолько вкусно пахнут, что перед доставкой букета мы кормим курьера зефиром, т.к. есть шанс, что он его съест</p>', 'Полезные сладости – это не миф', '<p>Основа зефира &ndash; фруктово-ягодное пюре. Его готовят из яблок, груш, клюквы, черной смородины и других плодов. Все они богаты витаминами и минералами, органическими кислотами и пектином.&nbsp;Воздушный зефир не содержит ни грамма жира, он легко переваривается, поэтому такое лакомство &laquo;не нагружает&raquo; наш желудок, как пирожные и торты. А за счет содержания пектина или другого желирующего агента зефир даже улучшает пищеварение. Сладкий букет может быть полезным!</p>', 'Хочешь сделать корпоративный заказ?', '<p>Оставь свой номер телефона и мы свяжемся с тобой в ближайшее время, что бы рассказать про условия.</p>', NULL, '2019-09-14 04:52:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tproducts`
--

CREATE TABLE IF NOT EXISTS `tproducts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` text COLLATE utf8mb4_unicode_ci,
  `product_id` smallint(5) unsigned DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `mass` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `size` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=70 ;

--
-- Dumping data for table `tproducts`
--

INSERT INTO `tproducts` (`id`, `name`, `icon`, `product_id`, `price`, `mass`, `priority`, `desc`, `created_at`, `updated_at`, `size`) VALUES
(32, 'Амур', '[91,92,93,94]', 49, 2767, '1000', '1', '<p>Мармеладки-ягодки со вкусом йогурта и малины</p>\r\n\r\n<p>в элегантном сочетании с классическим сахарным суфле.</p>', '2019-09-16 14:58:17', '2019-09-25 06:24:36', 5),
(33, 'Виолетти', '[96,97]', 50, 1690, '360', NULL, '<p>Хрустящие вафельные трубочки с ванильной ноткой</p>\r\n\r\n<p>в сливочном маршмеллоу</p>\r\n\r\n<p>и ягодки бабл гам.</p>', '2019-09-16 15:05:02', '2019-09-16 16:07:52', 3),
(34, 'Виолетти', '[104,105,106]', 50, 2290, '580', '1', '<p>Хрустящие вафельные трубочки с ванильной ноткой</p>\r\n\r\n<p>в сливочном маршмеллоу</p>\r\n\r\n<p>и ягодки бабл гам.</p>', '2019-09-16 15:05:40', '2019-09-16 16:07:59', 4),
(35, 'Виолетти', '[102,103]', 50, 2790, '1000', NULL, '<p>Хрустящие вафельные трубочки с ванильной ноткой</p>\r\n\r\n<p>в сливочном маршмеллоу</p>\r\n\r\n<p>и ягодки бабл гам.</p>', '2019-09-16 15:06:24', '2019-09-16 15:07:13', 5),
(36, 'Бананза', '[108,109]', 51, 1590, '360', NULL, '<p>Апельсиновый маршмеллоу с дольками арбуза, апельсина, лимона. Огромные бананы и персиковые мармеладные ягоды.</p>', '2019-09-16 15:09:29', '2019-09-16 15:10:55', 3),
(37, 'Бананза', '[110,111]', 51, 2190, '580', NULL, '<p>Апельсиновый маршмеллоу с дольками арбуза, апельсина, лимона. Огромные бананы и персиковые мармеладные ягоды.</p>', '2019-09-16 15:10:28', '2019-09-16 15:10:55', 4),
(38, 'Бананза', '[112,113,114,115]', 51, 2690, '1000', '1', '<p>Апельсиновый маршмеллоу с дольками арбуза, апельсина, лимона. Огромные бананы и персиковые мармеладные ягоды.</p>', '2019-09-16 15:10:56', '2019-09-16 15:10:56', 5),
(39, 'Флоренция', '[117,118]', 52, 1690, '360', NULL, '<p>Воздушное суфле сливочно-ванильного вкуса</p>\r\n\r\n<p>с хрустящими трубочками.</p>\r\n\r\n<p>Запах Флоренции.</p>', '2019-09-16 15:12:31', '2019-09-16 15:13:42', 3),
(40, 'Флоренция', '[119,120]', 52, 2190, '580', NULL, '<p>Воздушное суфле сливочно-ванильного вкуса</p>\r\n\r\n<p>с хрустящими трубочками.</p>\r\n\r\n<p>Запах Флоренции.</p>', '2019-09-16 15:13:17', '2019-09-16 15:13:42', 4),
(41, 'Флоренция', '[121,122,123,124,125]', 52, 2690, '1000', '1', '<p>Воздушное суфле сливочно-ванильного вкуса</p>\r\n\r\n<p>с хрустящими трубочками.</p>\r\n\r\n<p>Запах Флоренции.</p>', '2019-09-16 15:13:43', '2019-09-16 15:13:43', 5),
(42, 'Вэлэнтайн', '[127,128,129,130]', 53, 2490, '1000', '1', '<p>Нежное классическое маршмеллоу с ванильным ароматом в форме сердца.</p>', '2019-09-16 15:15:49', '2019-09-16 15:15:49', 5),
(43, 'Радуга', '[132,133]', 54, 1690, '360', '1', '<p>Вы почувствуете неповторимое сочетание</p>\r\n\r\n<p>аромата нежной ванили</p>\r\n\r\n<p>с легкими фруктовыми оттенками.</p>', '2019-09-16 15:17:16', '2019-09-16 15:17:16', 3),
(44, 'Радуга', '[134,135,136]', 54, 2290, '580', '1', '<p>Вы почувствуете неповторимое сочетание</p>\r\n\r\n<p>аромата нежной ванили</p>\r\n\r\n<p>с легкими фруктовыми оттенками.</p>', '2019-09-16 15:17:48', '2019-09-16 15:17:48', 4),
(45, 'Радуга', '[137,138]', 54, 2790, '1000', NULL, '<p>Вы почувствуете неповторимое сочетание</p>\r\n\r\n<p>аромата нежной ванили</p>\r\n\r\n<p>с легкими фруктовыми оттенками.</p>', '2019-09-16 15:18:11', '2019-09-16 15:18:11', 5),
(46, 'Лаванда', '[140,141,142]', 55, 2990, '1000', '1', '<p>Букет суфле с тонкой ноткой лесных ягод и удивительными ежевичными мармеладками.</p>', '2019-09-16 15:20:52', '2019-09-16 15:20:52', 5),
(47, 'Африка', '[144,145,146]', 56, 2190, '580', '1', '<p>Микс из ванильных трубочек,</p>\r\n\r\n<p>сахарных маргариток,</p>\r\n\r\n<p>персиковых мармеладок</p>\r\n\r\n<p>и мега суфле.</p>', '2019-09-16 15:22:31', '2019-09-16 15:22:31', 4),
(49, 'Индиго', '[154,155]', 58, 1690, '360', '1', '<p>Взрывной букет из маршмеллоу лесные ягоды &nbsp;</p>\r\n\r\n<p>с насыщенным вкусом персиковых</p>\r\n\r\n<p>и бабл гам мармеладок.</p>', '2019-09-16 15:35:02', '2019-09-16 15:35:02', 3),
(50, 'Индиго', '[156]', 58, 2190, '580', NULL, '<p>Взрывной букет из маршмеллоу лесные ягоды &nbsp;</p>\r\n\r\n<p>с насыщенным вкусом персиковых</p>\r\n\r\n<p>и бабл гам мармеладок.</p>', '2019-09-16 15:35:41', '2019-09-16 15:36:39', 4),
(51, 'Индиго', '[157]', 58, 2690, '1000', NULL, '<p>Взрывной букет из маршмеллоу лесные ягоды &nbsp;</p>\r\n\r\n<p>с насыщенным вкусом персиковых</p>\r\n\r\n<p>и бабл гам мармеладок.</p>', '2019-09-16 15:36:08', '2019-09-16 15:36:08', 5),
(52, 'Лето', '[159,160]', 59, 1690, '360', '1', '<p>Сахарные купола с запахом ванили</p>\r\n\r\n<p>и жевательный мармелад</p>\r\n\r\n<p>со вкусом персика в карамельной обсыпке.</p>', '2019-09-16 15:38:36', '2019-09-16 15:38:36', 3),
(53, 'Лето', '[161,162,163,164]', 59, 2490, '1000', '1', '<p>Сахарные купола с запахом ванили</p>\r\n\r\n<p>и жевательный мармелад</p>\r\n\r\n<p>со вкусом персика в карамельной обсыпке.</p>', '2019-09-16 15:39:37', '2019-09-16 15:39:37', 4),
(54, 'Лето', '[165,166,167]', 59, 2990, '2000', NULL, '<p>Сахарные купола с запахом ванили</p>\r\n\r\n<p>и жевательный мармелад</p>\r\n\r\n<p>со вкусом персика в карамельной обсыпке.</p>', '2019-09-16 15:40:03', '2019-09-16 15:40:03', 5),
(55, 'Фламинго', '[169,170]', 60, 1690, '360', '1', '<p>Мы предложили вам суфле со вкусом мороженного в виде рожка. Так же малиновые ягоды и сливочное маршмеллоу.</p>', '2019-09-16 15:42:13', '2019-09-16 15:42:13', 3),
(56, 'Фламинго', '[171,172,173]', 60, 2190, '580', '1', '<p>Мы предложили вам суфле со вкусом мороженного в виде рожка. Так же малиновые ягоды и сливочное маршмеллоу.<br />\r\n&nbsp;</p>', '2019-09-16 15:42:43', '2019-09-16 15:42:43', 4),
(66, 'Подсолнухи', '[198,199]', 62, 1590, '360', '1', '<p>Яркий сливочный суфле</p>\r\n\r\n<p>c дерзкой ноткой</p>\r\n\r\n<p>ежевичных мармеладок.</p>', '2019-09-16 15:55:09', '2019-09-16 15:55:09', 3),
(67, 'Подсолнухи', '[200,201,202]', 62, 2390, '580', '1', '<p>Яркий сливочный суфле</p>\r\n\r\n<p>c дерзкой ноткой</p>\r\n\r\n<p>ежевичных мармеладок.</p>', '2019-09-16 15:55:30', '2019-09-16 15:55:30', 4),
(68, 'Верано', '[204,205]', 63, 1590, '360', '1', '<p>Букет из воздушных сахарных сливочных маргариток с добавлением эксклюзивной ванильной мастики.</p>', '2019-09-16 16:00:38', '2019-09-16 16:00:38', 3),
(69, 'Марсель', '[207,208,209]', 64, 3890, '1500', '1', '<p>Самый мармеладный букет!</p>', '2019-09-16 16:01:58', '2019-09-16 16:01:58', 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `surname`, `adress`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.com', 'Admin', NULL, NULL, NULL, '$2y$10$6C7ejF8FPb2OMR2buBQKbOmk1dL9l3b.pE8N9VALuXoZpjpqnArDe', 'YIEIsSyUbe3fvS0QafF72EhXziqfHoEgaO5WWvIBe0EnhhOEWYLRgM6ArKoU', '2019-06-10 14:37:44', '2019-06-10 14:37:44');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
