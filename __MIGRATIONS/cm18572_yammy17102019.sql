-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Окт 17 2019 г., 14:59
-- Версия сервера: 5.6.39-83.1
-- Версия PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cm18572_yammy`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Букеты', '2019-06-10 14:39:26', '2019-06-10 14:39:26'),
(2, 'Открытки', '2019-06-17 14:53:19', '2019-06-17 14:53:19'),
(3, 'Топпер', '2019-10-11 09:52:28', '2019-10-11 09:52:28');

-- --------------------------------------------------------

--
-- Структура таблицы `coupons`
--

CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `discount`, `end_date`, `created_at`, `updated_at`) VALUES
(3, '001-508566-758', 10, NULL, '2019-06-14 10:04:37', '2019-06-14 10:04:37'),
(4, '222', 10, NULL, '2019-09-09 05:19:40', '2019-09-09 05:19:40');

-- --------------------------------------------------------

--
-- Структура таблицы `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `from` int(10) UNSIGNED NOT NULL,
  `to` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_01_02_131905_create_products_table', 1),
(4, '2018_01_02_132136_create_categories_table', 1),
(5, '2018_01_02_132533_create_subcategories_table', 1),
(6, '2018_01_02_213025_create_roles_table', 1),
(7, '2018_01_06_000752_create_photos_table', 1),
(8, '2018_01_09_092104_create_faqs_table', 1),
(9, '2018_01_12_163545_create_orders_table', 1),
(10, '2019_06_01_101336_change_discoutn_type_in_table_product', 1),
(11, '2019_06_01_134942_delete_mig_priority', 1),
(12, '2019_06_08_151732_create_text_table', 1),
(13, '2019_06_10_092018_create_feedback_table', 1),
(14, '2019_06_10_133104_change_order_table', 1),
(15, '2019_06_10_175352_create_shops_table', 2),
(16, '2019_06_12_085801_create_texts_table', 3),
(17, '2019_06_13_163650_create_coupons_table', 4),
(18, '2019_06_14_122203_delete_from_users', 5),
(19, '2019_06_14_130527_add_email_orders', 6),
(20, '2019_06_14_131630_create_messages_table', 7),
(21, '2019_06_17_180351_update_product', 8),
(22, '2019_06_18_141645_create_reviews_table', 9),
(23, '2019_06_22_085215_update_table_text', 10),
(24, '2019_09_04_132037_delete_from_catalog_and_add', 11),
(25, '2019_09_04_140418_create_size_table', 12),
(26, '2019_09_04_143616_create_tproducts_table', 13),
(27, '2019_09_04_145414_add_t_size', 14);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `orderId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` int(11) DEFAULT NULL,
  `products` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery` int(11) DEFAULT NULL,
  `name2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ascension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `orderId`, `status`, `totalAmount`, `products`, `adress`, `created_at`, `updated_at`, `name`, `phone`, `delivery`, `name2`, `phone2`, `street`, `house`, `flat`, `ascension`, `message`, `email`) VALUES
(35, '5d91c0b6ae86c', 'PENDING', 229000, '34-1-2', '___', '2019-09-30 05:45:42', '2019-09-30 05:45:42', 'наталья', '+7(903) 671-49-06', 0, NULL, NULL, 'нижегородская', '67/2', NULL, NULL, 'детский сад, доставка на 3 октября', 'dou350@yandex.ru'),
(36, '5d94848e9afa9', 'PENDING', 219000, '47-1-1', '___', '2019-10-02 08:05:50', '2019-10-02 08:05:50', 'Константин', '+7(917) 548-75-05', 0, NULL, NULL, 'г. Москва', '2', '153', '6', 'Домофон.153к3120', NULL),
(40, '5d970e0503814', 'PENDING', 169000, '9491-1-1', '___', '2019-10-04 06:16:53', '2019-10-04 06:16:53', 'Елена', '+7(927) 773-27-35', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, '5d9a099434350', 'PENDING', 279000, '9451-1-3', '___', '2019-10-06 12:34:44', '2019-10-06 12:34:44', 'Лидия', '+7(910) 486-35-82', 0, NULL, NULL, 'Лужнецкая набережная', '8', NULL, NULL, 'Олимпийский коммитет России', 'lida139@yandex.ru'),
(42, '5d9b564d7590c', 'PENDING', 249000, '9321-1-1', '___', '2019-10-07 12:14:21', '2019-10-07 12:14:21', 'Михаил', '+7(926) 211-11-72', 0, NULL, NULL, 'Окружной проезд', '22/64', '95', '3', 'Код домофона 70В7050.', NULL),
(43, '5d9f3b9b78911', 'PENDING', 220000, '9701-1-1', '___', '2019-10-10 11:09:31', '2019-10-10 11:09:31', 'Юлия', '+7(903) 298-08-97', 0, NULL, NULL, 'Севастопольский пр-т', '71', '25', '2', NULL, '2980897@gmail.com'),
(44, '5da627fd0c610', 'PENDING', 259000, '9321-1-1', '___', '2019-10-15 17:11:41', '2019-10-15 17:11:41', 'From iphone Test', '+7(111) 111-11-11', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Test@test.com'),
(45, '5da83109129b8', 'PENDING', 178000, '9681-1-1|9772-1-1', '___', '2019-10-17 06:14:49', '2019-10-17 06:14:49', 'Диана', '+7(926) 228-03-54', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dkusova@mail.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=374 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `photos`
--

INSERT INTO `photos` (`id`, `file`, `created_at`, `updated_at`) VALUES
(13, '1567880867Z2oGCXvOaCP5VWo.png', '2019-09-07 15:27:48', '2019-09-07 15:27:48'),
(14, '1567880942FR5Sfj4whNOFa9B.jpg', '2019-09-07 15:29:03', '2019-09-07 15:29:03'),
(15, '1567880957fv9ZvyCkCXowFjB.png', '2019-09-07 15:29:18', '2019-09-07 15:29:18'),
(17, '1567881255WV33x4hs2FwYIPY.png', '2019-09-07 15:34:16', '2019-09-07 15:34:16'),
(18, '1567881278QOlkWl5YtHF0crQ.png', '2019-09-07 15:34:39', '2019-09-07 15:34:39'),
(19, '1567881311LI4xwJdS2NgqBgk.png', '2019-09-07 15:35:12', '2019-09-07 15:35:12'),
(20, '1567881367XD8hQ2DuYqw64Ut.png', '2019-09-07 15:36:08', '2019-09-07 15:36:08'),
(68, '1568624383c927fGwDpKkg57M.jpg', '2019-09-16 05:59:44', '2019-09-16 05:59:44'),
(77, '15686531582oZacdlLtncvoyh.jpg', '2019-09-16 13:59:19', '2019-09-16 13:59:19'),
(86, '15686566366fsAnX28CWRh3Cm.jpg', '2019-09-16 14:57:16', '2019-09-16 14:57:16'),
(95, '1568657057iO1Y6eiBlDvAmLX.jpg', '2019-09-16 15:04:17', '2019-09-16 15:04:17'),
(96, '1568657102y92skwNw37Jj91J.jpg', '2019-09-16 15:05:02', '2019-09-16 15:05:02'),
(97, '1568657102yGEikwctiDSv5bl.jpg', '2019-09-16 15:05:02', '2019-09-16 15:05:02'),
(102, '1568657184q8bG9V2hhRCHmlX.jpg', '2019-09-16 15:06:24', '2019-09-16 15:06:24'),
(103, '1568657184Jrb8wadI4swHMt2.jpg', '2019-09-16 15:06:24', '2019-09-16 15:06:24'),
(104, '15686572341AXVlOby9gFOHJ6.jpg', '2019-09-16 15:07:14', '2019-09-16 15:07:14'),
(105, '1568657234QoLlEmfWhoMZAMZ.jpg', '2019-09-16 15:07:14', '2019-09-16 15:07:14'),
(106, '1568657234m8Oo2eEmzZtWZd3.jpg', '2019-09-16 15:07:14', '2019-09-16 15:07:14'),
(107, '1568657320jgtHxyha1pcr6yg.jpg', '2019-09-16 15:08:40', '2019-09-16 15:08:40'),
(116, '1568657514SAfRseb8DhgfhZ1.jpg', '2019-09-16 15:11:54', '2019-09-16 15:11:54'),
(117, '15686575515pPo933atXXDAad.jpg', '2019-09-16 15:12:31', '2019-09-16 15:12:31'),
(118, '1568657551KiQ96xt4NzDPJan.jpg', '2019-09-16 15:12:31', '2019-09-16 15:12:31'),
(119, '1568657597pVtbase48ZuOz28.jpg', '2019-09-16 15:13:17', '2019-09-16 15:13:17'),
(120, '1568657597JOgq9G6oxqF1Dco.jpg', '2019-09-16 15:13:17', '2019-09-16 15:13:17'),
(121, '156865762241ZX8uckPb8qsnd.jpg', '2019-09-16 15:13:42', '2019-09-16 15:13:42'),
(122, '1568657622tqxpTmFdbNqd9Z7.jpg', '2019-09-16 15:13:42', '2019-09-16 15:13:42'),
(123, '1568657622ryYhZdRUl8XyyJO.jpg', '2019-09-16 15:13:42', '2019-09-16 15:13:42'),
(124, '1568657622ikOMgYc9gUFfWhv.jpg', '2019-09-16 15:13:42', '2019-09-16 15:13:42'),
(125, '1568657622zBuZXgpxsisSxWG.jpg', '2019-09-16 15:13:43', '2019-09-16 15:13:43'),
(126, '1568657702ozR1M94kU5aVmI2.jpg', '2019-09-16 15:15:02', '2019-09-16 15:15:02'),
(131, '1568657803drclGbgV5bN2pOF.jpg', '2019-09-16 15:16:43', '2019-09-16 15:16:43'),
(139, '1568657985a8N7J265p0kM3Hv.jpg', '2019-09-16 15:19:45', '2019-09-16 15:19:45'),
(143, '1568658110kyLknWgUdKydZyl.jpg', '2019-09-16 15:21:51', '2019-09-16 15:21:51'),
(147, '1568658205R7ACZrZa0l4YpZj.jpg', '2019-09-16 15:23:25', '2019-09-16 15:23:25'),
(152, '1568658255HquTKnTyqDlZ06Q.jpg', '2019-09-16 15:24:16', '2019-09-16 15:24:16'),
(153, '1568658853xI7iDEsJKi8fJgN.jpg', '2019-09-16 15:34:14', '2019-09-16 15:34:14'),
(158, '1568659083ejVC0DQj1vHr5Du.jpg', '2019-09-16 15:38:03', '2019-09-16 15:38:03'),
(168, '15686593011dBDbONSTzZTpxq.jpg', '2019-09-16 15:41:41', '2019-09-16 15:41:41'),
(174, '1568659430IJsCuRD6xSRBGXf.jpg', '2019-09-16 15:43:50', '2019-09-16 15:43:50'),
(179, '1568659651o9zYr6xB8w3lFVH.jpg', '2019-09-16 15:47:31', '2019-09-16 15:47:31'),
(187, '1568659863MNefUmixszlZKSx.jpg', '2019-09-16 15:51:03', '2019-09-16 15:51:03'),
(203, '1568660404h0zf1MXT84paLMM.jpg', '2019-09-16 16:00:04', '2019-09-16 16:00:04'),
(204, '1568660437hca2Y5tCWcge6UH.jpg', '2019-09-16 16:00:37', '2019-09-16 16:00:37'),
(205, '1568660438PhOMf4sCBkp4X3s.jpg', '2019-09-16 16:00:38', '2019-09-16 16:00:38'),
(206, '1568660490grUhQ0jGJsSkr03.jpg', '2019-09-16 16:01:30', '2019-09-16 16:01:30'),
(210, '1568661002w5DqNUBy8gSS7mi.jpg', '2019-09-16 16:10:02', '2019-09-16 16:10:02'),
(211, '1568661019TnkuTqzDFj4z4zZ.jpg', '2019-09-16 16:10:19', '2019-09-16 16:10:19'),
(212, '1568661138msIeP7yrtkZvoWl.jpg', '2019-09-16 16:12:18', '2019-09-16 16:12:18'),
(213, '1568661149LNuezJh46QaxkhI.jpg', '2019-09-16 16:12:29', '2019-09-16 16:12:29'),
(214, '1568661162sEEVYik63nU6gqJ.jpg', '2019-09-16 16:12:42', '2019-09-16 16:12:42'),
(215, '1568661176X1kA6bLcak7cKR8.jpg', '2019-09-16 16:12:56', '2019-09-16 16:12:56'),
(216, '1568661187xb6pMfGQrc9Onzp.jpg', '2019-09-16 16:13:07', '2019-09-16 16:13:07'),
(218, '1568897067zb6GdJqjeSZbVu3.jpg', '2019-09-19 09:44:28', '2019-09-19 09:44:28'),
(219, '1568897090H4ocJb5cyQsmJC2.jpg', '2019-09-19 09:44:51', '2019-09-19 09:44:51'),
(220, '1568897153Th6ueh6duLYjkD2.jpg', '2019-09-19 09:45:54', '2019-09-19 09:45:54'),
(221, '15688971886reArUai9H0Ebb9.jpg', '2019-09-19 09:46:28', '2019-09-19 09:46:28'),
(224, '1568897387N4NnNpIsuhFUCtp.jpg', '2019-09-19 09:49:47', '2019-09-19 09:49:47'),
(244, '1570564368zogUrXzMa5LRnE0.JPG', '2019-10-08 16:52:48', '2019-10-08 16:52:48'),
(245, '1570564390sjeSkvj7iK5ArlB.JPG', '2019-10-08 16:53:10', '2019-10-08 16:53:10'),
(246, '1570564390IVIXHYt66n9xtV0.JPG', '2019-10-08 16:53:10', '2019-10-08 16:53:10'),
(247, '1570564390HGbO2HChfIiJJX5.JPG', '2019-10-08 16:53:10', '2019-10-08 16:53:10'),
(248, '1570564496Hvo3i7eiCxYL4IY.JPG', '2019-10-08 16:54:56', '2019-10-08 16:54:56'),
(249, '1570565319jtkYP2OsFiBe62L.jpg', '2019-10-08 17:08:39', '2019-10-08 17:08:39'),
(250, '15705653198FGclV3FvsHcSjV.jpg', '2019-10-08 17:08:39', '2019-10-08 17:08:39'),
(251, '1570565352IDeNU7koud2K4AA.jpg', '2019-10-08 17:09:13', '2019-10-08 17:09:13'),
(252, '15705653537eRQWfVJwFKxtOv.JPG', '2019-10-08 17:09:13', '2019-10-08 17:09:13'),
(253, '1570565353KEvgFTDLAReLsCR.jpg', '2019-10-08 17:09:13', '2019-10-08 17:09:13'),
(254, '1570565561JlR0uXNQX12wXrH.jpg', '2019-10-08 17:12:41', '2019-10-08 17:12:41'),
(255, '1570565827s2O2mj09F3xOAh6.JPG', '2019-10-08 17:17:07', '2019-10-08 17:17:07'),
(258, '15705658777nPV4yyJQ2TeBDI.JPG', '2019-10-08 17:17:57', '2019-10-08 17:17:57'),
(259, '1570565877Sbsks0DgIordaSI.JPG', '2019-10-08 17:17:57', '2019-10-08 17:17:57'),
(260, '157056587798M7i0QX4X7CJZy.JPG', '2019-10-08 17:17:57', '2019-10-08 17:17:57'),
(261, '1570565894ceeRejAArxp4XWQ.jpg', '2019-10-08 17:18:14', '2019-10-08 17:18:14'),
(262, '1570565894MouCQM75t2onw82.jpg', '2019-10-08 17:18:14', '2019-10-08 17:18:14'),
(263, '1570565913yDTHdectQQHR70J.jpg', '2019-10-08 17:18:33', '2019-10-08 17:18:33'),
(264, '1570565913qiu2VbgC9IwQwRY.jpg', '2019-10-08 17:18:33', '2019-10-08 17:18:33'),
(265, '1570566241G7qhl2vUFkTDckz.JPG', '2019-10-08 17:24:01', '2019-10-08 17:24:01'),
(266, '1570566256mjRtMnj0QsSLxaH.jpg', '2019-10-08 17:24:17', '2019-10-08 17:24:17'),
(267, '1570566257uNBrV23DRVw6nN8.JPG', '2019-10-08 17:24:17', '2019-10-08 17:24:17'),
(271, '1570566297rVQTTA5jRpx5MfZ.jpg', '2019-10-08 17:24:58', '2019-10-08 17:24:58'),
(272, '1570566298weCAyTrr6TqUmZu.jpg', '2019-10-08 17:24:58', '2019-10-08 17:24:58'),
(273, '1570566298mdSFxpRZVfinuIq.jpg', '2019-10-08 17:24:58', '2019-10-08 17:24:58'),
(278, '1570567554vJR1v8crQcnfNKC.jpg', '2019-10-08 17:45:54', '2019-10-08 17:45:54'),
(279, '1570567554gTPIULyrjg52apN.JPG', '2019-10-08 17:45:54', '2019-10-08 17:45:54'),
(280, '1570567554hVCL5R9XeS1xxvF.JPG', '2019-10-08 17:45:54', '2019-10-08 17:45:54'),
(281, '1570567554Osrbhl577HiI07F.jpg', '2019-10-08 17:45:54', '2019-10-08 17:45:54'),
(282, '1570567572XUGdqcKYEWGOjub.jpg', '2019-10-08 17:46:12', '2019-10-08 17:46:12'),
(283, '1570567572CM9FBUBvBNtvAsr.jpg', '2019-10-08 17:46:12', '2019-10-08 17:46:12'),
(284, '1570567584wKdnj97NCekeBSk.jpg', '2019-10-08 17:46:24', '2019-10-08 17:46:24'),
(285, '1570567584OgqiMWHXmJKz02e.jpg', '2019-10-08 17:46:24', '2019-10-08 17:46:24'),
(286, '157056818003nlOJhjmaw6R0r.jpg', '2019-10-08 17:56:21', '2019-10-08 17:56:21'),
(287, '1570568223ZK9M27MqcMemO1z.JPG', '2019-10-08 17:57:03', '2019-10-08 17:57:03'),
(288, '15705682230dM3Y7XWvRYJaoH.jpg', '2019-10-08 17:57:03', '2019-10-08 17:57:03'),
(289, '1570568237RreYdNOnfJGtp2o.jpg', '2019-10-08 17:57:17', '2019-10-08 17:57:17'),
(290, '1570568237FqgH8KoH5bGERVa.JPG', '2019-10-08 17:57:17', '2019-10-08 17:57:17'),
(291, '1570568237v7FtPakASuyN8On.JPG', '2019-10-08 17:57:17', '2019-10-08 17:57:17'),
(292, '1570568246ftbqGsvlfRCnjWk.jpg', '2019-10-08 17:57:26', '2019-10-08 17:57:26'),
(293, '1570568246TVTyapHgBXAEsqN.jpg', '2019-10-08 17:57:26', '2019-10-08 17:57:26'),
(294, '1570568258nfUkTzaHkdVuuvb.jpg', '2019-10-08 17:57:38', '2019-10-08 17:57:38'),
(305, '1570570702y6IdNuEVDiR7RHM.jpg', '2019-10-08 18:38:22', '2019-10-08 18:38:22'),
(310, '1570570855photo_2019-06-18_20-58-19.jpg', '2019-10-08 18:40:55', '2019-10-08 18:40:55'),
(311, '1570570858photo_2019-06-18_20-58-22.jpg', '2019-10-08 18:40:58', '2019-10-08 18:40:58'),
(312, '15706073723Bw5NrlPPJ6jjcz.jpg', '2019-10-09 04:49:32', '2019-10-09 04:49:32'),
(313, '1570607389J9Cvr6XWQfEcfVS.jpg', '2019-10-09 04:49:49', '2019-10-09 04:49:49'),
(314, '1570607389POPWY8FPVZaCqM5.jpg', '2019-10-09 04:49:49', '2019-10-09 04:49:49'),
(319, '15706074203XCoeg82qGHIlMq.jpg', '2019-10-09 04:50:20', '2019-10-09 04:50:20'),
(320, '1570607420adNvSJWoijEl0Rb.jpg', '2019-10-09 04:50:20', '2019-10-09 04:50:20'),
(321, '1570607420Bbs7zZrSI0JExey.JPG', '2019-10-09 04:50:20', '2019-10-09 04:50:20'),
(322, '1570607448RMvtjS8jHAoPnzw.JPG', '2019-10-09 04:50:48', '2019-10-09 04:50:48'),
(323, '1570607448UdlU5m5PHrnCanN.jpg', '2019-10-09 04:50:48', '2019-10-09 04:50:48'),
(324, '1570607448lI1IzxmhizOYhRe.jpg', '2019-10-09 04:50:48', '2019-10-09 04:50:48'),
(325, '1570607448i4mNw1EYu9Ww1LN.jpg', '2019-10-09 04:50:48', '2019-10-09 04:50:48'),
(326, '1570607773Y0kx5ExornoKS3Y.JPG', '2019-10-09 04:56:13', '2019-10-09 04:56:13'),
(327, '1570607789aDTI1VZhoCjEHkX.jpg', '2019-10-09 04:56:29', '2019-10-09 04:56:29'),
(328, '1570607789pi0SoapetANbbwq.jpg', '2019-10-09 04:56:29', '2019-10-09 04:56:29'),
(329, '1570607801BgYR7EeWVQB3SbW.JPG', '2019-10-09 04:56:41', '2019-10-09 04:56:41'),
(330, '1570607811eGKCYHFa87Unutv.JPG', '2019-10-09 04:56:51', '2019-10-09 04:56:51'),
(331, '1570608083yHMNICU4tfcLXAa.jpg', '2019-10-09 05:01:23', '2019-10-09 05:01:23'),
(332, '1570608100r84i3mcGEjXypwU.jpg', '2019-10-09 05:01:40', '2019-10-09 05:01:40'),
(333, '1570608100RNgNwRW5GTwOCkS.jpg', '2019-10-09 05:01:40', '2019-10-09 05:01:40'),
(334, '1570608100O3oZPjET5q9BvJj.jpg', '2019-10-09 05:01:40', '2019-10-09 05:01:40'),
(335, '1570608100k8S4bk1l4cLOTyI.jpg', '2019-10-09 05:01:40', '2019-10-09 05:01:40'),
(336, '1570608269Gk3Y5uKAbZspdSh.JPG', '2019-10-09 05:04:30', '2019-10-09 05:04:30'),
(337, '1570608302GnmGto9ZkJXKYi5.jpg', '2019-10-09 05:05:02', '2019-10-09 05:05:02'),
(338, '1570608302kYoUAKQOthB45rK.jpg', '2019-10-09 05:05:02', '2019-10-09 05:05:02'),
(339, '1570608302HGlqy4EZeXvQElH.JPG', '2019-10-09 05:05:02', '2019-10-09 05:05:02'),
(340, '1570608532iscIoaQkrp4Sc0K.jpg', '2019-10-09 05:08:52', '2019-10-09 05:08:52'),
(341, '1570608554tJ0AaoZZdyANpoG.JPG', '2019-10-09 05:09:14', '2019-10-09 05:09:14'),
(342, '1570608554Rq6MkPpXKeieLut.JPG', '2019-10-09 05:09:14', '2019-10-09 05:09:14'),
(343, '15706085541MG47Miuy6oz9a9.JPG', '2019-10-09 05:09:14', '2019-10-09 05:09:14'),
(344, '1570608973pGsl2SOB40SOb84.JPG', '2019-10-09 05:16:13', '2019-10-09 05:16:13'),
(345, '1570608991BGW2eU7b59aMzP4.jpg', '2019-10-09 05:16:31', '2019-10-09 05:16:31'),
(346, '1570608991VCzbEdOprl9ixUa.jpg', '2019-10-09 05:16:31', '2019-10-09 05:16:31'),
(347, '1570608991hqdgmCvedSpxD8R.jpg', '2019-10-09 05:16:31', '2019-10-09 05:16:31'),
(348, '1570608991dLLR09cr0gKLqYX.JPG', '2019-10-09 05:16:31', '2019-10-09 05:16:31'),
(349, '15707987919zZ9zQKEpLrOQQd.jpg', '2019-10-11 09:59:51', '2019-10-11 09:59:51'),
(350, '1570798933qvf0ykihLSNp4KR.JPG', '2019-10-11 10:02:13', '2019-10-11 10:02:13'),
(351, '1570799099GVQxCtDa9FvWaih.JPG', '2019-10-11 10:04:59', '2019-10-11 10:04:59'),
(352, '1570799114WCJv2hWlG06Ljls.jpg', '2019-10-11 10:05:14', '2019-10-11 10:05:14'),
(353, '157115988995LuVex533Y6M7Q.jpg', '2019-10-15 14:18:09', '2019-10-15 14:18:09'),
(354, '15711599654q0q8MJvrNNzGS0.jpg', '2019-10-15 14:19:25', '2019-10-15 14:19:25'),
(355, '1571160445zIOcQQxzrgF0irF.jpg', '2019-10-15 14:27:25', '2019-10-15 14:27:25'),
(356, '1571166535d5ChgKSBhpw7y2U.jpg', '2019-10-15 16:08:55', '2019-10-15 16:08:55'),
(357, '1571168627T8OaAWivTl2CnZG.jpg', '2019-10-15 16:43:47', '2019-10-15 16:43:47'),
(370, '1571170909Z7Ybf61r7wpNOCl.jpg', '2019-10-15 17:21:49', '2019-10-15 17:21:49'),
(371, '15711709095kTqRTG4G4rkWNZ.jpg', '2019-10-15 17:21:49', '2019-10-15 17:21:49'),
(372, '1571170909MQUfR0jCON9JXGS.jpg', '2019-10-15 17:21:50', '2019-10-15 17:21:50'),
(373, '1571170910i98TVFXc4S5Jxj8.jpg', '2019-10-15 17:21:50', '2019-10-15 17:21:50');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_id` smallint(5) UNSIGNED DEFAULT NULL,
  `category_id` smallint(5) UNSIGNED DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `released_on` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '500',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `name`, `photo_id`, `category_id`, `discount`, `released_on`, `created_at`, `updated_at`, `price`, `color`, `sort`) VALUES
(49, 'Амур', 356, 1, 10, NULL, '2019-09-16 14:57:17', '2019-10-15 16:08:56', NULL, '#fadcf8', 1),
(50, 'Виолетти', 357, 1, NULL, NULL, '2019-09-16 15:04:19', '2019-10-15 16:43:47', NULL, '#31dbf4', 2),
(51, 'Бананза', 107, 1, NULL, NULL, '2019-09-16 15:08:40', '2019-09-29 08:40:44', NULL, '#e9b77a', 4),
(52, 'Флоренция', 116, 1, NULL, NULL, '2019-09-16 15:11:54', '2019-09-29 08:39:17', NULL, '#f9f7be', 44),
(53, 'Вэлэнтайн', 344, 1, NULL, NULL, '2019-09-16 15:15:02', '2019-10-09 05:16:13', NULL, '#df9eb4', 5),
(54, 'Радуга', 354, 1, NULL, NULL, '2019-09-16 15:16:43', '2019-10-15 14:19:25', NULL, '#8ff3aa', 3),
(55, 'Лаванда', 340, 1, NULL, NULL, '2019-09-16 15:19:45', '2019-10-09 05:08:52', NULL, '#eed3f2', 7),
(56, 'Африка', 336, 1, NULL, NULL, '2019-09-16 15:21:51', '2019-10-09 05:04:30', NULL, '#ecc76e', 8),
(57, 'Гармония', 331, 1, NULL, NULL, '2019-09-16 15:23:25', '2019-10-09 05:01:23', NULL, '#c0d5fd', 9),
(58, 'Индиго', 326, 1, NULL, NULL, '2019-09-16 15:34:14', '2019-10-09 04:56:13', NULL, '#d8aaf5', 10),
(59, 'Лето', 312, 1, NULL, NULL, '2019-09-16 15:38:03', '2019-10-09 04:49:32', NULL, '#eef08f', 11),
(60, 'Фламинго', 265, 1, NULL, NULL, '2019-09-16 15:41:41', '2019-10-08 17:24:01', NULL, '#f999be', 11),
(61, 'Бэлла', 255, 1, NULL, NULL, '2019-09-16 15:43:50', '2019-10-08 17:17:07', NULL, '#f3a2b5', 13),
(62, 'Подсолнухи', 355, 1, 15, NULL, '2019-09-16 15:51:03', '2019-10-15 14:27:25', NULL, '#f3f3b3', 12),
(63, 'Верано', 203, 1, NULL, NULL, '2019-09-16 16:00:04', '2019-09-16 16:00:04', NULL, '#d3e7d5', 14),
(64, 'Марсель', 248, 1, NULL, NULL, '2019-09-16 16:01:30', '2019-10-08 16:54:56', NULL, '#f58c6c', 14),
(65, 'Открытка', 210, 2, NULL, NULL, '2019-09-16 16:10:02', '2019-10-11 10:06:30', '190', NULL, 7),
(66, 'Открытка', 211, 2, NULL, NULL, '2019-09-16 16:10:19', '2019-10-11 10:07:00', '190', NULL, 10),
(67, 'Открытка', 212, 2, NULL, NULL, '2019-09-16 16:12:18', '2019-09-16 16:12:18', '190', NULL, 500),
(68, 'Открытка', 213, 2, NULL, NULL, '2019-09-16 16:12:29', '2019-10-11 10:06:40', '190', NULL, 8),
(69, 'Открытка', 214, 2, NULL, NULL, '2019-09-16 16:12:42', '2019-10-11 10:06:53', '190', NULL, 9),
(70, 'Открытка', 215, 2, NULL, NULL, '2019-09-16 16:12:56', '2019-10-11 10:06:17', '190', NULL, 5),
(71, 'Открытка', 216, 2, NULL, NULL, '2019-09-16 16:13:07', '2019-10-11 10:06:08', '190', NULL, 3),
(75, 'Топпер', 350, 2, NULL, NULL, '2019-10-11 09:59:51', '2019-10-11 10:08:10', '190', NULL, 6),
(76, 'Топпер', 351, 2, NULL, NULL, '2019-10-11 10:04:59', '2019-10-11 10:08:21', '190', NULL, 4),
(77, 'Топпер', 352, 2, NULL, NULL, '2019-10-11 10:05:14', '2019-10-11 10:08:16', '190', NULL, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image` smallint(5) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id`, `video`, `image`, `created_at`, `updated_at`) VALUES
(66, NULL, 310, '2019-10-08 18:40:55', '2019-10-08 18:40:55'),
(67, NULL, 311, '2019-10-08 18:40:58', '2019-10-08 18:40:58');

-- --------------------------------------------------------

--
-- Структура таблицы `shops`
--

CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shops`
--

INSERT INTO `shops` (`id`, `name`, `address`, `lat`, `lng`, `created_at`, `updated_at`) VALUES
(1, 'Самовывоз', 'Строгинский бульвар 4 (метро Строгино)', '55.804616', '37.396617', NULL, '2019-10-03 07:57:32'),
(2, 'Самовывоз', 'улица Коккинаки д 4 (метро Аэропорт)', '55.804304', '37.546138', '2019-06-18 14:51:52', '2019-10-03 07:57:17'),
(3, 'Самовывоз', 'ТЦ Лига, Ленинградское шоссе вл 5', '55.888997', '37.433046', '2019-06-18 14:53:04', '2019-06-18 14:53:27');

-- --------------------------------------------------------

--
-- Структура таблицы `size`
--

CREATE TABLE IF NOT EXISTS `size` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `size`
--

INSERT INTO `size` (`id`, `name`, `created_at`, `updated_at`) VALUES
(3, 'S', '2019-09-04 11:31:35', '2019-09-04 11:31:35'),
(4, 'M', '2019-09-04 11:31:39', '2019-09-04 11:31:39'),
(5, 'L', '2019-09-04 11:31:42', '2019-09-04 11:31:42');

-- --------------------------------------------------------

--
-- Структура таблицы `subcategories`
--

CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subcategories_category_id_index` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `texts`
--

CREATE TABLE IF NOT EXISTS `texts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slider` text COLLATE utf8mb4_unicode_ci,
  `title2` text COLLATE utf8mb4_unicode_ci,
  `text2` text COLLATE utf8mb4_unicode_ci,
  `title3` text COLLATE utf8mb4_unicode_ci,
  `text3` text COLLATE utf8mb4_unicode_ci,
  `title4` text COLLATE utf8mb4_unicode_ci,
  `text4` text COLLATE utf8mb4_unicode_ci,
  `title5` text COLLATE utf8mb4_unicode_ci,
  `text5` text COLLATE utf8mb4_unicode_ci,
  `title6` text COLLATE utf8mb4_unicode_ci,
  `text6` text COLLATE utf8mb4_unicode_ci,
  `title7` text COLLATE utf8mb4_unicode_ci,
  `text7` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `privacy` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `texts`
--

INSERT INTO `texts` (`id`, `slider`, `title2`, `text2`, `title3`, `text3`, `title4`, `text4`, `title5`, `text5`, `title6`, `text6`, `title7`, `text7`, `created_at`, `updated_at`, `privacy`) VALUES
(1, '<h1>Сладкие воздушные<br />\r\nбукеты из зефира<br />\r\nи мармеладок</h1>', 'Используем самые качественные ингридиенты', '<p>Наши букеты из маршмеллоу и мармелада&nbsp;состоят из высококачественной продукции крупнейших европейских производителей, таких как Fini Sweets (Италия), Bulgari (Испания), Damel (Испания),&nbsp; Golmasa (Испания). Ведущие компании имеют огромный опыт и большую историю и продолжают традиции в кондитерском деле, имеют самые высокие международные оценки качества и безопасности пищевых продуктов, сертификаты IFS, BRC и ISO 9001: 2008.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Зефирные букеты всегда самого высшего качества и свежести, соблюдаются все условия хранения . Наше суфле остаётся мягким и свежим на протяжении долгого времени после изготовления букета. Мы собираем сладкие букеты согласно фитосанитарным нормам в перчатках и упаковываем в пищевую слюду за 2 часа до доставки, что позволяет гарантировать качество нашей продукции.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Букеты из маршмеллоу - всегда отличный подарок на любой праздник, юбилей, день рождение, свадьбу, 1 сентября учителю, 8 марта.</p>', 'Соблюдаем все санитарные нормы', '<h4 class=\"subtitle\">Букеты собираются в перчатках и упаковываются в плёнку</h4>', 'Доставка и оплата  <br> по Москве и области', '<p><strong>Бесплатная доставка</strong>&nbsp;по Москве в пределах МКАД осуществляется на следующий день после заказа.&nbsp;Возможна доставка день в день по согласованию.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Оплата курьеру&nbsp;<strong>наличными</strong>&nbsp;или банковским&nbsp;<strong>переводом</strong>&nbsp;на карту.&nbsp;</p>', 'Выбери свой букет', '<p>Наши букеты настолько вкусно пахнут, что перед доставкой букета мы кормим курьера зефиром, т.к. есть шанс, что он его съест</p>', 'Полезные сладости – это не миф', '<p>Основа зефира &ndash; фруктово-ягодное пюре. Его готовят из яблок, груш, клюквы, черной смородины и других плодов. Все они богаты витаминами и минералами, органическими кислотами и пектином.&nbsp;Воздушный зефир не содержит ни грамма жира, он легко переваривается, поэтому такое лакомство &laquo;не нагружает&raquo; наш желудок, как пирожные и торты. А за счет содержания пектина или другого желирующего агента зефир даже улучшает пищеварение. Сладкий букет может быть полезным!</p>', 'Хочешь сделать корпоративный заказ?', '<p>Оставь свой номер телефона и мы свяжемся с тобой в ближайшее время, что бы рассказать про условия.</p>', NULL, '2019-10-03 07:59:04', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tproducts`
--

CREATE TABLE IF NOT EXISTS `tproducts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` text COLLATE utf8mb4_unicode_ci,
  `product_id` smallint(5) UNSIGNED DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `mass` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `size` smallint(5) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tproducts`
--

INSERT INTO `tproducts` (`id`, `name`, `icon`, `product_id`, `price`, `mass`, `priority`, `desc`, `created_at`, `updated_at`, `size`) VALUES
(32, 'Амур', '[370,371,372,373]', 49, 2878, '1000', '1', '<p>Мармеладки-ягодки со вкусом йогурта и малины</p>\r\n\r\n<p>в элегантном сочетании с классическим сахарным суфле.</p>', '2019-09-16 14:58:17', '2019-10-15 17:21:50', 5),
(33, 'Виолетти', '[96,97]', 50, 1690, '360', NULL, '<p>Хрустящие вафельные трубочки с ванильной ноткой</p>\r\n\r\n<p>в сливочном маршмеллоу</p>\r\n\r\n<p>и ягодки бабл гам.</p>', '2019-09-16 15:05:02', '2019-09-16 16:07:52', 3),
(34, 'Виолетти', '[104,105,106]', 50, 2290, '580', '1', '<p>Хрустящие вафельные трубочки с ванильной ноткой</p>\r\n\r\n<p>в сливочном маршмеллоу</p>\r\n\r\n<p>и ягодки бабл гам.</p>', '2019-09-16 15:05:40', '2019-09-16 16:07:59', 4),
(35, 'Виолетти', '[102,103]', 50, 2790, '1000', NULL, '<p>Хрустящие вафельные трубочки с ванильной ноткой</p>\r\n\r\n<p>в сливочном маршмеллоу</p>\r\n\r\n<p>и ягодки бабл гам.</p>', '2019-09-16 15:06:24', '2019-09-16 15:07:13', 5),
(36, 'Бананза', '[284,285]', 51, 1590, '360', NULL, '<p>Апельсиновый маршмеллоу с дольками арбуза, апельсина, лимона. Огромные бананы и персиковые мармеладные ягоды.</p>', '2019-09-16 15:09:29', '2019-10-08 17:46:24', 3),
(37, 'Бананза', '[282,283]', 51, 2190, '580', NULL, '<p>Апельсиновый маршмеллоу с дольками арбуза, апельсина, лимона. Огромные бананы и персиковые мармеладные ягоды.</p>', '2019-09-16 15:10:28', '2019-10-08 17:46:12', 4),
(38, 'Бананза', '[278,279,280,281]', 51, 2690, '1000', '1', '<p>Апельсиновый маршмеллоу с дольками арбуза, апельсина, лимона. Огромные бананы и персиковые мармеладные ягоды.</p>', '2019-09-16 15:10:56', '2019-10-08 17:45:54', 5),
(39, 'Флоренция', '[117,118]', 52, 1690, '360', NULL, '<p>Воздушное суфле сливочно-ванильного вкуса</p>\r\n\r\n<p>с хрустящими трубочками.</p>\r\n\r\n<p>Запах Флоренции.</p>', '2019-09-16 15:12:31', '2019-09-16 15:13:42', 3),
(40, 'Флоренция', '[119,120]', 52, 2190, '580', NULL, '<p>Воздушное суфле сливочно-ванильного вкуса</p>\r\n\r\n<p>с хрустящими трубочками.</p>\r\n\r\n<p>Запах Флоренции.</p>', '2019-09-16 15:13:17', '2019-09-16 15:13:42', 4),
(41, 'Флоренция', '[121,122,123,124,125]', 52, 2690, '1000', '1', '<p>Воздушное суфле сливочно-ванильного вкуса</p>\r\n\r\n<p>с хрустящими трубочками.</p>\r\n\r\n<p>Запах Флоренции.</p>', '2019-09-16 15:13:43', '2019-09-16 15:13:43', 5),
(42, 'Вэлэнтайн', '[345,346,347,348]', 53, 2490, '1000', '1', '<p>Нежное классическое маршмеллоу с ванильным ароматом в форме сердца.</p>', '2019-09-16 15:15:49', '2019-10-09 05:16:31', 5),
(43, 'Радуга', '[292,293]', 54, 1690, '360', '1', '<p>Вы почувствуете неповторимое сочетание</p>\r\n\r\n<p>аромата нежной ванили</p>\r\n\r\n<p>с легкими фруктовыми оттенками.</p>', '2019-09-16 15:17:16', '2019-10-08 17:57:26', 3),
(44, 'Радуга', '[289,290,291]', 54, 2290, '580', NULL, '<p>Вы почувствуете неповторимое сочетание</p>\r\n\r\n<p>аромата нежной ванили</p>\r\n\r\n<p>с легкими фруктовыми оттенками.</p>', '2019-09-16 15:17:48', '2019-10-08 17:57:17', 4),
(45, 'Радуга', '[287,288]', 54, 2790, '1000', NULL, '<p>Вы почувствуете неповторимое сочетание</p>\r\n\r\n<p>аромата нежной ванили</p>\r\n\r\n<p>с легкими фруктовыми оттенками.</p>', '2019-09-16 15:18:11', '2019-10-08 17:57:03', 5),
(46, 'Лаванда', '[341,342,343]', 55, 2990, '1000', '1', '<p>Букет суфле с тонкой ноткой лесных ягод и удивительными ежевичными мармеладками.</p>', '2019-09-16 15:20:52', '2019-10-09 05:09:14', 5),
(47, 'Африка', '[337,338,339]', 56, 2190, '580', '1', '<p>Микс из ванильных трубочек,</p>\r\n\r\n<p>сахарных маргариток,</p>\r\n\r\n<p>персиковых мармеладок</p>\r\n\r\n<p>и мега суфле.</p>', '2019-09-16 15:22:31', '2019-10-09 05:05:02', 4),
(49, 'Индиго', '[327,328]', 58, 1690, '360', '1', '<p>Взрывной букет из маршмеллоу лесные ягоды &nbsp;</p>\r\n\r\n<p>с насыщенным вкусом персиковых</p>\r\n\r\n<p>и бабл гам мармеладок.</p>', '2019-09-16 15:35:02', '2019-10-09 04:57:00', 3),
(50, 'Индиго', '[329]', 58, 2190, '580', NULL, '<p>Взрывной букет из маршмеллоу лесные ягоды &nbsp;</p>\r\n\r\n<p>с насыщенным вкусом персиковых</p>\r\n\r\n<p>и бабл гам мармеладок.</p>', '2019-09-16 15:35:41', '2019-10-09 04:56:41', 4),
(51, 'Индиго', '[330]', 58, 2690, '1000', NULL, '<p>Взрывной букет из маршмеллоу лесные ягоды &nbsp;</p>\r\n\r\n<p>с насыщенным вкусом персиковых</p>\r\n\r\n<p>и бабл гам мармеладок.</p>', '2019-09-16 15:36:08', '2019-10-09 04:57:00', 5),
(52, 'Лето', '[313,314]', 59, 1690, '360', NULL, '<p>Сахарные купола с запахом ванили</p>\r\n\r\n<p>и жевательный мармелад</p>\r\n\r\n<p>со вкусом персика в карамельной обсыпке.</p>', '2019-09-16 15:38:36', '2019-10-09 04:49:49', 3),
(53, 'Лето', '[322,323,324,325]', 59, 2490, '1000', '1', '<p>Сахарные купола с запахом ванили</p>\r\n\r\n<p>и жевательный мармелад</p>\r\n\r\n<p>со вкусом персика в карамельной обсыпке.</p>', '2019-09-16 15:39:37', '2019-10-09 04:50:48', 4),
(54, 'Лето', '[319,320,321]', 59, 2990, '2000', NULL, '<p>Сахарные купола с запахом ванили</p>\r\n\r\n<p>и жевательный мармелад</p>\r\n\r\n<p>со вкусом персика в карамельной обсыпке.</p>', '2019-09-16 15:40:03', '2019-10-09 04:50:20', 5),
(55, 'Фламинго', '[266,267]', 60, 1690, '360', NULL, '<p>Мы предложили вам суфле со вкусом мороженного в виде рожка. Так же малиновые ягоды и сливочное маршмеллоу.</p>', '2019-09-16 15:42:13', '2019-10-08 17:24:17', 3),
(56, 'Фламинго', '[271,272,273]', 60, 2190, '580', '1', '<p>Мы предложили вам суфле со вкусом мороженного в виде рожка. Так же малиновые ягоды и сливочное маршмеллоу.<br />\r\n&nbsp;</p>', '2019-09-16 15:42:43', '2019-10-08 17:24:58', 4),
(66, 'Подсолнухи', '[249,250]', 62, 1871, '360', NULL, '<p>Яркий сливочный суфле</p>\r\n\r\n<p>c дерзкой ноткой</p>\r\n\r\n<p>ежевичных мармеладок.</p>', '2019-09-16 15:55:09', '2019-10-08 17:08:39', 3),
(67, 'Подсолнухи', '[251,252,253]', 62, 2812, '580', '1', '<p>Яркий сливочный суфле</p>\r\n\r\n<p>c дерзкой ноткой</p>\r\n\r\n<p>ежевичных мармеладок.</p>', '2019-09-16 15:55:30', '2019-10-08 17:09:13', 4),
(68, 'Верано', '[204,205]', 63, 1590, '360', '1', '<p>Букет из воздушных сахарных сливочных маргариток с добавлением эксклюзивной ванильной мастики.</p>', '2019-09-16 16:00:38', '2019-09-16 16:00:38', 3),
(69, 'Марсель', '[245,246,247]', 64, 3890, '1500', '1', '<p>Самый мармеладный букет!</p>', '2019-09-16 16:01:58', '2019-10-08 16:53:10', 5),
(70, 'Гармония', '[332,333,334,335]', 57, 2200, '580', '1', '<p>Гигантское маршмеллоу дополняется сливочным суфле-мороженное и ягодами со вкусом йогурта и бабл гам. Очень вкусно!</p>', '2019-09-27 07:05:07', '2019-10-09 05:01:40', 4),
(71, 'Бэлла', '[261,262]', 61, 1690, '360', NULL, '<p>Мега классическое суфле,ежевичные и малиновые ягоды,дополняющиеся ванильной мастикой. Почувствуй любовь на вкус.<br />\r\n&nbsp;</p>', '2019-09-27 07:07:03', '2019-10-08 17:18:14', 3),
(72, 'Бэлла', '[263,264]', 61, 2190, '580', '1', '<p>Мега классическое суфле,ежевичные и малиновые ягоды,дополняющиеся ванильной мастикой. Почувствуй любовь на вкус.<br />\r\n&nbsp;</p>', '2019-09-27 07:07:44', '2019-10-08 17:18:33', 4),
(73, 'Бэлла', '[258,259,260]', 61, 2690, '1000', NULL, '<p>Мега классическое суфле,ежевичные и малиновые ягоды,дополняющиеся ванильной мастикой. Почувствуй любовь на вкус.<br />\r\n&nbsp;</p>', '2019-09-27 07:08:11', '2019-10-08 17:17:57', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `surname`, `adress`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.com', 'Admin', NULL, NULL, NULL, '$2y$10$6C7ejF8FPb2OMR2buBQKbOmk1dL9l3b.pE8N9VALuXoZpjpqnArDe', 'YIEIsSyUbe3fvS0QafF72EhXziqfHoEgaO5WWvIBe0EnhhOEWYLRgM6ArKoU', '2019-06-10 14:37:44', '2019-06-10 14:37:44');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
