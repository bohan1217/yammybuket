-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 22, 2019 at 11:58 AM
-- Server version: 5.6.34
-- PHP Version: 5.3.10-1ubuntu3.26

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yammybuket`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Букеты', '2019-06-10 14:39:26', '2019-06-10 14:39:26'),
(2, 'Открытки', '2019-06-17 14:53:19', '2019-06-17 14:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `discount`, `end_date`, `created_at`, `updated_at`) VALUES
(3, '001-508566-758', 10, NULL, '2019-06-14 10:04:37', '2019-06-14 10:04:37');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `phone`, `created_at`, `updated_at`) VALUES
(6, '+7(345) 435-43-53', '2019-06-18 16:15:58', '2019-06-18 16:15:58');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` int(10) unsigned NOT NULL,
  `to` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_01_02_131905_create_products_table', 1),
(4, '2018_01_02_132136_create_categories_table', 1),
(5, '2018_01_02_132533_create_subcategories_table', 1),
(6, '2018_01_02_213025_create_roles_table', 1),
(7, '2018_01_06_000752_create_photos_table', 1),
(8, '2018_01_09_092104_create_faqs_table', 1),
(9, '2018_01_12_163545_create_orders_table', 1),
(10, '2019_06_01_101336_change_discoutn_type_in_table_product', 1),
(11, '2019_06_01_134942_delete_mig_priority', 1),
(12, '2019_06_08_151732_create_text_table', 1),
(13, '2019_06_10_092018_create_feedback_table', 1),
(14, '2019_06_10_133104_change_order_table', 1),
(15, '2019_06_10_175352_create_shops_table', 2),
(16, '2019_06_12_085801_create_texts_table', 3),
(17, '2019_06_13_163650_create_coupons_table', 4),
(18, '2019_06_14_122203_delete_from_users', 5),
(19, '2019_06_14_130527_add_email_orders', 6),
(20, '2019_06_14_131630_create_messages_table', 7),
(21, '2019_06_17_180351_update_product', 8),
(22, '2019_06_18_141645_create_reviews_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` int(11) DEFAULT NULL,
  `products` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery` int(11) DEFAULT NULL,
  `name2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ascension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=217 ;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `file`, `created_at`, `updated_at`) VALUES
(104, 'icon_1560793165card_bouquet2.png', '2019-06-17 14:39:25', '2019-06-17 14:39:25'),
(105, 'icon_1560793165card_bouquet3.png', '2019-06-17 14:39:25', '2019-06-17 14:39:25'),
(106, 'icon_1560793165card_bouquet4.png', '2019-06-17 14:39:25', '2019-06-17 14:39:25'),
(107, 'icon_1560793165card_bouquet5.png', '2019-06-17 14:39:25', '2019-06-17 14:39:25'),
(109, 'icon_1560793561card_bouquet3.png', '2019-06-17 14:46:01', '2019-06-17 14:46:01'),
(110, 'icon_1560793561card_bouquet4.png', '2019-06-17 14:46:01', '2019-06-17 14:46:01'),
(111, 'icon_1560793561card_bouquet5.png', '2019-06-17 14:46:01', '2019-06-17 14:46:01'),
(112, 'icon_1560793561hands4.png', '2019-06-17 14:46:01', '2019-06-17 14:46:01'),
(114, '15608705281.png', '2019-06-18 12:08:48', '2019-06-18 12:08:48'),
(115, '15608709544.png', '2019-06-18 12:15:54', '2019-06-18 12:15:54'),
(116, '15608709654.png', '2019-06-18 12:16:05', '2019-06-18 12:16:05'),
(117, '15608710493.png', '2019-06-18 12:17:29', '2019-06-18 12:17:29'),
(118, '15608711103.png', '2019-06-18 12:18:30', '2019-06-18 12:18:30'),
(119, '1560871133card_bouquet4.png', '2019-06-18 12:18:53', '2019-06-18 12:18:53'),
(120, '1560872074Радуга.png', '2019-06-18 12:34:34', '2019-06-18 12:34:34'),
(121, '1560872415Фламинго.png', '2019-06-18 12:40:15', '2019-06-18 12:40:15'),
(122, '1560883007photo_2019-06-18_20-58-19.jpg', '2019-06-18 15:36:48', '2019-06-18 15:36:48'),
(123, '1560883017photo_2019-06-18_20-58-20.jpg', '2019-06-18 15:36:57', '2019-06-18 15:36:57'),
(124, '1560883021photo_2019-06-18_20-58-21.jpg', '2019-06-18 15:37:01', '2019-06-18 15:37:01'),
(125, '1560883025photo_2019-06-18_20-58-22 (2).jpg', '2019-06-18 15:37:05', '2019-06-18 15:37:05'),
(126, '1560883029photo_2019-06-18_20-58-22.jpg', '2019-06-18 15:37:09', '2019-06-18 15:37:09'),
(127, '1560883085IMG_2595.MOV', '2019-06-18 15:38:05', '2019-06-18 15:38:05'),
(133, 'icon_1560887672Бэлла f2a0b4.png', '2019-06-18 16:54:32', '2019-06-18 16:54:32'),
(134, 'icon_1560887867Бэлла f2a0b4.png', '2019-06-18 16:57:47', '2019-06-18 16:57:47'),
(135, 'icon_1560888063Бэлла f2a0b4.png', '2019-06-18 17:01:03', '2019-06-18 17:01:03'),
(137, 'icon_1560889042f2a0b4 бэлла.jpg', '2019-06-18 17:17:23', '2019-06-18 17:17:23'),
(138, '1560890703a8d1f1 Азуро — копия.jpg', '2019-06-18 17:33:43', '2019-06-18 17:45:03'),
(139, 'icon_1560890076a8d1f1 Азуро — копия.jpg', '2019-06-18 17:34:36', '2019-06-18 17:34:36'),
(140, 'icon_1560890703a8d1f1 Азуро — копия.jpg', '2019-06-18 17:45:03', '2019-06-18 17:45:03'),
(141, '1560892022card_bouquet4.png', '2019-06-18 18:07:03', '2019-06-18 18:07:03'),
(142, '1560939690f2a0b4 бэлла.jpg', '2019-06-19 07:21:30', '2019-06-19 07:21:30'),
(143, 'icon_1560939690f2a0b4 бэлла.jpg', '2019-06-19 07:21:30', '2019-06-19 07:21:30'),
(144, 'icon_1560939906IMG_1420.HEIC', '2019-06-19 07:25:06', '2019-06-19 07:25:06'),
(145, 'icon_1560940752Белла М.HEIC', '2019-06-19 07:39:13', '2019-06-19 07:39:13'),
(146, 'icon_1560940752Белла L.HEIC', '2019-06-19 07:39:13', '2019-06-19 07:39:13'),
(147, '1560945818f2f3b3 Подсолнухи.png', '2019-06-19 09:03:38', '2019-06-19 09:03:38'),
(148, '1560946051d8e6d4 верано.jpg', '2019-06-19 09:05:28', '2019-06-19 09:07:31'),
(149, '1560946332амур f8daf8.jpg', '2019-06-19 09:09:20', '2019-06-19 09:12:12'),
(150, '1560946410eeef8f лето.png', '2019-06-19 09:13:30', '2019-06-19 09:13:30'),
(151, '1561059566фламинго f899bd.png', '2019-06-19 09:15:15', '2019-06-20 16:39:26'),
(152, '1561034820d4a5f2 индиг33333о.jpg', '2019-06-19 09:16:28', '2019-06-20 09:47:00'),
(153, '1561056749e9c76b.jpg', '2019-06-19 09:19:22', '2019-06-20 15:52:29'),
(154, '1560946821c3d5f6 гармония.jpg', '2019-06-19 09:20:21', '2019-06-19 09:20:21'),
(155, '1560946927лаванда e5d2f2.jpg', '2019-06-19 09:22:07', '2019-06-19 09:22:07'),
(156, '1561045799b3f4ac.png', '2019-06-19 09:23:38', '2019-06-20 12:49:59'),
(157, '1560947114f9f7bc.png', '2019-06-19 09:25:14', '2019-06-19 09:25:14'),
(158, '1560947273e09eb4 сердце.jpg', '2019-06-19 09:26:40', '2019-06-19 09:27:53'),
(159, '1561058405Фрэнки 30dbf4.png', '2019-06-19 09:29:19', '2019-06-20 16:20:05'),
(160, '1560947426d7a167 бананза.png', '2019-06-19 09:30:26', '2019-06-19 09:30:26'),
(161, '1561055245photo_2019-06-18_20-58-19.jpg', '2019-06-20 15:27:25', '2019-06-20 15:27:25'),
(162, '1561055860photo_2019-06-18_20-58-19.jpg', '2019-06-20 15:37:40', '2019-06-20 15:37:40'),
(163, '1561056184photo_2019-06-18_20-58-22.jpg', '2019-06-20 15:43:04', '2019-06-20 15:43:04'),
(164, 'icon_1561056948IMG_1331.jpeg', '2019-06-20 15:55:48', '2019-06-20 15:55:48'),
(165, 'icon_1561057033DH9B9121.jpg', '2019-06-20 15:57:13', '2019-06-20 15:57:13'),
(166, 'icon_1561057140DH22229B9121.jpg', '2019-06-20 15:59:00', '2019-06-20 15:59:00'),
(167, 'icon_1561057185DH22229B9121.jpg', '2019-06-20 15:59:45', '2019-06-20 15:59:45'),
(168, 'icon_1561057406IMG_1331.jpeg', '2019-06-20 16:03:26', '2019-06-20 16:03:26'),
(169, 'icon_1561057473DH9B9056.jpg', '2019-06-20 16:04:33', '2019-06-20 16:04:33'),
(170, 'icon_1561057543DH9B22229056.jpg', '2019-06-20 16:05:43', '2019-06-20 16:05:43'),
(171, 'icon_1561057643IMG_0959.jpeg', '2019-06-20 16:07:23', '2019-06-20 16:07:23'),
(172, 'icon_1561057643IMG_0973.jpeg', '2019-06-20 16:07:23', '2019-06-20 16:07:23'),
(173, 'icon_1561057643IMG_0973.jpeg', '2019-06-20 16:07:23', '2019-06-20 16:07:23'),
(174, 'icon_1561057670IMG_1181.jpeg', '2019-06-20 16:07:50', '2019-06-20 16:07:50'),
(175, 'icon_1561058144DH9B9195 — копия.jpg', '2019-06-20 16:15:44', '2019-06-20 16:15:44'),
(176, 'icon_1561058253IMG_1465.jpeg', '2019-06-20 16:17:33', '2019-06-20 16:17:33'),
(177, 'icon_1561058253IMG_1526.jpeg', '2019-06-20 16:17:33', '2019-06-20 16:17:33'),
(178, 'icon_1561058253IMG_1535.jpeg', '2019-06-20 16:17:33', '2019-06-20 16:17:33'),
(179, 'icon_1561058435IMG_1465.jpeg', '2019-06-20 16:20:35', '2019-06-20 16:20:35'),
(180, 'icon_1561058463IMG_1526.jpeg', '2019-06-20 16:21:03', '2019-06-20 16:21:03'),
(181, 'icon_1561058524IMG_1526.jpeg', '2019-06-20 16:22:04', '2019-06-20 16:22:04'),
(182, 'icon_1561058546IMG_1526 — копия.jpeg', '2019-06-20 16:22:26', '2019-06-20 16:22:26'),
(183, 'icon_1561058568IMG_1465.jpeg', '2019-06-20 16:22:48', '2019-06-20 16:22:48'),
(184, 'icon_1561058568IMG_1535.jpeg', '2019-06-20 16:22:48', '2019-06-20 16:22:48'),
(185, 'icon_1561058642IMG_1276.jpeg', '2019-06-20 16:24:02', '2019-06-20 16:24:02'),
(186, 'icon_1561058716DH9B9064 — копия.jpg', '2019-06-20 16:25:16', '2019-06-20 16:25:16'),
(187, 'icon_1561058778IMG_9859.JPG', '2019-06-20 16:26:18', '2019-06-20 16:26:18'),
(188, 'icon_1561058778IMG_9879.JPG', '2019-06-20 16:26:18', '2019-06-20 16:26:18'),
(189, 'icon_1561058965DH9B9130 — копия.jpg', '2019-06-20 16:29:25', '2019-06-20 16:29:25'),
(190, 'icon_1561059012IMG_1219.jpeg', '2019-06-20 16:30:12', '2019-06-20 16:30:12'),
(191, 'icon_1561059026IMG_1238.jpeg', '2019-06-20 16:30:26', '2019-06-20 16:30:26'),
(192, 'icon_1561059049IMG_1235.jpeg', '2019-06-20 16:30:49', '2019-06-20 16:30:49'),
(193, 'icon_1561059152IMG_1547.jpeg', '2019-06-20 16:32:32', '2019-06-20 16:32:32'),
(194, 'icon_1561059152IMG_1568.jpeg', '2019-06-20 16:32:32', '2019-06-20 16:32:32'),
(195, 'icon_1561059152IMG_1587.jpeg', '2019-06-20 16:32:32', '2019-06-20 16:32:32'),
(196, 'icon_1561059221IMG_1415.jpeg', '2019-06-20 16:33:41', '2019-06-20 16:33:41'),
(197, 'icon_1561059221IMG_1459.jpeg', '2019-06-20 16:33:41', '2019-06-20 16:33:41'),
(198, 'icon_1561059320IMG_0956 (1).jpeg', '2019-06-20 16:35:20', '2019-06-20 16:35:20'),
(199, 'icon_1561059320IMG_1006.jpeg', '2019-06-20 16:35:20', '2019-06-20 16:35:20'),
(200, 'icon_1561059320IMG_1159.jpeg', '2019-06-20 16:35:20', '2019-06-20 16:35:20'),
(201, 'icon_1561059377IMG_0956 (1).jpeg', '2019-06-20 16:36:17', '2019-06-20 16:36:17'),
(202, 'icon_1561059434IMG_1290.jpeg', '2019-06-20 16:37:14', '2019-06-20 16:37:14'),
(203, 'icon_1561059457IMG_1300.jpeg', '2019-06-20 16:37:37', '2019-06-20 16:37:37'),
(204, 'icon_1561059614DH9B9155.jpg', '2019-06-20 16:40:14', '2019-06-20 16:40:14'),
(205, 'icon_1561059614IMG_9859.JPG', '2019-06-20 16:40:14', '2019-06-20 16:40:14'),
(206, 'icon_1561059614IMG_9879.JPG', '2019-06-20 16:40:14', '2019-06-20 16:40:14'),
(207, 'icon_1561059662DH9B9155 — копия.jpg', '2019-06-20 16:41:02', '2019-06-20 16:41:02'),
(208, 'icon_1561061452IMG_1459.jpeg', '2019-06-20 17:10:52', '2019-06-20 17:10:52'),
(209, 'icon_1561061543IMG_1526 — копия.jpeg', '2019-06-20 17:12:23', '2019-06-20 17:12:23'),
(210, 'icon_1561061621IMG_1526 — копия.jpeg', '2019-06-20 17:13:41', '2019-06-20 17:13:41'),
(211, 'icon_1561062510IMG_1459.jpeg', '2019-06-20 17:28:30', '2019-06-20 17:28:30'),
(212, 'icon_1561062633IMG_1547.jpeg', '2019-06-20 17:30:33', '2019-06-20 17:30:33'),
(213, 'icon_1561062854photo_2019-06-20_23-33-24.jpg', '2019-06-20 17:34:14', '2019-06-20 17:34:14'),
(214, 'icon_1561064567IM222G_9859.jpg', '2019-06-20 18:02:47', '2019-06-20 18:02:47'),
(215, 'icon_1561064567IMG33333333_9879.jpg', '2019-06-20 18:02:47', '2019-06-20 18:02:47'),
(216, 'icon_1561064681IM222G_9859.jpg', '2019-06-20 18:04:41', '2019-06-20 18:04:41');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_id` smallint(5) unsigned DEFAULT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `icon_id1` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_id2` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_id3` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mass1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mass2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mass3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `price1` int(11) DEFAULT NULL,
  `price2` int(11) DEFAULT NULL,
  `price3` int(11) DEFAULT NULL,
  `description1` text COLLATE utf8mb4_unicode_ci,
  `description2` text COLLATE utf8mb4_unicode_ci,
  `description3` text COLLATE utf8mb4_unicode_ci,
  `released_on` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=42 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `photo_id`, `category_id`, `icon_id1`, `icon_id2`, `icon_id3`, `mass1`, `mass2`, `mass3`, `discount`, `price1`, `price2`, `price3`, `description1`, `description2`, `description3`, `released_on`, `created_at`, `updated_at`, `priority`, `price`, `color`) VALUES
(26, 'Букет Азурро', 138, 1, NULL, '[168]', NULL, NULL, '580', NULL, 15, NULL, 2500, NULL, NULL, '<p>Легкое разноцветное суфле и нежные персиковые мармеладки.</p>', NULL, NULL, '2019-06-18 17:33:43', '2019-06-21 11:11:11', 2, NULL, '#a8d1f1'),
(27, 'Букет Белла', 142, 1, '[144]', '[145]', '[146]', '360', '580', '1000', NULL, 1700, 2500, 3500, '<p>Мега классическое суфле,ежевичные и малиновые ягоды,дополняющиеся ванильной мастикой. Почувствуй любовь на вкус.</p>', '<p>Мега классическое суфле,ежевичные и малиновые ягоды,дополняющиеся ванильной мастикой. Почувствуй любовь на вкус..</p>', '<p>Мега классическое суфле,ежевичные и малиновые ягоды,дополняющиеся ванильной мастикой. Почувствуй любовь на вкус.</p>', NULL, '2019-06-19 07:21:30', '2019-06-21 10:56:32', 1, NULL, '#f2a0b4'),
(28, 'Букет  Подсолнухи', 147, 1, '[202]', '[203]', NULL, '360', '580', NULL, NULL, 1700, 2500, NULL, '<p>Яркий сливочный суфле c дерзкой ноткой</p>\r\n\r\n<p>ежевичных мармеладок</p>', '<p>Яркий сливочный суфле c дерзкой ноткой</p>\r\n\r\n<p>ежевичных мармеладок</p>', NULL, NULL, '2019-06-19 09:03:38', '2019-06-21 11:11:36', 2, NULL, '#f2f3b3'),
(29, 'Букет Верано', 148, 1, '[175]', NULL, NULL, '360', NULL, NULL, NULL, 1700, NULL, NULL, '<p>Букет из воздушных сахарныхсливочных маргариток с добавлением эксклюзивной ванильной мастики</p>', NULL, NULL, NULL, '2019-06-19 09:05:28', '2019-06-21 10:57:14', 1, NULL, '#d8e6d4'),
(30, 'Букет Амур', 149, 1, NULL, NULL, '[167]', NULL, NULL, '1000', NULL, NULL, NULL, 3500, '<p>Мармеладки-ягодки со вкусом йогрута и малины</p>\r\n\r\n<p>в элегантном сочетании с классическим сахарным суфле.</p>\r\n\r\n<p>Ванильная мастика дополняется хрустящими вафельками.</p>', NULL, '<p>Мармеладки-ягодки со вкусом йогрута и малины</p>\r\n\r\n<p>в элегантном сочетании с классическим сахарным суфле.</p>\r\n\r\n<p>Ванильная мастика дополняется хрустящими вафельками.</p>', NULL, '2019-06-19 09:09:20', '2019-06-21 11:05:41', 3, NULL, '#f8daf8'),
(31, 'Букет Лето', 150, 1, '[191]', '[190]', '[192]', '360', '580', '2000', NULL, 1700, 2500, 3900, '<p>Легкие сахарные купола с запахом ванили</p>\r\n\r\n<p>и жевательный мармелад</p>\r\n\r\n<p>со вкусом персика в карамельной обсыпке.</p>\r\n\r\n<p>Окунись в лето!</p>', '<p>Легкие сахарные купола с запахом ванили</p>\r\n\r\n<p>и жевательный мармелад</p>\r\n\r\n<p>со вкусом персика в карамельной обсыпке.</p>\r\n\r\n<p>Окунись в лето!</p>', '<p>Легкие сахарные купола с запахом ванили</p>\r\n\r\n<p>и жевательный мармелад</p>\r\n\r\n<p>со вкусом персика в карамельной обсыпке.</p>\r\n\r\n<p>Окунись в лето!</p>', NULL, '2019-06-19 09:13:30', '2019-06-21 11:00:56', 2, NULL, '#eeef8f'),
(32, 'Букет Фламинго', 151, 1, '[196]', '[211]', NULL, '360', '580', NULL, NULL, 1700, 2500, NULL, '<p>Мы решили вас удивить и предложили суфле со вкусом мороженного в виде рожка. Так же малиновые ягоды и сливочное&nbsp;маршмеллоу для сладкоежек</p>', '<p>Мы решили вас удивить и предложили суфле со вкусом мороженного в виде рожка. Так же малиновые ягоды и сливочн</p>', NULL, NULL, '2019-06-19 09:15:15', '2019-06-21 11:02:14', 2, NULL, '#f899bd'),
(33, 'Букет Индиго', 152, 1, '[207]', '[216]', '[215]', '360', '580', '1000', NULL, 1700, 2500, 3500, '<p>Взрывной букет из маршмеллоу лесные ягоды &nbsp;</p>\r\n\r\n<p>в сочетании с насыщенным вкусом персиковых</p>\r\n\r\n<p>и бабл гам мармеладок</p>', '<p>Взрывной букет из маршмеллоу лесные ягоды &nbsp;</p>\r\n\r\n<p>в сочетании с насыщенным вкусом персиковых</p>\r\n\r\n<p>и бабл гам мармеладок</p>', '<p>Взрывной букет из маршмеллоу лесные ягоды &nbsp;</p>\r\n\r\n<p>в сочетании с насыщенным вкусом персиковых</p>\r\n\r\n<p>и бабл гам мармеладок</p>', NULL, '2019-06-19 09:16:28', '2019-06-21 10:59:11', 1, NULL, '#e9a8f9'),
(34, 'Букет Африка', 153, 1, NULL, '[170]', NULL, NULL, '580', NULL, NULL, NULL, 2500, NULL, NULL, '<p>Африканский микс из ванильных трубочек,</p>\r\n\r\n<p>сахарных маргариток,</p>\r\n\r\n<p>персиковых мармеладок</p>\r\n\r\n<p>и мега суфле.</p>', NULL, NULL, '2019-06-19 09:19:22', '2019-06-21 10:55:12', 2, NULL, '#f4c963'),
(35, 'Букет Гармония', 154, 1, NULL, '[186]', NULL, NULL, '580', NULL, NULL, NULL, 2500, NULL, '<p>Гигантское маршмеллоу дополняется сливочным суфле-мороженное и ягодами со вкусом йогурта и бабл гам. Оооочень вкусно!</p>', '<p>Гигантское маршмеллоу дополняется сливочным суфле-мороженное и ягодами со вкусом йогурта и бабл гам. Оооочень вкусно!</p>', NULL, NULL, '2019-06-19 09:20:21', '2019-06-21 11:04:42', 2, NULL, '#c3d5f6'),
(36, 'Букет Лаванда', 155, 1, NULL, NULL, '[189]', NULL, NULL, '1000', NULL, NULL, NULL, 3900, '<p>Букет суфле с тонкой ноткой лесных ягод</p>\r\n\r\n<p>и удивительными ежевичными мармеладками</p>', NULL, '<p>Букет суфле с тонкой ноткой лесных ягод</p>\r\n\r\n<p>и удивительными ежевичными мармеладками</p>', NULL, '2019-06-19 09:22:08', '2019-06-21 11:04:08', 3, NULL, '#ecd3f4'),
(37, 'Букет Радуга', 156, 1, '[213]', '[194]', '[195]', '360', '580', '1000', 5, 1700, 2500, 3500, '<p>Вы почувствуете неповторимое сочетание</p>\r\n\r\n<p>аромата нежной ванили</p>\r\n\r\n<p>с легкими фруктовыми оттенками</p>', '<p>Вы почувствуете неповторимое сочетание</p>\r\n\r\n<p>аромата нежной ванили</p>\r\n\r\n<p>с легкими фруктовыми оттенками</p>', '<p>Вы почувствуете неповторимое сочетание</p>\r\n\r\n<p>аромата нежной ванили</p>\r\n\r\n<p>с легкими фруктовыми оттенками</p>', NULL, '2019-06-19 09:23:38', '2019-06-21 11:10:16', 2, NULL, '#b3f4ac'),
(38, 'Букет Флоренция', 157, 1, '[201]', '[199]', '[200]', '360', '580', '1000', NULL, 1700, 2500, 3500, '<p>Воздушное суфле сливочно-ванильного вкуса</p>\r\n\r\n<p>с хрустящими трубочками.</p>\r\n\r\n<p>Запах Флоренции.</p>', '<p>Воздушное суфле сливочно-ванильного вкуса</p>\r\n\r\n<p>с хрустящими трубочками.</p>\r\n\r\n<p>Запах Флоренции.</p>', '<p>Воздушное суфле сливочно-ванильного вкуса</p>\r\n\r\n<p>с хрустящими трубочками.</p>\r\n\r\n<p>Запах Флоренции.</p>', NULL, '2019-06-19 09:25:14', '2019-06-21 11:02:41', 3, NULL, '#f9f7bc'),
(39, 'Букет Вэлинтайн', 158, 1, NULL, '[185]', NULL, NULL, '580', NULL, NULL, NULL, 2500, NULL, '<p>Нежное классическое маршмеллоу с ванильным ароматом в форме сердца.</p>', '<p>Нежное классическое маршмеллоу с ванильным ароматом в форме сердца.</p>', NULL, NULL, '2019-06-19 09:26:41', '2019-06-21 11:09:02', 2, NULL, '#e09eb4'),
(40, 'Букет Виолетти', 159, 1, '[183]', '[210]', '[184]', '360', '580', '1000', 10, 1700, 2500, 3500, '<p>Хрустящие вафельные трубочки с ванильной ноткой</p>\r\n\r\n<p>в классическом сливочном маршмеллоу</p>\r\n\r\n<p>и удивительные ягодки бабл гам.</p>', '<p>Хрустящие вафельные трубочки с ванильной ноткой</p>\r\n\r\n<p>в классическом сливочном маршмеллоу</p>\r\n\r\n<p>и удивительные ягодки бабл гам.</p>', '<p>Хрустящие вафельные трубочки с ванильной ноткой</p>\r\n\r\n<p>в классическом сливочном маршмеллоу</p>\r\n\r\n<p>и удивительные ягодки бабл гам.</p>', NULL, '2019-06-19 09:29:19', '2019-06-21 11:09:47', 2, NULL, '#30dbf4'),
(41, 'Букет Бананза', 160, 1, '[171]', '[172]', '[174]', '360', '580', '3900', NULL, 1700, 2500, 3500, '<p>Апельсиновый маршмеллоу с дольками арбуза, апельсина, лимона. Огромные бананы и персиковые мармеладные ягоды.</p>', '<p>Апельсиновый маршмеллоу с дольками арбуза, апельсина, лимона. Огромные бананы и персиковые мармеладные ягоды.</p>', '<p>Апельсиновый маршмеллоу с дольками арбуза, апельсина, лимона. Огромные бананы и персиковые мармеладные ягоды.</p>', NULL, '2019-06-19 09:30:26', '2019-06-21 10:56:49', 3, NULL, '#d7a167');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image` smallint(5) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=37 ;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `video`, `image`, `created_at`, `updated_at`) VALUES
(33, 'https://youtube.com/embed/hnxCtRbss9g', NULL, '2019-06-19 07:14:00', '2019-06-19 07:14:00'),
(35, NULL, 162, '2019-06-20 15:37:40', '2019-06-20 15:37:40'),
(36, NULL, 163, '2019-06-20 15:43:04', '2019-06-20 15:43:04');

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `name`, `address`, `lat`, `lng`, `created_at`, `updated_at`) VALUES
(1, 'Самовывоз', 'Строгинский бульвар 4', '55.804616', '37.396617', NULL, '2019-06-18 14:50:07'),
(2, 'Самовывоз', 'улица Коккинаки д 4', '55.804304', '37.546138', '2019-06-18 14:51:52', '2019-06-18 14:51:52'),
(3, 'Самовывоз', 'ТЦ Лига, Ленинградское шоссе вл 5', '55.888997', '37.433046', '2019-06-18 14:53:04', '2019-06-18 14:53:27'),
(4, 'Самовывоз', 'Лихачёвский просп. д 64', '55.931235', '37.494153', '2019-06-18 14:54:21', '2019-06-18 14:54:21');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subcategories_category_id_index` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `texts`
--

CREATE TABLE IF NOT EXISTS `texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slider` text COLLATE utf8mb4_unicode_ci,
  `title2` text COLLATE utf8mb4_unicode_ci,
  `text2` text COLLATE utf8mb4_unicode_ci,
  `title3` text COLLATE utf8mb4_unicode_ci,
  `text3` text COLLATE utf8mb4_unicode_ci,
  `title4` text COLLATE utf8mb4_unicode_ci,
  `text4` text COLLATE utf8mb4_unicode_ci,
  `title5` text COLLATE utf8mb4_unicode_ci,
  `text5` text COLLATE utf8mb4_unicode_ci,
  `title6` text COLLATE utf8mb4_unicode_ci,
  `text6` text COLLATE utf8mb4_unicode_ci,
  `title7` text COLLATE utf8mb4_unicode_ci,
  `text7` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `texts`
--

INSERT INTO `texts` (`id`, `slider`, `title2`, `text2`, `title3`, `text3`, `title4`, `text4`, `title5`, `text5`, `title6`, `text6`, `title7`, `text7`, `created_at`, `updated_at`) VALUES
(1, '<h1>Сладкие воздушные<br />\r\nбукеты из зефира<br />\r\nи мармеладок</h1>', 'Используем самые качественные ингридиенты', '<p class="subtitle"><span style="font-size:16px;"><span style="line-height:115%"><span new="" roman="" times="">Наши букеты из маршмеллоу состоят из высококачественной продукции крупнейших европейских производителей, таких как Fini Sweets (Италия), Bulgari (Испания), Damel (Испания),&nbsp; Golmasa (Испания). Ведущие компании имеют огромный опыт и большую историю и продолжают традиции в кондитерском деле, имеют самые высокие международные оценки качества и безопасности пищевых продуктов, сертификаты IFS, BRC и ISO 9001: 2008. </span></span></span></p>\r\n\r\n<p class="subtitle"><span style="font-size:16px;"><span style="line-height:115%"><span new="" roman="" times="">Зефирные букеты всегда самого высшего качества и свежести, соблюдаются все условия хранения . Наше суфле остаётся мягким и свежим на протяжении долгого времени после изготовления букета. Мы собираем сладкие букеты согласно фитосанитарным нормам в перчатках и упаковываем в пищевую слюду за 2 часа до доставки, что позволяет гарантировать качество нашей продукции.</span></span></span></p>', 'Соблюдаем все санитарные нормы', '<h4 class="subtitle">Букеты собираются в перчатках и упаковываются в плёнку</h4>', 'Доставка и оплата  <br> по Москве и области', '<p class="text">Оплата курьеру <strong>наличными</strong> или <strong>переводом</strong> на карту. Доставка осуществляется на следующий день после заказа. Возможна доставка день в день по согласованию. Бесплатная доставка по Москве в пределах МКАД.</p>', 'Выбери свой букет', '<p>Наши букеты настолько вкусно пахнут, что перед доставкой букета мы кормим курьера зефиром, т.к. есть шанс, что он его съест</p>', 'Полезные сладости – это не миф', '<p><span style="font-size:14px;"><span style="background:white"><span style="line-height:115%"><span new="" roman="" times=""><span style="color:black">Основа зефира &ndash; фруктово-ягодное пюре. Его готовят из яблок, груш, клюквы, черной смородины и других плодов. Все они богаты витаминами и минералами, органическими кислотами и пектином.&nbsp;</span></span></span></span><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="background:white"><span style="line-height:150%"><span new="" roman="" times=""><span style="color:black">Воздушный зефир не содержит ни грамма жира, он легко переваривается, поэтому такое лакомство &laquo;не нагружает&raquo; наш желудок, как пирожные и торты. А за счет содержания пектина или другого желирующего агента зефир даже улучшает пищеварение.</span></span></span></span></span></span></span></p>', 'Вы можете приобрести букет в одном из магазинов наших партнёров', '<p>Для того чтобы стать партнёром оставьте свой номер, мы перезвоним и расскажем про условия</p>', NULL, '2019-06-19 11:18:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `surname`, `adress`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.com', 'Admin', NULL, NULL, NULL, '$2y$10$6C7ejF8FPb2OMR2buBQKbOmk1dL9l3b.pE8N9VALuXoZpjpqnArDe', 'YIEIsSyUbe3fvS0QafF72EhXziqfHoEgaO5WWvIBe0EnhhOEWYLRgM6ArKoU', '2019-06-10 14:37:44', '2019-06-10 14:37:44');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
