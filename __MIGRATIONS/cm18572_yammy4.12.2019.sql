-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 04, 2019 at 01:45 PM
-- Server version: 5.6.39-83.1
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cm18572_yammy`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Букеты', '2019-06-10 14:39:26', '2019-06-10 14:39:26'),
(2, 'Открытки', '2019-06-17 14:53:19', '2019-06-17 14:53:19'),
(3, 'Топпер', '2019-10-11 09:52:28', '2019-10-11 09:52:28');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `discount`, `end_date`, `created_at`, `updated_at`) VALUES
(3, '001-508566-758', 10, NULL, '2019-06-14 10:04:37', '2019-06-14 10:04:37'),
(4, '222', 10, NULL, '2019-09-09 05:19:40', '2019-09-09 05:19:40');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `phone`, `created_at`, `updated_at`) VALUES
(10, '+7(985) 425-49-13', '2019-10-22 09:29:25', '2019-10-22 09:29:25'),
(11, '+7(925) 343-32-16', '2019-10-23 04:21:09', '2019-10-23 04:21:09'),
(12, '+7(985) 233-20-79', '2019-10-27 10:23:02', '2019-10-27 10:23:02'),
(13, '+7(915) 419-44-34', '2019-11-08 16:51:06', '2019-11-08 16:51:06'),
(14, '+7(111) 111-11-11', '2019-11-08 16:51:38', '2019-11-08 16:51:38');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `from` int(10) UNSIGNED NOT NULL,
  `to` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_01_02_131905_create_products_table', 1),
(4, '2018_01_02_132136_create_categories_table', 1),
(5, '2018_01_02_132533_create_subcategories_table', 1),
(6, '2018_01_02_213025_create_roles_table', 1),
(7, '2018_01_06_000752_create_photos_table', 1),
(8, '2018_01_09_092104_create_faqs_table', 1),
(9, '2018_01_12_163545_create_orders_table', 1),
(10, '2019_06_01_101336_change_discoutn_type_in_table_product', 1),
(11, '2019_06_01_134942_delete_mig_priority', 1),
(12, '2019_06_08_151732_create_text_table', 1),
(13, '2019_06_10_092018_create_feedback_table', 1),
(14, '2019_06_10_133104_change_order_table', 1),
(15, '2019_06_10_175352_create_shops_table', 2),
(16, '2019_06_12_085801_create_texts_table', 3),
(17, '2019_06_13_163650_create_coupons_table', 4),
(18, '2019_06_14_122203_delete_from_users', 5),
(19, '2019_06_14_130527_add_email_orders', 6),
(20, '2019_06_14_131630_create_messages_table', 7),
(21, '2019_06_17_180351_update_product', 8),
(22, '2019_06_18_141645_create_reviews_table', 9),
(23, '2019_06_22_085215_update_table_text', 10),
(24, '2019_09_04_132037_delete_from_catalog_and_add', 11),
(25, '2019_09_04_140418_create_size_table', 12),
(26, '2019_09_04_143616_create_tproducts_table', 13),
(27, '2019_09_04_145414_add_t_size', 14);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `orderId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` int(11) DEFAULT NULL,
  `products` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery` int(11) DEFAULT NULL,
  `name2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ascension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `orderId`, `status`, `totalAmount`, `products`, `adress`, `created_at`, `updated_at`, `name`, `phone`, `delivery`, `name2`, `phone2`, `street`, `house`, `flat`, `ascension`, `message`, `email`) VALUES
(35, '5d91c0b6ae86c', 'PENDING', 229000, '34-1-2', '___', '2019-09-30 05:45:42', '2019-09-30 05:45:42', 'наталья', '+7(903) 671-49-06', 0, NULL, NULL, 'нижегородская', '67/2', NULL, NULL, 'детский сад, доставка на 3 октября', 'dou350@yandex.ru'),
(36, '5d94848e9afa9', 'PENDING', 219000, '47-1-1', '___', '2019-10-02 08:05:50', '2019-10-02 08:05:50', 'Константин', '+7(917) 548-75-05', 0, NULL, NULL, 'г. Москва', '2', '153', '6', 'Домофон.153к3120', NULL),
(40, '5d970e0503814', 'PENDING', 169000, '9491-1-1', '___', '2019-10-04 06:16:53', '2019-10-04 06:16:53', 'Елена', '+7(927) 773-27-35', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, '5d9a099434350', 'PENDING', 279000, '9451-1-3', '___', '2019-10-06 12:34:44', '2019-10-06 12:34:44', 'Лидия', '+7(910) 486-35-82', 0, NULL, NULL, 'Лужнецкая набережная', '8', NULL, NULL, 'Олимпийский коммитет России', 'lida139@yandex.ru'),
(42, '5d9b564d7590c', 'PENDING', 249000, '9321-1-1', '___', '2019-10-07 12:14:21', '2019-10-07 12:14:21', 'Михаил', '+7(926) 211-11-72', 0, NULL, NULL, 'Окружной проезд', '22/64', '95', '3', 'Код домофона 70В7050.', NULL),
(43, '5d9f3b9b78911', 'PENDING', 220000, '9701-1-1', '___', '2019-10-10 11:09:31', '2019-10-10 11:09:31', 'Юлия', '+7(903) 298-08-97', 0, NULL, NULL, 'Севастопольский пр-т', '71', '25', '2', NULL, '2980897@gmail.com'),
(44, '5da627fd0c610', 'PENDING', 259000, '9321-1-1', '___', '2019-10-15 17:11:41', '2019-10-15 17:11:41', 'From iphone Test', '+7(111) 111-11-11', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Test@test.com'),
(45, '5da83109129b8', 'PENDING', 178000, '9681-1-1|9772-1-1', '___', '2019-10-17 06:14:49', '2019-10-17 06:14:49', 'Диана', '+7(926) 228-03-54', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dkusova@mail.ru'),
(46, '5dad6bba1dc48', 'PENDING', 209000, '9321-1-1|9772-1-1', '___', '2019-10-21 05:26:34', '2019-10-21 05:26:34', 'Анастасия', '+7(915) 322-80-67', 0, NULL, NULL, '7- парковая', '27', '41', '3', 'Домофон в41в1782', NULL),
(47, '5daf12f670b00', 'PENDING', 169000, '9401-1-1|9772-1-1', '___', '2019-10-22 11:32:22', '2019-10-22 11:32:22', 'Кристина', '+7(903) 828-20-46', 0, NULL, NULL, 'Москвитина', '1к2', '82', '1', NULL, 'Kristiben@bk.ru'),
(48, '5db2b41481199', 'PENDING', 188000, '9551-1-1|9772-1-1', '___', '2019-10-25 05:36:36', '2019-10-25 05:36:36', 'Людмила', '+7(926) 226-31-71', 0, NULL, NULL, 'г.Видное, ул.Ольховая,', '9', '261', '3, этаж 2, домофон 261', 'Доставка завтра, 26.10.2019 до 12ч', NULL),
(49, '5db31b1bb69fa', 'PENDING', 190000, '9321-1-1', '___', '2019-10-25 12:56:11', '2019-10-25 12:56:11', 'Анна', '+7(905) 503-77-27', 0, NULL, NULL, 'Батайский проезд', '11', '106', '3', 'Код домофона В106В3619В   , 9 этаж', NULL),
(50, '5dc573463b7a7', 'PENDING', 389000, '9691-1-1', '___', '2019-11-08 10:53:10', '2019-11-08 10:53:10', 'Вадим', '+7(903) 761-10-66', 0, NULL, NULL, 'Удальцова', '87 к 4', '30', NULL, NULL, NULL),
(53, '5dccb4f75be38', 'PENDING', 299000, '9772-1-1|9801-1-3', '___', '2019-11-13 22:59:19', '2019-11-13 22:59:19', 'Роман', '+7(905) 041-80-59', 0, NULL, NULL, 'Плеханова', '35', NULL, NULL, 'Предворительно позвонить', NULL),
(54, '5dd2e7cf788a7', 'PENDING', 249000, '9531-1-2', '___', '2019-11-18 15:49:51', '2019-11-18 15:49:51', 'Юлия', '+7(899) 985-99-40', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'faza-demon@yandex.ru'),
(55, '5dd64000dbe62', 'PENDING', 260000, '9321-1-1', '___', '2019-11-21 04:42:56', '2019-11-21 04:42:56', 'Анна', '+7(926) 273-15-68', 0, NULL, NULL, 'строгинский бульвар', '4', '88', '2', 'эт 6', NULL),
(56, '5dd7c0e1dc012', 'PENDING', 209000, '9841-1-1|9652-1-1', '___', '2019-11-22 08:05:05', '2019-11-22 08:05:05', 'Антон', '+7(916) 265-51-92', 0, NULL, NULL, 'Погонный проезд', '5к4', '61', '1', NULL, 'flembo10@mail.ru'),
(57, '5dde0254eb085', 'PENDING', 189000, '9841-1-1', '___', '2019-11-27 01:57:56', '2019-11-27 01:57:56', 'Галина', '+7(916) 578-23-85', 0, NULL, NULL, 'Краснопролетарская', '16 стр.3', NULL, '8', NULL, NULL),
(58, '5de14ea5e6e13', 'PENDING', 269000, '9891-1-3', '___', '2019-11-29 14:00:21', '2019-11-29 14:00:21', 'Артём', '+7(909) 698-92-45', 0, NULL, NULL, 'Маросейка', '4/2', NULL, NULL, 'Центральный рынок 3 этаж. Позвонить мне как подъедете', NULL),
(59, '5de519005f86d', 'PENDING', 180000, '9901-1-1', '___', '2019-12-02 11:00:32', '2019-12-02 11:00:32', 'Инна', '+7(967) 158-66-58', 0, NULL, NULL, '11 Парковая', '19', '35', '2', '2 этаж, код: 1569', 'kiamalu@mail.ru'),
(60, '5de756e67e358', 'PENDING', 279000, '9801-1-3', '___', '2019-12-04 03:49:10', '2019-12-04 03:49:10', 'Анна', '+7(864) 516-53-67', 0, NULL, NULL, 'Боярский переулок', '4, строение 1', NULL, NULL, 'офис', NULL),
(61, '5de7806dc4565', 'PENDING', 290000, '9921-1-3', '___', '2019-12-04 06:46:21', '2019-12-04 06:46:21', 'Дмитрий', '+7(916) 178-02-82', 0, NULL, NULL, 'реутов советская', '19', NULL, '2', NULL, 'dima-s87@yandex.ru');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=468 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `file`, `created_at`, `updated_at`) VALUES
(13, '1567880867Z2oGCXvOaCP5VWo.png', '2019-09-07 15:27:48', '2019-09-07 15:27:48'),
(14, '1567880942FR5Sfj4whNOFa9B.jpg', '2019-09-07 15:29:03', '2019-09-07 15:29:03'),
(15, '1567880957fv9ZvyCkCXowFjB.png', '2019-09-07 15:29:18', '2019-09-07 15:29:18'),
(17, '1567881255WV33x4hs2FwYIPY.png', '2019-09-07 15:34:16', '2019-09-07 15:34:16'),
(18, '1567881278QOlkWl5YtHF0crQ.png', '2019-09-07 15:34:39', '2019-09-07 15:34:39'),
(19, '1567881311LI4xwJdS2NgqBgk.png', '2019-09-07 15:35:12', '2019-09-07 15:35:12'),
(20, '1567881367XD8hQ2DuYqw64Ut.png', '2019-09-07 15:36:08', '2019-09-07 15:36:08'),
(68, '1568624383c927fGwDpKkg57M.jpg', '2019-09-16 05:59:44', '2019-09-16 05:59:44'),
(77, '15686531582oZacdlLtncvoyh.jpg', '2019-09-16 13:59:19', '2019-09-16 13:59:19'),
(86, '15686566366fsAnX28CWRh3Cm.jpg', '2019-09-16 14:57:16', '2019-09-16 14:57:16'),
(95, '1568657057iO1Y6eiBlDvAmLX.jpg', '2019-09-16 15:04:17', '2019-09-16 15:04:17'),
(107, '1568657320jgtHxyha1pcr6yg.jpg', '2019-09-16 15:08:40', '2019-09-16 15:08:40'),
(116, '1568657514SAfRseb8DhgfhZ1.jpg', '2019-09-16 15:11:54', '2019-09-16 15:11:54'),
(126, '1568657702ozR1M94kU5aVmI2.jpg', '2019-09-16 15:15:02', '2019-09-16 15:15:02'),
(131, '1568657803drclGbgV5bN2pOF.jpg', '2019-09-16 15:16:43', '2019-09-16 15:16:43'),
(139, '1568657985a8N7J265p0kM3Hv.jpg', '2019-09-16 15:19:45', '2019-09-16 15:19:45'),
(143, '1568658110kyLknWgUdKydZyl.jpg', '2019-09-16 15:21:51', '2019-09-16 15:21:51'),
(147, '1568658205R7ACZrZa0l4YpZj.jpg', '2019-09-16 15:23:25', '2019-09-16 15:23:25'),
(152, '1568658255HquTKnTyqDlZ06Q.jpg', '2019-09-16 15:24:16', '2019-09-16 15:24:16'),
(153, '1568658853xI7iDEsJKi8fJgN.jpg', '2019-09-16 15:34:14', '2019-09-16 15:34:14'),
(158, '1568659083ejVC0DQj1vHr5Du.jpg', '2019-09-16 15:38:03', '2019-09-16 15:38:03'),
(168, '15686593011dBDbONSTzZTpxq.jpg', '2019-09-16 15:41:41', '2019-09-16 15:41:41'),
(174, '1568659430IJsCuRD6xSRBGXf.jpg', '2019-09-16 15:43:50', '2019-09-16 15:43:50'),
(179, '1568659651o9zYr6xB8w3lFVH.jpg', '2019-09-16 15:47:31', '2019-09-16 15:47:31'),
(187, '1568659863MNefUmixszlZKSx.jpg', '2019-09-16 15:51:03', '2019-09-16 15:51:03'),
(203, '1568660404h0zf1MXT84paLMM.jpg', '2019-09-16 16:00:04', '2019-09-16 16:00:04'),
(204, '1568660437hca2Y5tCWcge6UH.jpg', '2019-09-16 16:00:37', '2019-09-16 16:00:37'),
(205, '1568660438PhOMf4sCBkp4X3s.jpg', '2019-09-16 16:00:38', '2019-09-16 16:00:38'),
(206, '1568660490grUhQ0jGJsSkr03.jpg', '2019-09-16 16:01:30', '2019-09-16 16:01:30'),
(210, '1568661002w5DqNUBy8gSS7mi.jpg', '2019-09-16 16:10:02', '2019-09-16 16:10:02'),
(211, '1568661019TnkuTqzDFj4z4zZ.jpg', '2019-09-16 16:10:19', '2019-09-16 16:10:19'),
(212, '1568661138msIeP7yrtkZvoWl.jpg', '2019-09-16 16:12:18', '2019-09-16 16:12:18'),
(214, '1568661162sEEVYik63nU6gqJ.jpg', '2019-09-16 16:12:42', '2019-09-16 16:12:42'),
(215, '1568661176X1kA6bLcak7cKR8.jpg', '2019-09-16 16:12:56', '2019-09-16 16:12:56'),
(216, '1568661187xb6pMfGQrc9Onzp.jpg', '2019-09-16 16:13:07', '2019-09-16 16:13:07'),
(218, '1568897067zb6GdJqjeSZbVu3.jpg', '2019-09-19 09:44:28', '2019-09-19 09:44:28'),
(219, '1568897090H4ocJb5cyQsmJC2.jpg', '2019-09-19 09:44:51', '2019-09-19 09:44:51'),
(220, '1568897153Th6ueh6duLYjkD2.jpg', '2019-09-19 09:45:54', '2019-09-19 09:45:54'),
(221, '15688971886reArUai9H0Ebb9.jpg', '2019-09-19 09:46:28', '2019-09-19 09:46:28'),
(224, '1568897387N4NnNpIsuhFUCtp.jpg', '2019-09-19 09:49:47', '2019-09-19 09:49:47'),
(244, '1570564368zogUrXzMa5LRnE0.JPG', '2019-10-08 16:52:48', '2019-10-08 16:52:48'),
(245, '1570564390sjeSkvj7iK5ArlB.JPG', '2019-10-08 16:53:10', '2019-10-08 16:53:10'),
(246, '1570564390IVIXHYt66n9xtV0.JPG', '2019-10-08 16:53:10', '2019-10-08 16:53:10'),
(247, '1570564390HGbO2HChfIiJJX5.JPG', '2019-10-08 16:53:10', '2019-10-08 16:53:10'),
(248, '1570564496Hvo3i7eiCxYL4IY.JPG', '2019-10-08 16:54:56', '2019-10-08 16:54:56'),
(249, '1570565319jtkYP2OsFiBe62L.jpg', '2019-10-08 17:08:39', '2019-10-08 17:08:39'),
(250, '15705653198FGclV3FvsHcSjV.jpg', '2019-10-08 17:08:39', '2019-10-08 17:08:39'),
(251, '1570565352IDeNU7koud2K4AA.jpg', '2019-10-08 17:09:13', '2019-10-08 17:09:13'),
(252, '15705653537eRQWfVJwFKxtOv.JPG', '2019-10-08 17:09:13', '2019-10-08 17:09:13'),
(253, '1570565353KEvgFTDLAReLsCR.jpg', '2019-10-08 17:09:13', '2019-10-08 17:09:13'),
(254, '1570565561JlR0uXNQX12wXrH.jpg', '2019-10-08 17:12:41', '2019-10-08 17:12:41'),
(255, '1570565827s2O2mj09F3xOAh6.JPG', '2019-10-08 17:17:07', '2019-10-08 17:17:07'),
(265, '1570566241G7qhl2vUFkTDckz.JPG', '2019-10-08 17:24:01', '2019-10-08 17:24:01'),
(266, '1570566256mjRtMnj0QsSLxaH.jpg', '2019-10-08 17:24:17', '2019-10-08 17:24:17'),
(267, '1570566257uNBrV23DRVw6nN8.JPG', '2019-10-08 17:24:17', '2019-10-08 17:24:17'),
(271, '1570566297rVQTTA5jRpx5MfZ.jpg', '2019-10-08 17:24:58', '2019-10-08 17:24:58'),
(272, '1570566298weCAyTrr6TqUmZu.jpg', '2019-10-08 17:24:58', '2019-10-08 17:24:58'),
(273, '1570566298mdSFxpRZVfinuIq.jpg', '2019-10-08 17:24:58', '2019-10-08 17:24:58'),
(286, '157056818003nlOJhjmaw6R0r.jpg', '2019-10-08 17:56:21', '2019-10-08 17:56:21'),
(294, '1570568258nfUkTzaHkdVuuvb.jpg', '2019-10-08 17:57:38', '2019-10-08 17:57:38'),
(305, '1570570702y6IdNuEVDiR7RHM.jpg', '2019-10-08 18:38:22', '2019-10-08 18:38:22'),
(310, '1570570855photo_2019-06-18_20-58-19.jpg', '2019-10-08 18:40:55', '2019-10-08 18:40:55'),
(311, '1570570858photo_2019-06-18_20-58-22.jpg', '2019-10-08 18:40:58', '2019-10-08 18:40:58'),
(312, '15706073723Bw5NrlPPJ6jjcz.jpg', '2019-10-09 04:49:32', '2019-10-09 04:49:32'),
(313, '1570607389J9Cvr6XWQfEcfVS.jpg', '2019-10-09 04:49:49', '2019-10-09 04:49:49'),
(314, '1570607389POPWY8FPVZaCqM5.jpg', '2019-10-09 04:49:49', '2019-10-09 04:49:49'),
(319, '15706074203XCoeg82qGHIlMq.jpg', '2019-10-09 04:50:20', '2019-10-09 04:50:20'),
(320, '1570607420adNvSJWoijEl0Rb.jpg', '2019-10-09 04:50:20', '2019-10-09 04:50:20'),
(321, '1570607420Bbs7zZrSI0JExey.JPG', '2019-10-09 04:50:20', '2019-10-09 04:50:20'),
(322, '1570607448RMvtjS8jHAoPnzw.JPG', '2019-10-09 04:50:48', '2019-10-09 04:50:48'),
(323, '1570607448UdlU5m5PHrnCanN.jpg', '2019-10-09 04:50:48', '2019-10-09 04:50:48'),
(324, '1570607448lI1IzxmhizOYhRe.jpg', '2019-10-09 04:50:48', '2019-10-09 04:50:48'),
(325, '1570607448i4mNw1EYu9Ww1LN.jpg', '2019-10-09 04:50:48', '2019-10-09 04:50:48'),
(326, '1570607773Y0kx5ExornoKS3Y.JPG', '2019-10-09 04:56:13', '2019-10-09 04:56:13'),
(327, '1570607789aDTI1VZhoCjEHkX.jpg', '2019-10-09 04:56:29', '2019-10-09 04:56:29'),
(328, '1570607789pi0SoapetANbbwq.jpg', '2019-10-09 04:56:29', '2019-10-09 04:56:29'),
(329, '1570607801BgYR7EeWVQB3SbW.JPG', '2019-10-09 04:56:41', '2019-10-09 04:56:41'),
(330, '1570607811eGKCYHFa87Unutv.JPG', '2019-10-09 04:56:51', '2019-10-09 04:56:51'),
(331, '1570608083yHMNICU4tfcLXAa.jpg', '2019-10-09 05:01:23', '2019-10-09 05:01:23'),
(332, '1570608100r84i3mcGEjXypwU.jpg', '2019-10-09 05:01:40', '2019-10-09 05:01:40'),
(333, '1570608100RNgNwRW5GTwOCkS.jpg', '2019-10-09 05:01:40', '2019-10-09 05:01:40'),
(334, '1570608100O3oZPjET5q9BvJj.jpg', '2019-10-09 05:01:40', '2019-10-09 05:01:40'),
(335, '1570608100k8S4bk1l4cLOTyI.jpg', '2019-10-09 05:01:40', '2019-10-09 05:01:40'),
(336, '1570608269Gk3Y5uKAbZspdSh.JPG', '2019-10-09 05:04:30', '2019-10-09 05:04:30'),
(337, '1570608302GnmGto9ZkJXKYi5.jpg', '2019-10-09 05:05:02', '2019-10-09 05:05:02'),
(338, '1570608302kYoUAKQOthB45rK.jpg', '2019-10-09 05:05:02', '2019-10-09 05:05:02'),
(339, '1570608302HGlqy4EZeXvQElH.JPG', '2019-10-09 05:05:02', '2019-10-09 05:05:02'),
(340, '1570608532iscIoaQkrp4Sc0K.jpg', '2019-10-09 05:08:52', '2019-10-09 05:08:52'),
(341, '1570608554tJ0AaoZZdyANpoG.JPG', '2019-10-09 05:09:14', '2019-10-09 05:09:14'),
(342, '1570608554Rq6MkPpXKeieLut.JPG', '2019-10-09 05:09:14', '2019-10-09 05:09:14'),
(343, '15706085541MG47Miuy6oz9a9.JPG', '2019-10-09 05:09:14', '2019-10-09 05:09:14'),
(344, '1570608973pGsl2SOB40SOb84.JPG', '2019-10-09 05:16:13', '2019-10-09 05:16:13'),
(345, '1570608991BGW2eU7b59aMzP4.jpg', '2019-10-09 05:16:31', '2019-10-09 05:16:31'),
(346, '1570608991VCzbEdOprl9ixUa.jpg', '2019-10-09 05:16:31', '2019-10-09 05:16:31'),
(347, '1570608991hqdgmCvedSpxD8R.jpg', '2019-10-09 05:16:31', '2019-10-09 05:16:31'),
(348, '1570608991dLLR09cr0gKLqYX.JPG', '2019-10-09 05:16:31', '2019-10-09 05:16:31'),
(349, '15707987919zZ9zQKEpLrOQQd.jpg', '2019-10-11 09:59:51', '2019-10-11 09:59:51'),
(350, '1570798933qvf0ykihLSNp4KR.JPG', '2019-10-11 10:02:13', '2019-10-11 10:02:13'),
(351, '1570799099GVQxCtDa9FvWaih.JPG', '2019-10-11 10:04:59', '2019-10-11 10:04:59'),
(352, '1570799114WCJv2hWlG06Ljls.jpg', '2019-10-11 10:05:14', '2019-10-11 10:05:14'),
(353, '157115988995LuVex533Y6M7Q.jpg', '2019-10-15 14:18:09', '2019-10-15 14:18:09'),
(354, '15711599654q0q8MJvrNNzGS0.jpg', '2019-10-15 14:19:25', '2019-10-15 14:19:25'),
(355, '1571160445zIOcQQxzrgF0irF.jpg', '2019-10-15 14:27:25', '2019-10-15 14:27:25'),
(356, '1571166535d5ChgKSBhpw7y2U.jpg', '2019-10-15 16:08:55', '2019-10-15 16:08:55'),
(357, '1571168627T8OaAWivTl2CnZG.jpg', '2019-10-15 16:43:47', '2019-10-15 16:43:47'),
(370, '1571170909Z7Ybf61r7wpNOCl.jpg', '2019-10-15 17:21:49', '2019-10-15 17:21:49'),
(371, '15711709095kTqRTG4G4rkWNZ.jpg', '2019-10-15 17:21:49', '2019-10-15 17:21:49'),
(372, '1571170909MQUfR0jCON9JXGS.jpg', '2019-10-15 17:21:50', '2019-10-15 17:21:50'),
(373, '1571170910i98TVFXc4S5Jxj8.jpg', '2019-10-15 17:21:50', '2019-10-15 17:21:50'),
(374, '1572349309r2VfYA1pKIdbVEt.jpg', '2019-10-29 08:41:49', '2019-10-29 08:41:49'),
(375, '1572349354J6gThJjLQpFBwiD.jpg', '2019-10-29 08:42:34', '2019-10-29 08:42:34'),
(376, '1572349394NtVB9R4neJQlhCB.jpg', '2019-10-29 08:43:14', '2019-10-29 08:43:14'),
(386, '1572349688z2e3Ad2r6MkV2Vy.jpg', '2019-10-29 08:48:08', '2019-10-29 08:48:08'),
(387, '1572349688Z2J5hR44QgIDNKd.jpg', '2019-10-29 08:48:08', '2019-10-29 08:48:08'),
(388, '1572349709pPcl9FFbUolmjXv.jpg', '2019-10-29 08:48:29', '2019-10-29 08:48:29'),
(389, '1572349709o3FUdqrppxWi19N.jpg', '2019-10-29 08:48:29', '2019-10-29 08:48:29'),
(390, '1572349728SggCjxsbCljJuTH.JPG', '2019-10-29 08:48:48', '2019-10-29 08:48:48'),
(391, '157234972870QpFQSNT5HRLP4.JPG', '2019-10-29 08:48:48', '2019-10-29 08:48:48'),
(392, '1572349728BVawlXPOzqOMFNZ.JPG', '2019-10-29 08:48:48', '2019-10-29 08:48:48'),
(393, '1573732985CveyEzHdgUPjrci.jpg', '2019-11-14 09:03:05', '2019-11-14 09:03:05'),
(394, '1573732985vPhx5TcywaxXCmn.jpg', '2019-11-14 09:03:05', '2019-11-14 09:03:05'),
(395, '1573733010Q3FmbcEvJNLhHeI.jpg', '2019-11-14 09:03:30', '2019-11-14 09:03:30'),
(396, '1573733010HME7Ly2E3iu2MOj.jpg', '2019-11-14 09:03:30', '2019-11-14 09:03:30'),
(397, '1573733046QreixLgDhlLwifF.jpg', '2019-11-14 09:04:06', '2019-11-14 09:04:06'),
(398, '1573733046UfGjPFpTKXCG5pD.jpg', '2019-11-14 09:04:06', '2019-11-14 09:04:06'),
(399, '1573733046IiV8lLv1GpfVSxz.jpg', '2019-11-14 09:04:06', '2019-11-14 09:04:06'),
(400, '1573733046TZrbMmouw4z4KaP.jpg', '2019-11-14 09:04:06', '2019-11-14 09:04:06'),
(401, '1573733169e7w0gVViejY8BiB.jpg', '2019-11-14 09:06:09', '2019-11-14 09:06:09'),
(402, '1573733169mu1Ufy6nJDIc0Dz.jpg', '2019-11-14 09:06:09', '2019-11-14 09:06:09'),
(403, '1573733190FIZJT5S0Ug1yrVb.jpg', '2019-11-14 09:06:30', '2019-11-14 09:06:30'),
(404, '1573733190yvb9McfHEQ7mY60.JPG', '2019-11-14 09:06:30', '2019-11-14 09:06:30'),
(405, '1573733190D35BtEFgokTY5MU.JPG', '2019-11-14 09:06:30', '2019-11-14 09:06:30'),
(406, '1573733214OttsfLzDZXGlS75.JPG', '2019-11-14 09:06:54', '2019-11-14 09:06:54'),
(407, '1573733214uI5kwgLVBlL3I5V.jpg', '2019-11-14 09:06:54', '2019-11-14 09:06:54'),
(408, '1573733287RaLEwOHRtHUbaZQ.jpg', '2019-11-14 09:08:07', '2019-11-14 09:08:07'),
(409, '1573733287YU1D4PigBKQrqeL.jpg', '2019-11-14 09:08:07', '2019-11-14 09:08:07'),
(410, '1573733311xFaoLQpJQuRcHRG.jpg', '2019-11-14 09:08:31', '2019-11-14 09:08:31'),
(411, '1573733311706ky2YVOinlmho.jpg', '2019-11-14 09:08:31', '2019-11-14 09:08:31'),
(412, '1573733339QScb24MaGtHkbLd.jpg', '2019-11-14 09:08:59', '2019-11-14 09:08:59'),
(413, '1573733339nycjcEeMFqvM2dI.JPG', '2019-11-14 09:08:59', '2019-11-14 09:08:59'),
(414, '1573733339AdwnXlieXMTpz9z.JPG', '2019-11-14 09:08:59', '2019-11-14 09:08:59'),
(415, '1573733339SiL61YvrAzvlMOg.jpg', '2019-11-14 09:08:59', '2019-11-14 09:08:59'),
(416, '1573733408napmqf6pqcVB4gZ.jpg', '2019-11-14 09:10:08', '2019-11-14 09:10:08'),
(417, '1573733408q6Tzsm7vVp8Qgya.jpg', '2019-11-14 09:10:08', '2019-11-14 09:10:08'),
(418, '1573733437wRftoBbyxG7PKDo.jpg', '2019-11-14 09:10:37', '2019-11-14 09:10:37'),
(419, '1573733437FYt23wV59mB02c3.jpg', '2019-11-14 09:10:37', '2019-11-14 09:10:37'),
(420, '1573733437rPKtNj5eKGMuZ0m.jpg', '2019-11-14 09:10:37', '2019-11-14 09:10:37'),
(421, '1573733455x8Hfcvc8bUZdF8T.jpg', '2019-11-14 09:10:55', '2019-11-14 09:10:55'),
(422, '1573733455wnmXmBWMO6ZQYBg.jpg', '2019-11-14 09:10:55', '2019-11-14 09:10:55'),
(423, '1574425976V1vFanp3b9QvLxc.png', '2019-11-22 09:32:56', '2019-11-22 09:32:56'),
(424, '1574426084lhIJdUVHfTTdffr.png', '2019-11-22 09:34:45', '2019-11-22 09:34:45'),
(425, '1574426152Bv2UPL42vXSAzpl.png', '2019-11-22 09:35:52', '2019-11-22 09:35:52'),
(426, '15744262484tjgY1b1SZXu13H.png', '2019-11-22 09:37:28', '2019-11-22 09:37:28'),
(427, '15744263148Vn9cUtciFbX8li.png', '2019-11-22 09:38:35', '2019-11-22 09:38:35'),
(428, '1574426404wQWQTiKcPLRPV9X.png', '2019-11-22 09:40:04', '2019-11-22 09:40:04'),
(432, '1574522146KvstfS7wSeHVUeQ.jpg', '2019-11-23 12:15:46', '2019-11-23 12:15:46'),
(433, '15745221577SSq3t26JhurLYX.jpg', '2019-11-23 12:15:57', '2019-11-23 12:15:57'),
(434, '1574522219XKprwJWsYWlF8B9.jpg', '2019-11-23 12:16:59', '2019-11-23 12:16:59'),
(435, '1574522226Hr1EKorQYXcHBmS.jpg', '2019-11-23 12:17:06', '2019-11-23 12:17:06'),
(436, '1575389330ZS3HRteq4ZsCtC9.jpg', '2019-12-03 13:08:50', '2019-12-03 13:08:50'),
(438, '1575389741ARPGawC6TqtAPn1.jpg', '2019-12-03 13:15:41', '2019-12-03 13:15:41'),
(439, '1575389786VYswDB1dZmNDvdR.jpg', '2019-12-03 13:16:27', '2019-12-03 13:16:27'),
(440, '1575389825hauXU5lzlcRTYAH.jpg', '2019-12-03 13:17:05', '2019-12-03 13:17:05'),
(441, '15753898597gXUb6gsnJC4RuQ.jpg', '2019-12-03 13:17:39', '2019-12-03 13:17:39'),
(442, '1575390027TeNBvbDZUsHDtZL.jpg', '2019-12-03 13:20:27', '2019-12-03 13:20:27'),
(443, '15753900388USkpZ0wr21kOb6.jpg', '2019-12-03 13:20:38', '2019-12-03 13:20:38'),
(444, '1575390084d4EBnyUpRIIuPID.jpg', '2019-12-03 13:21:25', '2019-12-03 13:21:25'),
(445, '1575390132J6n5r0cMnh7raSY.jpg', '2019-12-03 13:22:12', '2019-12-03 13:22:12'),
(446, '1575390147H0pozNKIBxTmTiT.jpg', '2019-12-03 13:22:27', '2019-12-03 13:22:27'),
(447, '1575390162sFSSlN3XQArPP0F.jpg', '2019-12-03 13:22:42', '2019-12-03 13:22:42'),
(448, '1575392424JgbRERBSu0vlgoT.jpeg', '2019-12-03 14:00:25', '2019-12-03 14:00:25'),
(459, '1575393443XMJICz3XCCMCGLB.jpg', '2019-12-03 14:17:24', '2019-12-03 14:17:24'),
(460, '1575393458f4RZInwtUb7mpP5.jpg', '2019-12-03 14:17:38', '2019-12-03 14:17:38'),
(461, '1575393458wqCIPQOsqQ2efw5.jpg', '2019-12-03 14:17:38', '2019-12-03 14:17:38'),
(462, '1575393469Jip3BChy2kmdh6L.jpg', '2019-12-03 14:17:49', '2019-12-03 14:17:49'),
(463, '1575393469eWTK1yLaNsDw10d.jpg', '2019-12-03 14:17:50', '2019-12-03 14:17:50'),
(464, '1575393481by1WvmFkLkkJhvN.jpg', '2019-12-03 14:18:01', '2019-12-03 14:18:01'),
(465, '1575393481oim0JKoynzlHgSs.jpg', '2019-12-03 14:18:02', '2019-12-03 14:18:02'),
(466, '1575393490LYuzx6EYD4qgcGG.jpg', '2019-12-03 14:18:10', '2019-12-03 14:18:10'),
(467, '1575393490cUWCDTYVRqFihOk.jpg', '2019-12-03 14:18:11', '2019-12-03 14:18:11');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_id` smallint(5) UNSIGNED DEFAULT NULL,
  `category_id` smallint(5) UNSIGNED DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `released_on` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '500',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `photo_id`, `category_id`, `discount`, `released_on`, `created_at`, `updated_at`, `price`, `color`, `sort`) VALUES
(49, 'Амур', 356, 1, 20, NULL, '2019-09-16 14:57:17', '2019-12-03 14:23:48', NULL, '#fadcf8', 1),
(50, 'Виолетти', 357, 1, NULL, NULL, '2019-09-16 15:04:19', '2019-12-03 14:22:53', NULL, '#31dbf4', 2),
(51, 'Бананза', 107, 1, NULL, NULL, '2019-09-16 15:08:40', '2019-11-23 12:31:43', NULL, '#e9b77a', 7),
(52, 'Флоренция', 116, 1, NULL, NULL, '2019-09-16 15:11:54', '2019-10-31 06:08:43', NULL, '#f9f7be', 44),
(53, 'Вэлэнтайн', 344, 1, NULL, NULL, '2019-09-16 15:15:02', '2019-11-24 12:05:52', NULL, '#f49eb7', 5),
(54, 'Радуга', 354, 1, NULL, NULL, '2019-09-16 15:16:43', '2019-11-24 12:04:03', NULL, '#b2f4ac', 3),
(55, 'Лаванда', 340, 1, NULL, NULL, '2019-09-16 15:19:45', '2019-11-24 12:05:15', NULL, '#f7d4f4', 4),
(56, 'Африка', 336, 1, 5, NULL, '2019-09-16 15:21:51', '2019-11-24 12:14:38', NULL, '#fac869', 8),
(57, 'Гармония', 331, 1, NULL, NULL, '2019-09-16 15:23:25', '2019-10-09 05:01:23', NULL, '#c0d5fd', 9),
(58, 'Индиго', 326, 1, NULL, NULL, '2019-09-16 15:34:14', '2019-11-24 12:07:35', NULL, '#e9acf9', 10),
(59, 'Лето', 312, 1, NULL, NULL, '2019-09-16 15:38:03', '2019-10-09 04:49:32', NULL, '#eef08f', 11),
(60, 'Фламинго', 265, 1, NULL, NULL, '2019-09-16 15:41:41', '2019-12-03 14:20:27', NULL, '#f999be', 12),
(61, 'Бэлла', 376, 1, NULL, NULL, '2019-09-16 15:43:50', '2019-10-29 08:43:14', NULL, '#f3a2b5', 13),
(62, 'Подсолнухи', 355, 1, 15, NULL, '2019-09-16 15:51:03', '2019-12-03 14:20:31', NULL, '#f3f3b3', 11),
(63, 'Верано', 203, 1, NULL, NULL, '2019-09-16 16:00:04', '2019-09-16 16:00:04', NULL, '#d3e7d5', 14),
(64, 'Марсель', 248, 1, 15, NULL, '2019-09-16 16:01:30', '2019-11-24 12:13:00', NULL, '#f58c6c', 14),
(65, 'Открытка', 428, 2, NULL, NULL, '2019-09-16 16:10:02', '2019-11-22 09:44:18', '190', NULL, 9),
(66, 'Открытка', 426, 2, NULL, NULL, '2019-09-16 16:10:19', '2019-11-22 09:37:28', '190', NULL, 10),
(67, 'Открытка', 425, 2, NULL, NULL, '2019-09-16 16:12:18', '2019-11-22 09:44:11', '190', NULL, 8),
(69, 'Открытка', 427, 2, NULL, NULL, '2019-09-16 16:12:42', '2019-11-22 09:44:05', '190', NULL, 7),
(70, 'Открытка', 423, 2, NULL, NULL, '2019-09-16 16:12:56', '2019-11-22 09:43:59', '190', NULL, 6),
(71, 'Открытка', 424, 2, NULL, NULL, '2019-09-16 16:13:07', '2019-11-22 09:43:52', '190', NULL, 4),
(75, 'Топпер', 435, 2, NULL, NULL, '2019-10-11 09:59:51', '2019-11-23 12:17:06', '190', NULL, 3),
(76, 'Топпер', 434, 2, NULL, NULL, '2019-10-11 10:04:59', '2019-11-23 12:16:59', '190', NULL, 2),
(77, 'Топпер', 352, 2, NULL, NULL, '2019-10-11 10:05:14', '2019-11-22 09:43:34', '190', NULL, 1),
(81, 'Букеты по Акции!', 443, 1, 45, NULL, '2019-12-03 13:08:50', '2019-12-03 13:33:08', NULL, '#e3c0c8', 600),
(82, 'Букеты по Акции!', 442, 1, 45, NULL, '2019-12-03 13:20:27', '2019-12-03 13:33:21', NULL, '#dab2bb', 660),
(83, 'Sweet Box', 459, 1, NULL, NULL, '2019-12-03 14:00:25', '2019-12-03 14:21:02', NULL, '#ffffff', 700);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image` smallint(5) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `video`, `image`, `created_at`, `updated_at`) VALUES
(66, NULL, 310, '2019-10-08 18:40:55', '2019-10-08 18:40:55'),
(67, NULL, 311, '2019-10-08 18:40:58', '2019-10-08 18:40:58');

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `name`, `address`, `lat`, `lng`, `created_at`, `updated_at`) VALUES
(1, 'Самовывоз (м. Строгино)', 'Ул. Маршала Катукова д 24 к 4', '55.803712', '37.413403', NULL, '2019-12-03 16:12:47'),
(2, 'Самовывоз (м. Аэропорт)', 'Ул. Коккинаки д 4', '55.804304', '37.546138', '2019-06-18 14:51:52', '2019-12-03 16:12:52'),
(3, 'Самовывоз', 'ТЦ Лига, Ленинградское шоссе вл 5', '55.888997', '37.433046', '2019-06-18 14:53:04', '2019-06-18 14:53:27');

-- --------------------------------------------------------

--
-- Table structure for table `size`
--

CREATE TABLE IF NOT EXISTS `size` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `size`
--

INSERT INTO `size` (`id`, `name`, `created_at`, `updated_at`) VALUES
(3, 'S', '2019-09-04 11:31:35', '2019-09-04 11:31:35'),
(4, 'M', '2019-09-04 11:31:39', '2019-09-04 11:31:39'),
(5, 'L', '2019-09-04 11:31:42', '2019-09-04 11:31:42'),
(10, 'S1', '2019-12-03 13:09:59', '2019-12-03 13:09:59'),
(11, 'S2', '2019-12-03 13:10:01', '2019-12-03 13:10:01'),
(12, 'S3', '2019-12-03 13:10:03', '2019-12-03 13:10:03'),
(13, 'S4', '2019-12-03 13:10:05', '2019-12-03 13:10:05'),
(14, 'S5', '2019-12-03 13:10:08', '2019-12-03 13:10:08'),
(15, 'S5', '2019-12-03 13:10:10', '2019-12-03 13:10:10');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subcategories_category_id_index` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `texts`
--

CREATE TABLE IF NOT EXISTS `texts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slider` text COLLATE utf8mb4_unicode_ci,
  `title2` text COLLATE utf8mb4_unicode_ci,
  `text2` text COLLATE utf8mb4_unicode_ci,
  `title3` text COLLATE utf8mb4_unicode_ci,
  `text3` text COLLATE utf8mb4_unicode_ci,
  `title4` text COLLATE utf8mb4_unicode_ci,
  `text4` text COLLATE utf8mb4_unicode_ci,
  `title5` text COLLATE utf8mb4_unicode_ci,
  `text5` text COLLATE utf8mb4_unicode_ci,
  `title6` text COLLATE utf8mb4_unicode_ci,
  `text6` text COLLATE utf8mb4_unicode_ci,
  `title7` text COLLATE utf8mb4_unicode_ci,
  `text7` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `privacy` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `texts`
--

INSERT INTO `texts` (`id`, `slider`, `title2`, `text2`, `title3`, `text3`, `title4`, `text4`, `title5`, `text5`, `title6`, `text6`, `title7`, `text7`, `created_at`, `updated_at`, `privacy`) VALUES
(1, '<h1>Сладкие воздушные<br />\r\nбукеты из зефира<br />\r\nи мармеладок</h1>', 'Используем самые качественные ингридиенты', '<p>Наши букеты из маршмеллоу и мармелада&nbsp;состоят из высококачественной продукции крупнейших европейских производителей, таких как Fini Sweets (Италия), Bulgari (Испания), Damel (Испания),&nbsp; Golmasa (Испания). Ведущие компании имеют огромный опыт и большую историю и продолжают традиции в кондитерском деле, имеют самые высокие международные оценки качества и безопасности пищевых продуктов, сертификаты IFS, BRC и ISO 9001: 2008.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Зефирные букеты всегда самого высшего качества и свежести, соблюдаются все условия хранения . Наше суфле остаётся мягким и свежим на протяжении долгого времени после изготовления букета. Мы собираем сладкие букеты согласно фитосанитарным нормам в перчатках и упаковываем в пищевую слюду за 2 часа до доставки, что позволяет гарантировать качество нашей продукции.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Букеты из маршмеллоу - всегда отличный подарок на любой праздник, юбилей, день рождение, свадьбу, 1 сентября учителю, 8 марта.</p>', 'Соблюдаем все санитарные нормы', '<h4 class=\"subtitle\">Букеты собираются в перчатках и упаковываются в плёнку</h4>', 'Доставка и оплата  <br> по Москве и области', '<p><strong>Бесплатная доставка</strong>&nbsp;по Москве в пределах МКАД осуществляется на следующий день после заказа.&nbsp;Возможна доставка день в день по согласованию.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Оплата курьеру&nbsp;<strong>наличными</strong>&nbsp;или банковским&nbsp;<strong>переводом</strong>&nbsp;на карту.&nbsp;</p>', 'Выбери свой букет', '<p>Наши букеты настолько вкусно пахнут, что перед доставкой букета мы кормим курьера зефиром, т.к. есть шанс, что он его съест</p>', 'Полезные сладости – это не миф', '<p>Основа зефира &ndash; фруктово-ягодное пюре. Его готовят из яблок, груш, клюквы, черной смородины и других плодов. Все они богаты витаминами и минералами, органическими кислотами и пектином.&nbsp;Воздушный зефир не содержит ни грамма жира, он легко переваривается, поэтому такое лакомство &laquo;не нагружает&raquo; наш желудок, как пирожные и торты. А за счет содержания пектина или другого желирующего агента зефир даже улучшает пищеварение. Сладкий букет может быть полезным!</p>', 'Хочешь сделать корпоративный заказ?', '<p>Оставь свой номер телефона и мы свяжемся с тобой в ближайшее время, что бы рассказать про условия.</p>', NULL, '2019-10-03 07:59:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tproducts`
--

CREATE TABLE IF NOT EXISTS `tproducts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` text COLLATE utf8mb4_unicode_ci,
  `product_id` smallint(5) UNSIGNED DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `mass` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `size` smallint(5) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tproducts`
--

INSERT INTO `tproducts` (`id`, `name`, `icon`, `product_id`, `price`, `mass`, `priority`, `desc`, `created_at`, `updated_at`, `size`) VALUES
(32, 'Амур', '[370,371,372,373]', 49, 3362, '1000', '1', '<p>Мармеладки-ягодки со вкусом йогурта и малины</p>\r\n\r\n<p>в элегантном сочетании с классическим сахарным суфле. d=35см.</p>', '2019-09-16 14:58:17', '2019-12-03 15:21:24', 5),
(42, 'Вэлэнтайн', '[345,346,347,348]', 53, 2490, '1000', '1', '<p>Нежное классическое маршмеллоу с ванильным ароматом в форме сердца.&nbsp;d=40см.</p>', '2019-09-16 15:15:49', '2019-12-03 17:32:50', 5),
(46, 'Лаванда', '[341,342,343]', 55, 2990, '1000', '1', '<p>Букет суфле с тонкой ноткой лесных ягод и удивительными ежевичными мармеладками.&nbsp;d=38см.</p>', '2019-09-16 15:20:52', '2019-12-03 15:35:16', 5),
(47, 'Африка', '[337,338,339]', 56, 2579, '580', '1', '<p>Микс из ванильных трубочек,</p>\r\n\r\n<p>сахарных маргариток,</p>\r\n\r\n<p>персиковых мармеладок и мега суфле.&nbsp;d=27см.</p>', '2019-09-16 15:22:31', '2019-12-03 15:37:52', 4),
(49, 'Индиго', '[327,328]', 58, 1790, '360', '1', '<p>Взрывной букет из маршмеллоу лесные ягоды с насыщенным вкусом персиковых и бабл гам мармеладок.&nbsp;d=22см.</p>', '2019-09-16 15:35:02', '2019-12-03 15:52:29', 3),
(50, 'Индиго', '[329]', 58, 2290, '580', NULL, '<p>Взрывной букет из маршмеллоу лесные ягоды &nbsp;с насыщенным вкусом персиковых и бабл гам мармеладок.&nbsp;d=27см.</p>', '2019-09-16 15:35:41', '2019-12-03 15:52:22', 4),
(51, 'Индиго', '[330]', 58, 2690, '1000', NULL, '<p>Взрывной букет из маршмеллоу лесные ягоды с насыщенным вкусом персиковых и бабл гам мармеладок.&nbsp;d=37см.</p>', '2019-09-16 15:36:08', '2019-12-03 15:52:29', 5),
(52, 'Лето', '[313,314]', 59, 1790, '360', NULL, '<p>Сахарные купола с запахом ванили и жевательный мармелад со вкусом персика в карамельной обсыпке.&nbsp;d=22см.</p>\r\n\r\n<p>&nbsp;</p>', '2019-09-16 15:38:36', '2019-12-03 15:53:22', 3),
(53, 'Лето', '[322,323,324,325]', 59, 2490, '750', '1', '<p>Сахарные купола с запахом ванили и жевательный мармелад со вкусом персика в карамельной обсыпке.&nbsp;d=29см.</p>', '2019-09-16 15:39:37', '2019-12-03 15:54:04', 4),
(54, 'Лето', '[319,320,321]', 59, 3300, '2000', NULL, '<p>Сахарные купола с запахом ванили и жевательный мармелад со вкусом персика в карамельной обсыпке.&nbsp;d=44см.</p>', '2019-09-16 15:40:03', '2019-12-03 15:53:59', 5),
(55, 'Фламинго', '[266,267]', 60, 1790, '360', NULL, '<p>Мы предложили вам суфлесо вкусом мороженного в виде рожка. Так же малиновые ягоды и сливочное маршмеллоу. d=22см.</p>', '2019-09-16 15:42:13', '2019-12-03 15:22:55', 3),
(56, 'Фламинго', '[271,272,273]', 60, 2380, '580', '1', '<p>Мы предложили вам суфле со вкусом мороженного в виде рожка. Так же малиновые ягоды и сливочное маршмеллоу. d=27см.</p>', '2019-09-16 15:42:43', '2019-12-03 15:47:20', 4),
(66, 'Подсолнухи', '[249,250]', 62, 1871, '360', NULL, '<p>Яркий сливочный суфле c дерзкой ноткой ежевичных мармеладок.&nbsp;d=22см.</p>', '2019-09-16 15:55:09', '2019-12-03 15:48:10', 3),
(67, 'Подсолнухи', '[251,252,253]', 62, 2812, '580', '1', '<p>Яркий сливочный суфле c дерзкой ноткой ежевичных мармеладок.&nbsp;d=27см.</p>', '2019-09-16 15:55:30', '2019-12-03 15:48:14', 4),
(68, 'Верано', '[204,205]', 63, 1890, '360', '1', '<p>Букет из воздушных сахарных сливочных маргариток с добавлением эксклюзивной ванильной мастики.&nbsp;d=22см.</p>', '2019-09-16 16:00:38', '2019-12-03 15:56:19', 3),
(69, 'Марсель', '[245,246,247]', 64, 4576, '1500', '1', '<p>Самый мармеладный букет!&nbsp;d=39см.</p>', '2019-09-16 16:01:58', '2019-12-03 15:27:34', 5),
(70, 'Гармония', '[332,333,334,335]', 57, 2350, '580', '1', '<p>Гигантское маршмеллоу дополняется сливочным суфле-мороженное и ягодами со вкусом йогурта и бабл гам. Очень вкусно!&nbsp;d=27см.</p>', '2019-09-27 07:05:07', '2019-12-03 15:54:37', 4),
(78, 'Бэлла', '[386,387]', 61, 1790, '360', NULL, '<p>Мега классическое суфле,ежевичные и малиновые ягоды,дополняющиеся ванильной мастикой. Почувствуй любовь на вкус.&nbsp;d=22см.</p>', '2019-10-29 08:48:08', '2019-12-03 15:55:30', 3),
(79, 'Бэлла', '[388,389]', 61, 2390, '580', NULL, '<p>Мега классическое суфле,ежевичные и малиновые ягоды,дополняющиеся ванильной мастикой. Почувствуй любовь на вкус.&nbsp;d=27см.</p>', '2019-10-29 08:48:29', '2019-12-03 15:55:40', 4),
(80, 'Бэлла', '[390,391,392]', 61, 2790, '1000', '1', '<p>Мега классическое суфле,ежевичные и малиновые ягоды,дополняющиеся ванильной мастикой. Почувствуй любовь на вкус.&nbsp;d=38см.</p>', '2019-10-29 08:48:48', '2019-12-03 15:59:48', 5),
(81, 'Флоренция', '[393,394]', 52, 1790, '360', NULL, '<p>Воздушное суфле сливочно-ванильного вкуса с хрустящими трубочками. Запах Флоренции. d=25см.</p>', '2019-11-14 09:03:05', '2019-12-03 15:45:33', 3),
(82, 'Флоренция', '[395,396]', 52, 2390, '580', NULL, '<p>Воздушное суфле сливочно-ванильного вкуса с хрустящими трубочками. Запах Флоренции. d=30см.</p>', '2019-11-14 09:03:30', '2019-12-03 15:45:20', 4),
(83, 'Флоренция', '[397,398,399,400]', 52, 2790, '1000', '1', '<p>Воздушное суфле сливочно-ванильного вкуса с хрустящими трубочками. Запах Флоренции.&nbsp;d=40см.</p>', '2019-11-14 09:04:06', '2019-12-03 15:45:40', 5),
(84, 'Радуга', '[401,402]', 54, 1890, '360', NULL, '<p>Вы почувствуете неповторимое сочетание аромата нежной ванили с легкими фруктовыми оттенками.&nbsp;d=23см.</p>', '2019-11-14 09:06:09', '2019-12-03 15:51:31', 3),
(85, 'Радуга', '[403,404,405]', 54, 2380, '580', '1', '<p>Вы почувствуете неповторимое сочетание аромата нежной ванили с легкими фруктовыми оттенками.&nbsp;d=28см.</p>', '2019-11-14 09:06:30', '2019-12-03 17:32:13', 4),
(86, 'Радуга', '[406,407]', 54, 2890, '1000', NULL, '<p>Вы почувствуете неповторимое сочетание аромата нежной ванили с легкими фруктовыми оттенками.&nbsp;d=40см.</p>', '2019-11-14 09:06:54', '2019-12-03 17:31:55', 5),
(87, 'Бананза', '[408,409]', 51, 1680, '360', NULL, '<p>Апельсиновый маршмеллоу с дольками арбуза, апельсина, лимона. Огромные бананы и персиковые мармеладные ягоды.&nbsp;d=22см.</p>', '2019-11-14 09:08:07', '2019-12-03 15:24:29', 3),
(88, 'Бананза', '[410,411]', 51, 2290, '560', NULL, '<p>Апельсиновый маршмеллоу с дольками арбуза, апельсина, лимона. Огромные бананы и персиковые мармеладные ягоды.d=27см.</p>', '2019-11-14 09:08:31', '2019-12-03 15:24:38', 4),
(89, 'Бананза', '[412,413,414,415]', 51, 2690, '1000', '1', '<p>Апельсиновый маршмеллоу с дольками арбуза, апельсина, лимона. Огромные бананы и персиковые мармеладные ягоды.d=37см.</p>', '2019-11-14 09:08:59', '2019-12-03 15:24:38', 5),
(90, 'Виолетти', '[416,417]', 50, 1800, '360', NULL, '<p>Хрустящие вафельные трубочки с ванильной ноткой в сливочном маршмеллоу и ягодки бабл гам.&nbsp;d=22см.</p>', '2019-11-14 09:10:08', '2019-12-03 15:56:56', 3),
(91, 'Виолетти', '[418,419,420]', 50, 2400, '580', '1', '<p>Хрустящие вафельные трубочки с ванильной ноткой в сливочном маршмеллоу и ягодки бабл гам.&nbsp;d=27см.</p>', '2019-11-14 09:10:37', '2019-12-03 15:58:28', 4),
(92, 'Виолетти', '[421,422]', 50, 2900, '1000', NULL, '<p>Хрустящие вафельные трубочки с ванильной ноткой в сливочном маршмеллоу и ягодки бабл гам.&nbsp;d=39см.</p>', '2019-11-14 09:10:55', '2019-12-03 15:58:22', 5),
(94, 'Акция', '[438]', 81, 2164, '300', '1', NULL, '2019-12-03 13:15:41', '2019-12-03 16:01:05', 3),
(95, 'Акция', '[439]', 81, 2164, '300', NULL, NULL, '2019-12-03 13:16:27', '2019-12-03 13:32:01', 3),
(96, 'Акция', '[440]', 81, 2164, '300', NULL, NULL, '2019-12-03 13:17:05', '2019-12-03 13:32:12', 3),
(97, 'Акция', '[441]', 81, 2164, '300', NULL, NULL, '2019-12-03 13:17:39', '2019-12-03 16:01:05', 3),
(98, 'Акция', '[444]', 82, 2164, '300', '1', NULL, '2019-12-03 13:21:25', '2019-12-03 16:00:39', 3),
(99, 'Акция', '[445]', 82, 2164, '300', NULL, NULL, '2019-12-03 13:22:12', '2019-12-03 13:32:47', 3),
(100, 'Акция', '[446]', 82, 2164, '300', NULL, NULL, '2019-12-03 13:22:27', '2019-12-03 13:32:55', 3),
(101, 'Акция', '[447]', 82, 2164, '300', NULL, NULL, '2019-12-03 13:22:42', '2019-12-03 16:00:39', 3),
(102, 'Sweet Box', '[460,461]', 83, 1990, '300', '1', '<p>Sweet Box - это микс нежнейшего маршмеллоу и дизайнерская кружка для идеального подарка!</p>', '2019-12-03 14:02:48', '2019-12-03 14:18:24', 4),
(103, 'Sweet Box', '[462,463]', 83, 1990, '300', NULL, '<p>Sweet Box - это микс нежнейшего маршмеллоу и дизайнерская кружка для идеального подарка!</p>', '2019-12-03 14:05:02', '2019-12-03 14:18:01', 4),
(104, 'Sweet Box', '[464,465]', 83, 1990, '300', NULL, '<p>Sweet Box - это микс нежнейшего маршмеллоу и дизайнерская кружка для идеального подарка!</p>', '2019-12-03 14:06:16', '2019-12-03 14:18:10', 4),
(105, 'Sweet Box', '[466,467]', 83, 1990, '300', NULL, '<p>Sweet Box - это микс нежнейшего маршмеллоу и дизайнерская кружка для идеального подарка!</p>', '2019-12-03 14:07:04', '2019-12-03 14:18:24', 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `surname`, `adress`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.com', 'Admin', NULL, NULL, NULL, '$2y$10$6C7ejF8FPb2OMR2buBQKbOmk1dL9l3b.pE8N9VALuXoZpjpqnArDe', 'YIEIsSyUbe3fvS0QafF72EhXziqfHoEgaO5WWvIBe0EnhhOEWYLRgM6ArKoU', '2019-06-10 14:37:44', '2019-06-10 14:37:44');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
