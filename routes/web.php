<?php
Auth::routes();
Route::get('/', 'HomeController@index')->name('mainpage');
Route::get('/cart', 'OrdersController@show')->name('cart');



Route::get('/FeedbackAdd/', 'HomeController@FeedbackAdd')->name('home.feedbackAdd');

Route::get('/addProductsToCart/', 'ProductsController@addProductsToCart')->name('products.addProductsToCart');
Route::get('/addProductsToCartCarts/', 'ProductsController@addProductsToCartCarts')->name('products.addProductsToCartCarts');

Route::get('/xhrAddProductsToCart/{id}/{qty}/{type}', 'ProductsController@xhrAddProductsToCart')->name('products.xhrAddProductsToCart');
Route::get('/deleteProductsFromCart/{id}/{qty}', 'ProductsController@deleteProductsFromCart')->name('products.deleteProductsFromCart');
Route::get('/xhrDeleteProductsFromCart/{id}/{qty}/{type}', 'ProductsController@xhrDeleteProductsFromCart')->name('products.xhrDeleteProductsFromCart');



Route::get('/cart', 'OrdersController@show')->name('cart.show');
Route::get('/buy', 'OrdersController@buy')->name('orders.buy');
Route::get('/coupon', 'OrdersController@coupon')->name('coupon.buy');
Route::get('/catalog', 'HomeController@catalog')->name('catalog');
Route::get('/catalog/{name}', 'HomeController@catalogSection')->name('catalogSection');


Route::get('notify/index', 'NotificationController@index');

//Route::group(['prefix' => 'admin',  'middleware' => 'admin'], function() {
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function() {
    Route::get('/', 'Admin\AdminDashboardController@index')->name('admin.index');
    Route::resource('orders', 'Admin\AdminOrdersController', [
        'names' => [
            'index' => 'admin.orders.index',
            'destroy' => 'admin.orders.destroy',
        ]
    ]);

    Route::resource('users', 'Admin\AdminUsersController', [
        'names' => [
            'index' => 'admin.users.index',
            'store' => 'admin.users.store',
            'create' => 'admin.users.create',
            'destroy' => 'admin.users.destroy',
            'update' => 'admin.users.update',
            'show' => 'admin.users.show',
            'edit' => 'admin.users.edit',
        ]
    ]);


    Route::resource('products', 'Admin\AdminProductsController', [
        'names' => [
            'index' => 'admin.products.index',
            'store' => 'admin.products.store',
            'create' => 'admin.products.create',
            'destroy' => 'admin.products.destroy',
            'update' => 'admin.products.update',
            'show' => 'admin.products.show',
            'edit' => 'admin.products.edit',
        ]
    ]);


    Route::resource('tproducts', 'Admin\AdminTProductsController', [
        'names' => [
            'index' => 'admin.tproducts.index',
            'store' => 'admin.tproducts.store',
            'create' => 'admin.tproducts.create',
            'destroy' => 'admin.tproducts.destroy',
            'update' => 'admin.tproducts.update',
            'show' => 'admin.tproducts.show',
            'edit' => 'admin.tproducts.edit',
        ]
    ]);

    Route::resource('categories', 'Admin\AdminCategoriesController', [
        'names' => [
            'index' => 'admin.categories.index',
            'store' => 'admin.categories.store',
            'create' => 'admin.categories.create',
            'destroy' => 'admin.categories.destroy',
            'update' => 'admin.categories.update',
            'show' => 'admin.categories.show',
            'edit' => 'admin.categories.edit',
        ]
    ]);

    Route::resource('size', 'Admin\AdminSizeController', [
        'names' => [
            'index' => 'admin.size.index',
            'store' => 'admin.size.store',
            'destroy' => 'admin.size.destroy',
        ]
    ]);

    Route::resource('coupons', 'Admin\AdminCouponsController', [
        'names' => [
            'index' => 'admin.coupons.index',
            'store' => 'admin.coupons.store',
            'create' => 'admin.coupons.create',
            'destroy' => 'admin.coupons.destroy',
            'update' => 'admin.coupons.update',
            'show' => 'admin.coupons.show',
            'edit' => 'admin.coupons.edit',
            'edit' => 'admin.coupons.edit',
        ]
    ]);

    Route::resource('reviews', 'Admin\AdminReviewsController', [
        'names' => [
            'index' => 'admin.review.index',
            'store' => 'admin.review.store',
            'create' => 'admin.review.create',
            'destroy' => 'admin.review.destroy',
            'update' => 'admin.review.update',
            'show' => 'admin.review.show',
            'edit' => 'admin.review.edit',
            'edit' => 'admin.review.edit',
        ]
    ]);

    Route::resource('feedback', 'Admin\AdminFeedbackController', [
        'names' => [
            'index' => 'admin.feedback.index',
        ]
    ]);

    Route::resource('settings', 'Admin\AdminSettingsController', [
        'names' => [
            'index' => 'admin.settings.index',
            'edit' => 'admin.settings.edit',
        ]
    ]);

    Route::resource('shops', 'Admin\AdminShopsController', [
        'names' => [
            'index' => 'admin.shops.index',
            'store' => 'admin.shops.store',
            'create' => 'admin.shops.create',
            'destroy' => 'admin.shops.destroy',
            'update' => 'admin.shops.update',
            'show' => 'admin.shops.show',
            'edit' => 'admin.shops.edit',
        ]
    ]);

    Route::get('/products/xhrGetSubcategories/{id}', 'Admin\AdminProductsController@xhrGetSubcategories')->name('getSubcategories');
});


//Route::prefix('/orders')->group(function() {
//    Route::get('/show', 'OrdersController@show')->name('orders.show');
//    Route::get('/summary', 'OrdersController@summary')->name('orders.summary')->middleware('auth');
//    Route::get('/buy', 'OrdersController@buy')->name('orders.buy')->middleware('auth');
//    Route::post('/catchresponsepayu', 'OrdersController@catchResponsePayu')->name('orders.catchresponsepayu');
//    Route::get('/test', 'OrdersController@test')->name('orders.test');
//});


//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});


