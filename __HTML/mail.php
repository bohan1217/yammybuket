<?php
require_once('phpmailer/PHPMailerAutoload.php');
$mail = new PHPMailer;
$mail->CharSet = 'utf-8';

function checkFormType ($typeVal) {
    if ($typeVal === 'requestCall') {
        $phone = $_POST['phone'];  // Переменная для мобильного тел

        return '<h2>С вашего сайта поступила заявка на звонок.</h2><hr>
                <p><b>Перезвоните на этот номер телефона: ' . $phone . '</b></p>';
    }

    if ($typeVal === 'requestGoods') {
        $name = $_POST['name']; // Переменная для имени покупателя
        $phone = $_POST['phone']; // Переменная для мобильного тел
        $bouquet = $_POST['bouquet']; // Переменная выбранного букета

        return '<h2>С вашего сайта поступила заявка c заказом.</h2>
                  <ul>
                      <li><p>Имя покупателя: ' . $name . '</p></li>
                      <li><p>Номер телефона: ' . $phone . '</p></li>
                      <li><p>Выбранный букет: ' . %bouquet . '</p></li>
                  </ul>';
    }
};

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.mail.ru';  																							// Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'postinsite@mail.ru'; // Ваш логин от почты с которой будут отправляться письма
$mail->Password = 'xy9reQgQ7FjASuk'; // Ваш пароль от почты с которой будут отправляться письма
$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 465; // TCP port to connect to / этот порт может отличаться у других провайдеров

$mail->setFrom('postinsite@mail.ru'); // от кого будет уходить письмо?
$mail->addAddress('sascha.sadov@yandex.ru');     // Кому будет уходить письмо
$mail->isHTML(true);  // Set email format to HTML

$mail->Subject = 'Новая заявка с сайта Yammy';
$mail->Body    = checkFormType($_POST['type']);
$mail->AltBody = '';

if(!$mail->send()) {
    $json_data = array ('status' => 500,'name' => "ERROR");
    echo json_encode($json_data);
} else {
    $json_data = array ('status' => 200,'name' => "OK");
    echo json_encode($json_data);
}
?>