<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Feedback;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;


class AdminFeedbackController extends Controller
{
    public function index()
    {
        $feedback = Feedback::all();
        return view('admin.feedback.index', ['feedback' => $feedback]);
    }

    public function destroy($id)
    {
        Feedback::findOrFail($id)->delete();
        Session::flash('alert-success', 'FAQ was successful deleted!');
        return redirect()->route('admin.feedback.index');
    }

}