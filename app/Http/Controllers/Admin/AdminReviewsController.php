<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests\PhotosRequestReview;
use App\Review;
use App\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;



class AdminReviewsController extends Controller
{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
    public function index()
    {
        $reviews = Review::all();
        return view('admin.review.index', ['reviews' => $reviews]);
    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()
    {
        return view('admin.review.create');
    }
    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */
    public function store(PhotosRequestReview $request)
    {
        $input = $request->all();
        $time = time();

        if ($file = $request->file('image')){
            $filename = $time . $file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());
            //$image_resize->resize(1536, 745);
            $image_resize->save(public_path('images/products/' .$filename));
            $photo = Photo::create(['file'=> $filename]);
            $input['image'] = $photo->id;
        }

        Review::create($input);
        $request->session()->flash('alert-success', 'review was successful added!');
        return redirect()->route('admin.review.index');
    }


    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */
    public function destroy($id)
    {
        $review = Review::findOrFail($id);
        if(!empty($review->image)) {
            $file = Photo::find($review->image);
            $file_path = public_path() . $file->file;
            unlink($file_path);
            $file->delete();
        }

        $review->delete();
        Session::flash('alert-success', 'review was successful deleted!');
        return redirect()->route('admin.review.index');
    }

}

