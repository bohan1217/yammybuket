<?php
namespace App\Http\Controllers\Admin;
use App\Text;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminSettingsController extends Controller
{

    public function index() {
        $text = Text::all();
        return view('admin.settings.index', ['text' => $text]);
    }

    public function edit($id)
    {
        $text = Text::findOrFail($id);
        return view('admin.settings.edit', ['text'=>$text]);
    }


    public function update(Request $request, $id)
    {
        Text::findOrFail($id)->update($request->all());
        $request->session()->flash('alert-success', 'Text was successful edited!');
        return redirect()->route('admin.settings.index');
    }


}