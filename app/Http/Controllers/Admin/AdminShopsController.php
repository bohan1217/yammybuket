<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Shop;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Validator;


class AdminShopsController extends Controller
{
    public function index()
    {
        $shops = Shop::all();
        return view('admin.shops.index', ['shops' => $shops]);
    }


    public function edit($id)
    {
        $shop = Shop::findOrFail($id);
        return view('admin.shops.edit', ['shop'=>$shop]);
    }

    public function create()
    {
        return view('admin.shops.create');
    }


    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        $input = $request->all();
        Shop::create($input);
        $request->session()->flash('alert-success', 'Shop was successful added!');
        return redirect()->route('admin.shops.index');
    }


    public function update(Request $request, $id)
    {
        Shop::findOrFail($id)->update($request->all());
        $request->session()->flash('alert-success', 'Shop was successful edited!');
        return redirect()->route('admin.shops.index');
    }



    public function destroy($id)
    {
        Shop::findOrFail($id)->delete();
        Session::flash('alert-success', 'Shop was successful deleted!');
        return redirect()->route('admin.shops.index');
    }

}