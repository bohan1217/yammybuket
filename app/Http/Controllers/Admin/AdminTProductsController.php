<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\PhotosRequest;

use App\Product;
use App\Size;
use App\Photo;
use App\Tproducts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;

class AdminTProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Tproducts::orderBy('created_at', 'desc')->get();
        return view('admin.tproducts.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all()->pluck('name', 'id')->all();
        $size = Size::all()->pluck('name', 'id')->all();
        return view('admin.tproducts.create', ['products'=>$products, 'size' => $size]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'icon' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $input = $request->all();

        $products_all = Tproducts::where('product_id', $request->product_id)->get();
        if(count($products_all) > 1) {
                if($request->priority != 1) {
                    $input['priority'] = null;
                } else {
                    foreach ($products_all as $callPlan) {
                        $callPlan->priority = null;
                        $callPlan->save();
                    }
                }
            }
        else {
            $input['priority'] = 1;
        }

        if ($files = $request->file('icon')){
            $fil1=[];
            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $filename = time().str_random(15).'.'.$extension;

                $image_resize = Image::make($file->getRealPath());
                //$image_resize->resize(640, 600);
                $image_resize->save(public_path('images/products/' .$filename));
                $photo = Photo::create(['file'=> $filename]);
                $fil1[] = $photo->id;
            }
            $input['icon'] = json_encode($fil1);
        }

        Tproducts::create($input);
        $request->session()->flash('alert-success', 'Product was successful added!');
        return redirect()->route('admin.tproducts.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Tproducts::findOrFail($id);
        $products = Product::all()->pluck('name', 'id')->all();
        $size = Size::all()->pluck('name', 'id')->all();
        return view('admin.tproducts.edit', ['product'=>$product, 'products'=>$products, 'size' => $size]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $validator = \Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        $product = Tproducts::findOrFail($id);
        $input = $request->all();


        $products_all = Tproducts::where('product_id', $request->product_id)->get();
        if(count($products_all) > 1) {
            if($request->priority != 1) {
                $input['priority'] = null;
            } else {
                foreach ($products_all as $callPlan) {
                    $callPlan->priority = null;
                    $callPlan->save();
                }
            }
        }
        else {
            $input['priority'] = 1;
        }

        if ($files = $request->file('icon')){
            if(!empty($product->icon)) {
                foreach (json_decode($product->icon) as $photo_del) {
                    $file_del = Photo::find($photo_del);

                    if(!empty($file_del)) {
                        $file_path = public_path() . $file_del->file;
                        unlink($file_path);
                        $file_del->delete();
                    }
                }
            }

            $fil2=[];
            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $filename = time().str_random(15).'.'.$extension;

                $image_resize = Image::make($file->getRealPath());
                //$image_resize->resize(640, 600);
                $image_resize->save(public_path('images/products/' .$filename));
                $photo = Photo::create(['file'=> $filename]);

                $fil2[] = $photo->id;
            }
            $input['icon'] = json_encode($fil2);
        }

        $product->update($input);
        $request->session()->flash('alert-success', 'Product was successful update!');
        return redirect()->route('admin.tproducts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Tproducts::findOrFail($id);
        if(!empty($product->icon)) {
            foreach (json_decode($product->icon) as $photo) {
                $file = Photo::find($photo);
                if(!empty($file)) {
                    $file_path = public_path() . $file->file;
                    unlink($file_path);
                    $file->delete();
                }
            }
        }
        $product->delete();
        Session::flash('alert-success', 'Product was successful deleted!');
        return redirect()->route('admin.tproducts.index');
    }
}
