<?php

namespace App\Http\Controllers;
use App\Category;
use App\Product;
use App\Feedback;

use App\Review;
use App\Shop;
use App\Text;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //$products = Product::orderBy('created_at', 'desc')->where('category_id', '=', 1)->get();
        $products = Product::orderBy('sort', 'asc')->where('category_id', '=', 1)->take(4)->get();
        $products_fruit = Product::orderBy('sort', 'asc')->where('category_id', '=', 4)->take(4)->get();
        $shops = Shop::orderBy('id', 'asc')->get();
        $texts = Text::orderBy('created_at', 'desc')->where('id', '=', 1)->get();
        $reviews = Review::orderBy('id', 'asc')->get();


        $cart = Session::has('cart') ? Session::get('cart') : null;
        return view('index', ['products'=>$products, 'products_fruit'=>$products_fruit, 'cart'=>$cart, 'shops' => $shops, 'texts'=>$texts, 'reviews' => $reviews]);
    }


    public function catalog() {
        $products = Product::orderBy('sort', 'asc')->where('category_id', '!=', 2)->get();
        $texts = Text::orderBy('created_at', 'desc')->where('id', '=', 1)->get();

        $cart = Session::has('cart') ? Session::get('cart') : null;
        return view('catalog.index', ['products'=>$products, 'cart'=>$cart, 'texts'=>$texts]);
    }

    public function catalogSection($name) {
        $category= Category::orderBy('id', 'asc')->where('alias', '=', $name)->first();
        if(!$category['id']) {
            return Redirect::to('/catalog/');
        }
        $products = Product::orderBy('sort', 'asc')->where('category_id', '=', $category['id'])->get();
        $texts = Text::orderBy('created_at', 'desc')->where('id', '=', 1)->get();

        $cart = Session::has('cart') ? Session::get('cart') : null;
        return view('catalog.index', ['products'=>$products, 'cart'=>$cart, 'texts'=>$texts, 'category' => $category]);
    }

    public function FeedbackAdd(Request $request)
    {
        $input = $request->all();
        Feedback::create($input);

        $result = '<h2>С вашего сайта поступила заявка на звонок.</h2>';
        $result .= '<ul>';
            $result .= '<li><p>Номер телефона: ' . $request->phone . '</p></li>';
        $result .= '</ul>';
        $to      = 'zakazyammy@gmail.com';
        //$to      = 'bohdan864@gmail.com';
        $subject = 'Новая заявка с сайта Yammy';
        $message = $result;
        $headers = 'From: zakazyammy@gmail.com' . "\r\n" .
            'Reply-To: zakazyammy@gmail.com' . "\r\n" .
            "MIME-Version: 1.0" . "\r\n" .
            "Content-type: text/html; charset=UTF-8" . "\r\n"
        ;
        mail($to, $subject, $message, $headers);


        echo 1;
    }


}
