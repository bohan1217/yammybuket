<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Order extends Model
{
    protected $fillable = ['orderId','status','totalAmount', 'products', 'adress', 'name', 'phone', 'delivery', 'name2', 'phone2', 'street', 'house','flat','ascension', 'message','email', 'deliveryshop'];

    public function getProductsAttribute($value)
    {

        $pairs = explode('|', $value);
        $products = array();
        $x=0;
        foreach ($pairs as $pair) {
            $nums = explode('-', $pair);

            $final_key = substr($nums[0], -1);
            $id = substr($nums[0], 1, -1);


            if($final_key == 2){
                $products[$x]['product'] = Product::find($id);
            } else {
                $products[$x]['product'] = Tproducts::find($id);
            }

            $products[$x]['qty'] = $nums[1];
            $products[$x]['value'] = $nums[2];
            $x++;
        }
        return $products;
    }
}

