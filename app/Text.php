<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    protected $fillable = ['slider', 'title2', 'text2', 'title3', 'text3', 'title4', 'text4', 'title5', 'text5','title6', 'text6','title7', 'text7', 'privacy'];
}

