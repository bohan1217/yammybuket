<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tproducts extends Model
{
    public function icon()
    {
        return $this->hasMany(Photo::class, 'icon');
    }


    protected $fillable = [
        'name',
        'icon',
        'product_id',
        'price',
        'mass',
        'priority',
        'desc',
        'size',
    ];

}
