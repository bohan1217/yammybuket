<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['video', 'image'];

    public function image()
    {
        return $this->belongsTo(Photo::class);
    }


}
