function slideTemplate(imageSource, dataValue) {
    return '' + '<div class="carousel-item car_item_'+ dataValue +'" data-value="'+ dataValue +'">' + '<div class="carousel-item-inner">' + '<img class="d-block img-fluid" src="' + imageSource + '" alt="" data-value="'+ dataValue +'">' + '</div>' + '</div>';
}

function generateSlider(item) {
    return $(item).find('.additional-images img').toArray().map(function (image) {
        return slideTemplate($(image).attr('src'), $(image).attr('data-value'));
    }).reduce(function (a, b) {
        return a + b;
    });
}


function checkInput(id, itemId, el, button, price) {

    $('.decription_modal').html($('.ckech_'+el+'.check_input_item_'+id).attr('data-value'));
    $('.mass_element').html($('.ckech_'+el+'.check_input_item_'+id).attr('data-size'));

    $('.add_to_cart').text(button);
    $('.carousel-inner .carousel-item').removeClass('active');

    $('#id_product').val(itemId);
    $('#size_product').val(el);
    $('#price_product').val(price);

    $('#bouquetModal').find('.carousel-inner').html(generateSlider('#element_' + id)).css('background-color');
    $(".carousel-item").not('.car_item_' + el).remove();
    $('#bouquetModal').find('.car_item_'+ el)[0].classList.add('active');
}


function sendForm(nameForm, href_form = '', fancybox_open = '#thank') {
    $('.'+ nameForm).validate({
        errorClass: 'error-input',
        validClass: 'success-input',
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
        },
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 50
            },
            phone: {
                required: true,
                minlength: 3,
                maxlength: 50
            },
            email: {
                required: true,
                minlength: 3,
                maxlength: 50
            },
            message: {
                required: true,
                minlength: 3
            }
        },
        submitHandler: function(form) {
            var form = $(form)
            form_result = $('.error_result');
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                async: true,
                data: form.serialize(),
                beforeSend: function() {
                    $('#loading').fadeIn('fast');
                },
                success: function(data) {
                    if (data == 1) {
                        form_result.html('');
                        form.trigger('reset');
                        $('input').removeClass('success-input');
                        $('textarea').removeClass('success-input');
                        setTimeout(function() {
                            $(fancybox_open).modal('show');
                            //document.getElementById(href_form).click();
                        }, 100);

                    } else {
                        form_result.html(data);
                    }
                    $('#loading').fadeOut('fast');
                },
                error: function() {
                    $('#loading').fadeOut('fast');
                }
            });
            return false;
        }
    });
}


$(".button_send_form_not_modal_window").on('click', function(){
    let nameForm = $(this).closest("form").attr('name');
    sendForm(nameForm);
});


$('.gotopage').click(function(e){
    e.preventDefault();
    let href_form = $(this).attr('data-url');
    if(href_form) {
        $('#loading').show();
        window.location.href = href_form;
    }
});

$('.basket-switch-tab').click(function(){
    $("html, body").animate({
        scrollTop: 0
    }, 0);
});

$('.cursor_pointer').click(function(e){
    e.preventDefault();
    $('#loading').fadeIn('fast');
    $('#loading').fadeOut('fast');

    let id = $(this).attr('data-id');
    $('.input_del').attr('checked',false);
    $('.inp_' + id).attr('checked',true);
});


var bLazy = new Blazy();


$('.my-del-button').click(function(){
    let id_form = $(this).attr('data-id');
    document.getElementById(id_form).remove();
});




